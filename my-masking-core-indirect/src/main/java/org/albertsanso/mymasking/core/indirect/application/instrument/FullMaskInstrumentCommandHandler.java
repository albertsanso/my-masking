package org.albertsanso.mymasking.core.indirect.application.instrument;

import org.albertsanso.commons.command.DomainCommandHandler;
import org.albertsanso.commons.command.DomainCommandResponse;
import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrument;
import org.albertsanso.mymasking.core.indirect.command.instrument.FullMaskInstrumentCommand;
import org.albertsanso.mymasking.core.indirect.domain.service.instrument.InstrumentFullMaskService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
public class FullMaskInstrumentCommandHandler extends DomainCommandHandler<FullMaskInstrumentCommand> {

    private final InstrumentFullMaskService instrumentFullMaskService;

    @Inject
    public FullMaskInstrumentCommandHandler(InstrumentFullMaskService instrumentFullMaskService) {
        this.instrumentFullMaskService = instrumentFullMaskService;
    }

    @Override
    public DomainCommandResponse handle(FullMaskInstrumentCommand command) {
        DomainCommandResponse response;
        try {
            MaskingInstrument maskingInstrument = instrumentFullMaskService.maskInstrument(
                    command.getInstrumentId(),
                    command.getActor(),
                    command.getMaskingType(),
                    command.getUser(),
                    command.getComment(),
                    Optional.empty()
            );
            response = DomainCommandResponse.successResponse(maskingInstrument);
        }
        catch (IllegalStateException ise) {
            response = DomainCommandResponse.failResponse(ise);
        }
        return response;
    }
}
