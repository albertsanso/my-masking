package org.albertsanso.mymasking.core.indirect.application.module;

import org.albertsanso.commons.command.DomainCommandHandler;
import org.albertsanso.commons.command.DomainCommandResponse;
import org.albertsanso.mymasking.core.domain.model.module.MaskingModule;
import org.albertsanso.mymasking.core.indirect.command.module.FullMaskModuleInInstrumentCommand;
import org.albertsanso.mymasking.core.indirect.domain.service.module.ModuleFullMaskService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
public class FullMaskModuleInInstrumentCommandHandler extends DomainCommandHandler<FullMaskModuleInInstrumentCommand> {

    private final ModuleFullMaskService moduleFullMaskService;

    @Inject
    public FullMaskModuleInInstrumentCommandHandler(ModuleFullMaskService moduleFullMaskService) {
        this.moduleFullMaskService = moduleFullMaskService;
    }

    @Override
    public DomainCommandResponse handle(FullMaskModuleInInstrumentCommand command) {
        DomainCommandResponse response;
        try {
            MaskingModule maskingModule = moduleFullMaskService.maskModule(
                    command.getModuleId(),
                    command.getInstrumentId(),
                    command.getActor(),
                    command.getMaskingType(),
                    command.getUser(),
                    command.getComment(),
                    Optional.empty()
            );
            response = DomainCommandResponse.successResponse(maskingModule);
        }
        catch (IllegalStateException ise) {
            response = DomainCommandResponse.failResponse(ise);
        }
        return response;
    }
}
