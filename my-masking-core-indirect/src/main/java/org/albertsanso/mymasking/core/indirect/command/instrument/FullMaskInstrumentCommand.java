package org.albertsanso.mymasking.core.indirect.command.instrument;

import org.albertsanso.mymasking.core.command.instrument.MaskInstrumentCommand;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;
import org.albertsanso.mymasking.core.domain.model.User;

import java.time.ZonedDateTime;

public class FullMaskInstrumentCommand extends MaskInstrumentCommand {
    public FullMaskInstrumentCommand(ZonedDateTime occurredOn, String uuid, String instrumentId, MaskingActor actor, MaskingType maskingType, User user, String comment) {
        super(occurredOn, uuid, instrumentId, actor, maskingType, user, comment);
    }
}
