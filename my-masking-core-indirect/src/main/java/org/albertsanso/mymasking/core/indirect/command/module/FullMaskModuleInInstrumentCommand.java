package org.albertsanso.mymasking.core.indirect.command.module;

import org.albertsanso.mymasking.core.command.module.MaskModuleInInstrumentCommand;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;
import org.albertsanso.mymasking.core.domain.model.User;

import java.time.ZonedDateTime;

public class FullMaskModuleInInstrumentCommand extends MaskModuleInInstrumentCommand {
    public FullMaskModuleInInstrumentCommand(ZonedDateTime occurredOn, String uuid, String moduleId, String instrumentId, MaskingActor actor, MaskingType maskingType, User user, String comment) {
        super(occurredOn, uuid, moduleId, instrumentId, actor, maskingType, user, comment);
    }
}
