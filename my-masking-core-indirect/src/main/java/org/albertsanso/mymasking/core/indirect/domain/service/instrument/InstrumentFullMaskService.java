package org.albertsanso.mymasking.core.indirect.domain.service.instrument;

import org.albertsanso.mymasking.core.domain.model.*;
import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrument;
import org.albertsanso.mymasking.core.domain.port.InstrumentRepository;
import org.albertsanso.mymasking.core.domain.service.instrument.InstrumentCreationService;
import org.albertsanso.mymasking.core.domain.service.instrument.InstrumentDirectMaskService;
import org.albertsanso.mymasking.core.domain.service.instrument.InstrumentMaskService;
import org.albertsanso.mymasking.core.domain.service.module.ModuleIndirectMaskService;
import org.albertsanso.mymasking.core.domain.service.test.TestIndirectMaskService;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument.MasterInstrumentJpaRepository;
import org.albertsanso.mymasking.core.masterdata.domain.model.instrument.MasterInstrument;
import org.albertsanso.mymasking.core.masterdata.domain.model.module.MasterModule;
import org.albertsanso.mymasking.core.masterdata.domain.model.test.MasterTest;
import org.albertsanso.mymasking.core.masterdata.domain.service.module.MasterModuleSearchService;
import org.albertsanso.mymasking.core.masterdata.domain.service.test.MasterTestSearchService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Named
public class InstrumentFullMaskService extends InstrumentMaskService {

    private final MasterInstrumentJpaRepository masterInstrumentJpaRepository;

    private final MasterModuleSearchService masterModuleSearchService;

    private final MasterTestSearchService masterTestSearchService;

    private final InstrumentDirectMaskService instrumentDirectMaskService;

    private final ModuleIndirectMaskService moduleIndirectMaskService;

    private final TestIndirectMaskService testIndirectMaskService;

    @Inject
    public InstrumentFullMaskService(InstrumentRepository instrumentRepository, InstrumentCreationService instrumentCreationService, MasterInstrumentJpaRepository masterInstrumentJpaRepository, MasterModuleSearchService masterModuleSearchService, MasterTestSearchService masterTestSearchService, ModuleIndirectMaskService moduleIndirectMaskService, InstrumentDirectMaskService instrumentDirectMaskService, TestIndirectMaskService testIndirectMaskService) {
        super(instrumentRepository, instrumentCreationService);
        this.masterInstrumentJpaRepository = masterInstrumentJpaRepository;
        this.masterModuleSearchService = masterModuleSearchService;
        this.masterTestSearchService = masterTestSearchService;
        this.moduleIndirectMaskService = moduleIndirectMaskService;
        this.instrumentDirectMaskService = instrumentDirectMaskService;
        this.testIndirectMaskService = testIndirectMaskService;
    }

    @Override
    public MaskingInstrument maskInstrument(String instrumentId, MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> op) {

        IndirectContextInfo contextInfo = new IndirectContextInfo(MaskingEntityType.INSTRUMENT);
        contextInfo.addProperty("instrument_id", instrumentId);

        Optional<IndirectContextInfo> optionalContextInfo = Optional.of(contextInfo);

        MaskingInstrument maskingInstrument = null;
        final Optional<MasterInstrument> optionalMasterInstrument = masterInstrumentJpaRepository.findByInstrumentId(instrumentId);

        if (optionalMasterInstrument.isPresent()) {
            MasterInstrument masterInstrument = optionalMasterInstrument.get();

            maskingInstrument = instrumentDirectMaskService.maskInstrument(masterInstrument.getInstrumentId(), actor, type, user, comment, optionalContextInfo);

            final List<MasterModule> masterModuleList = masterModuleSearchService.findModulesInInstrument(masterInstrument.getId());
            for (MasterModule masterModule : masterModuleList) {
                moduleIndirectMaskService.maskModule(masterModule.getModuleId(), masterInstrument.getInstrumentId(), actor, type, user, comment, optionalContextInfo);

                final List<MasterTest> masterTestList = masterTestSearchService.findTestsInModule(masterModule.getId(), masterInstrument.getId());
                for (MasterTest masterTest : masterTestList) {
                    testIndirectMaskService.maskTestInModule(masterTest.getTestId(), masterInstrument.getInstrumentId(), masterModule.getModuleId(), actor, type, user, comment, optionalContextInfo);
                }

            }
        }
        return maskingInstrument;
    }
}
