package org.albertsanso.mymasking.core.indirect.domain.service.module;

import org.albertsanso.mymasking.core.domain.model.*;
import org.albertsanso.mymasking.core.domain.model.module.MaskingModule;
import org.albertsanso.mymasking.core.domain.port.ModuleRepository;
import org.albertsanso.mymasking.core.domain.service.module.ModuleCreationService;
import org.albertsanso.mymasking.core.domain.service.module.ModuleDirectMaskService;
import org.albertsanso.mymasking.core.domain.service.module.ModuleMaskService;
import org.albertsanso.mymasking.core.domain.service.test.TestIndirectMaskService;
import org.albertsanso.mymasking.core.masterdata.domain.model.instrument.MasterInstrument;
import org.albertsanso.mymasking.core.masterdata.domain.model.module.MasterModule;
import org.albertsanso.mymasking.core.masterdata.domain.model.test.MasterTest;
import org.albertsanso.mymasking.core.masterdata.domain.service.instrument.MasterInstrumentSearchService;
import org.albertsanso.mymasking.core.masterdata.domain.service.module.MasterModuleSearchService;
import org.albertsanso.mymasking.core.masterdata.domain.service.test.MasterTestSearchService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Named
public class ModuleFullMaskService extends ModuleMaskService {

    private final MasterModuleSearchService masterModuleSearchService;

    private final MasterInstrumentSearchService masterInstrumentSearchService;

    private final ModuleDirectMaskService moduleDirectMaskService;

    private final MasterTestSearchService masterTestSearchService;

    private final TestIndirectMaskService testIndirectMaskService;

    @Inject
    public ModuleFullMaskService(ModuleRepository moduleRepository, ModuleCreationService moduleCreationService, MasterModuleSearchService masterModuleSearchService, MasterInstrumentSearchService masterInstrumentSearchService, ModuleDirectMaskService moduleDirectMaskService, MasterTestSearchService masterTestSearchService, TestIndirectMaskService testIndirectMaskService) {
        super(moduleRepository, moduleCreationService);
        this.masterModuleSearchService = masterModuleSearchService;
        this.masterInstrumentSearchService = masterInstrumentSearchService;
        this.moduleDirectMaskService = moduleDirectMaskService;
        this.masterTestSearchService = masterTestSearchService;
        this.testIndirectMaskService = testIndirectMaskService;
    }

    @Override
    public MaskingModule maskModule(String moduleId, String instrumentId, MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> op) {

        IndirectContextInfo contextInfo = new IndirectContextInfo(MaskingEntityType.MODULE);
        contextInfo.addProperty("module_id", moduleId);
        contextInfo.addProperty("instrument_id", instrumentId);

        Optional<IndirectContextInfo> optionalContextInfo = Optional.of(contextInfo);

        MaskingModule maskingModule = null;

        final Optional<MasterInstrument> optionalMasterInstrument = masterInstrumentSearchService.findByInstrumentId(instrumentId);
        final Optional<MasterModule> optionalMasterModule = masterModuleSearchService.findByModuleId(moduleId);

        if (optionalMasterInstrument.isPresent()) {
            MasterInstrument masterInstrument = optionalMasterInstrument.get();

            if (optionalMasterModule.isPresent()) {
                MasterModule masterModule = optionalMasterModule.get();

                maskingModule = moduleDirectMaskService.maskModule(
                        masterModule.getModuleId(),
                        masterInstrument.getInstrumentId(),
                        actor, type, user, comment, optionalContextInfo);

                final List<MasterTest> masterTestList = masterTestSearchService.findTestsInModule(masterModule.getId(), masterInstrument.getId());
                for (MasterTest masterTest : masterTestList) {
                    testIndirectMaskService.maskTestInModule(
                            masterTest.getTestId(),
                            masterInstrument.getInstrumentId(),
                            masterModule.getModuleId(),
                            actor, type, user, comment, optionalContextInfo);
                }
            }
        }
        return maskingModule;
    }
}
