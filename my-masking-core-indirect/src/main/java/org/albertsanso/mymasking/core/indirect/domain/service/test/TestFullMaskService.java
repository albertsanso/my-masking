package org.albertsanso.mymasking.core.indirect.domain.service.test;

import org.albertsanso.mymasking.core.domain.model.IndirectContextInfo;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;
import org.albertsanso.mymasking.core.domain.model.User;
import org.albertsanso.mymasking.core.domain.model.test.MaskingTest;
import org.albertsanso.mymasking.core.domain.port.TestRepository;
import org.albertsanso.mymasking.core.domain.service.test.TestCreationService;
import org.albertsanso.mymasking.core.domain.service.test.TestMaskService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import java.util.Optional;

@Named
public class TestFullMaskService extends TestMaskService {

    @Inject
    public TestFullMaskService(TestRepository testRepository, TestCreationService testCreationService) {
        super(testRepository, testCreationService);
    }

    @Override
    public MaskingTest maskTestInInstrument(String testId, String instrumentId, MaskingActor actor, MaskingType maskingType, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        MaskingTest maskingTest = getOrCreateTest(testId, instrumentId, Optional.empty());
        return mask(maskingTest, actor, maskingType, user, comment, contextInfo);
    }

    @Override
    public MaskingTest maskTestInModule(String testId, String instrumentId, String moduleId, MaskingActor actor, MaskingType maskingType, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        MaskingTest maskingTest = getOrCreateTest(testId, instrumentId, Optional.of(moduleId));
        return mask(maskingTest, actor, maskingType, user, comment, contextInfo);
    }

    private MaskingTest mask(MaskingTest maskingTest, MaskingActor actor, MaskingType maskingType, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        maskingTest.directMask(actor, maskingType, user, comment, Optional.empty());
        return saveMaskingTest(maskingTest);
    }
}
