package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="masking_masterdata_instrument")
public class MasterInstrumentJpa {

    @Id
    @NotNull
    private String id;

    @Column(name="instrumentId")
    @NotNull
    private String instrumentId;

    @Column(name="name")
    @NotNull
    private String name;

    public MasterInstrumentJpa() {
    }

    public MasterInstrumentJpa(@NotNull String id, @NotNull String instrumentId, @NotNull String name) {
        this.id = id;
        this.instrumentId = instrumentId;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(String instrumentId) {
        this.instrumentId = instrumentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
