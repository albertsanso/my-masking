package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument;

import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument.mapper.MasterInstrumentJpaToMasterInstrumentMapper;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument.mapper.MasterInstrumentToMasterInstrumentJpaMapper;
import org.albertsanso.mymasking.core.masterdata.domain.model.instrument.MasterInstrument;
import org.albertsanso.mymasking.core.masterdata.domain.port.MasterInstrumentRepository;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
public class MasterInstrumentJpaRepository implements MasterInstrumentRepository {

    private final MasterInstrumentJpaRepositoryHelper masterInstrumentJpaRepositoryHelper;

    private final MasterInstrumentToMasterInstrumentJpaMapper masterInstrumentToMasterInstrumentJpaMapper;

    private final MasterInstrumentJpaToMasterInstrumentMapper masterInstrumentJpaToMasterInstrumentMapper;

    @Inject
    public MasterInstrumentJpaRepository(MasterInstrumentJpaRepositoryHelper masterInstrumentJpaRepositoryHelper, MasterInstrumentToMasterInstrumentJpaMapper masterInstrumentToMasterInstrumentJpaMapper, MasterInstrumentJpaToMasterInstrumentMapper masterInstrumentJpaToMasterInstrumentMapper) {
        this.masterInstrumentJpaRepositoryHelper = masterInstrumentJpaRepositoryHelper;
        this.masterInstrumentToMasterInstrumentJpaMapper = masterInstrumentToMasterInstrumentJpaMapper;
        this.masterInstrumentJpaToMasterInstrumentMapper = masterInstrumentJpaToMasterInstrumentMapper;
    }

    @Override
    public Optional<MasterInstrument> findById(String id) {
        final Optional<MasterInstrumentJpa> optionalMasterInstrumentJpa = masterInstrumentJpaRepositoryHelper.findById(id);
        if (optionalMasterInstrumentJpa.isPresent()) {
            MasterInstrument masterInstrument = masterInstrumentJpaToMasterInstrumentMapper.apply(optionalMasterInstrumentJpa.get());
            return Optional.of(masterInstrument);
        }
        return Optional.empty();
    }

    @Override
    public Optional<MasterInstrument> findByInstrumentId(String id) {
        final Optional<MasterInstrumentJpa> optionalMasterInstrumentJpa = masterInstrumentJpaRepositoryHelper.findByInstrumentId(id);
        if (optionalMasterInstrumentJpa.isPresent()) {
            MasterInstrument masterInstrument = masterInstrumentJpaToMasterInstrumentMapper.apply(optionalMasterInstrumentJpa.get());
            return Optional.of(masterInstrument);
        }
        return Optional.empty();
    }

    @Override
    public MasterInstrument save(MasterInstrument masterInstrument) {
        MasterInstrumentJpa masterInstrumentJpa = masterInstrumentToMasterInstrumentJpaMapper.apply(masterInstrument);
        final MasterInstrumentJpa save = masterInstrumentJpaRepositoryHelper.save(masterInstrumentJpa);
        return masterInstrumentJpaToMasterInstrumentMapper.apply(masterInstrumentJpa);
    }


}
