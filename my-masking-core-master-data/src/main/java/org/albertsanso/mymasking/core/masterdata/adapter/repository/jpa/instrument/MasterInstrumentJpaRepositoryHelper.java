package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MasterInstrumentJpaRepositoryHelper extends CrudRepository<MasterInstrumentJpa, String> {
    Optional<MasterInstrumentJpa> findById(String id);
    Optional<MasterInstrumentJpa> findByInstrumentId(String id);
}
