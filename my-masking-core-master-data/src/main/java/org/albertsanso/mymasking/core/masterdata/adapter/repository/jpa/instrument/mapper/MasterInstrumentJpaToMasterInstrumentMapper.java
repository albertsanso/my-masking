package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument.mapper;

import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument.MasterInstrumentJpa;
import org.albertsanso.mymasking.core.masterdata.domain.model.instrument.MasterInstrument;

import javax.inject.Named;
import java.util.function.Function;

@Named
public class MasterInstrumentJpaToMasterInstrumentMapper implements Function<MasterInstrumentJpa, MasterInstrument> {
    @Override
    public MasterInstrument apply(MasterInstrumentJpa masterInstrumentJpa) {
        return MasterInstrument.builder()
                .withId(masterInstrumentJpa.getId())
                .withInstrumentId(masterInstrumentJpa.getInstrumentId())
                .withName(masterInstrumentJpa.getName())
                .build();
    }
}
