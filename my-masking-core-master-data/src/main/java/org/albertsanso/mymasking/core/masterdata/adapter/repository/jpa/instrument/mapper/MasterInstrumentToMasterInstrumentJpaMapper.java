package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument.mapper;

import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument.MasterInstrumentJpa;
import org.albertsanso.mymasking.core.masterdata.domain.model.instrument.MasterInstrument;

import javax.inject.Named;
import java.util.function.Function;

@Named
public class MasterInstrumentToMasterInstrumentJpaMapper implements Function<MasterInstrument, MasterInstrumentJpa> {
    @Override
    public MasterInstrumentJpa apply(MasterInstrument masterInstrument) {
        return new MasterInstrumentJpa(
                masterInstrument.getId(),
                masterInstrument.getInstrumentId(),
                masterInstrument.getName()
        );
    }
}
