package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="masking_masterdata_module")
public class MasterModuleJpa {

    @Id
    @NotNull
    private String id;

    @Column(name="moduleId")
    @NotNull
    private String moduleId;

    @Column(name="name")
    @NotNull
    private String name;

    public MasterModuleJpa() {
    }

    public MasterModuleJpa(@NotNull String id, @NotNull String moduleId, @NotNull String name) {
        this.id = id;
        this.moduleId = moduleId;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }
}
