package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module.mapper.MasterModuleJpaToMasterModuleMapper;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module.mapper.MasterModuleToMasterModuleJpaMapper;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping.QMasterTestMappingJpa;
import org.albertsanso.mymasking.core.masterdata.domain.model.module.MasterModule;
import org.albertsanso.mymasking.core.masterdata.domain.port.MasterModuleRepository;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Named
public class MasterModuleJpaRepository implements MasterModuleRepository {

    private final MasterModuleJpaRepositoryHelper masterModuleJpaRepositoryHelper;

    private final MasterModuleJpaToMasterModuleMapper masterModuleJpaToMasterModuleMapper;

    private final MasterModuleToMasterModuleJpaMapper masterModuleToMasterModuleJpaMapper;

    private final EntityManager entityManager;

    @Inject
    public MasterModuleJpaRepository(MasterModuleJpaRepositoryHelper masterModuleJpaRepositoryHelper, MasterModuleJpaToMasterModuleMapper masterModuleJpaToMasterModuleMapper, MasterModuleToMasterModuleJpaMapper masterModuleToMasterModuleJpaMapper, EntityManager entityManager) {
        this.masterModuleJpaRepositoryHelper = masterModuleJpaRepositoryHelper;
        this.masterModuleJpaToMasterModuleMapper = masterModuleJpaToMasterModuleMapper;
        this.masterModuleToMasterModuleJpaMapper = masterModuleToMasterModuleJpaMapper;
        this.entityManager = entityManager;
    }

    @Override
    public Optional<MasterModule> findById(String id) {
        final Optional<MasterModuleJpa> optionalMasterModuleJpa = masterModuleJpaRepositoryHelper.findById(id);
        if (optionalMasterModuleJpa.isPresent()) {
            final MasterModule masterModule = masterModuleJpaToMasterModuleMapper.apply((optionalMasterModuleJpa.get()));
            return Optional.of(masterModule);
        }
        return Optional.empty();
    }

    @Override
    public Optional<MasterModule> findByModuleId(String id) {
        final Optional<MasterModuleJpa> optionalMasterModuleJpa = masterModuleJpaRepositoryHelper.findByModuleId(id);
        if (optionalMasterModuleJpa.isPresent()) {
            final MasterModule masterModule = masterModuleJpaToMasterModuleMapper.apply((optionalMasterModuleJpa.get()));
            return Optional.of(masterModule);
        }
        return Optional.empty();
    }

    @Override
    public List<MasterModule> findByInstrumentId(String instrumentId) {
        final QMasterModuleJpa qMasterModuleJpa = QMasterModuleJpa.masterModuleJpa;
        final QMasterTestMappingJpa qMasterTestMappingJpa = QMasterTestMappingJpa.masterTestMappingJpa;

        final JPAQuery<MasterModuleJpa> query = new JPAQuery<>(entityManager);

        BooleanBuilder booleanBuilder = new BooleanBuilder();
        booleanBuilder.and(qMasterTestMappingJpa.masterInstrument.id.eq(instrumentId));

        List<Tuple> results = query
                .distinct()
                .select(qMasterModuleJpa.id, qMasterModuleJpa.moduleId, qMasterModuleJpa.name)
                .from(qMasterTestMappingJpa)
                .innerJoin(qMasterTestMappingJpa.masterModule, qMasterModuleJpa)
                    .on(qMasterTestMappingJpa.masterModule.eq(qMasterModuleJpa))
                .where(booleanBuilder)
                .groupBy(qMasterTestMappingJpa.masterModule)
                .fetch();

        return fillMasterModuleFromTuples(qMasterModuleJpa, results);
    }

    private static List<MasterModule> fillMasterModuleFromTuples(QMasterModuleJpa qMasterModuleJpa, List<Tuple> results) {
        return results
                .stream()
                .map(tuple -> MasterModule.builder()
                        .withId(tuple.get(qMasterModuleJpa.id))
                        .withModuleId(tuple.get(qMasterModuleJpa.moduleId))
                        .withName(tuple.get(qMasterModuleJpa.name))
                        .build()
                ).collect(Collectors.toList());
    }

    @Override
    public MasterModule save(MasterModule masterModule) {
        MasterModuleJpa masterModuleJpa = masterModuleToMasterModuleJpaMapper.apply(masterModule);
        masterModuleJpa = masterModuleJpaRepositoryHelper.save(masterModuleJpa);
        return masterModuleJpaToMasterModuleMapper.apply(masterModuleJpa);
    }
}
