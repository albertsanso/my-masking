package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MasterModuleJpaRepositoryHelper extends CrudRepository<MasterModuleJpa, String> {
    Optional<MasterModuleJpa> findById(String id);
    Optional<MasterModuleJpa> findByModuleId(String id);
}
