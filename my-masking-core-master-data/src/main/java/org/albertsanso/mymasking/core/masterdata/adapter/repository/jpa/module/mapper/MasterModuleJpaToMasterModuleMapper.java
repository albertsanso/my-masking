package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module.mapper;

import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module.MasterModuleJpa;
import org.albertsanso.mymasking.core.masterdata.domain.model.module.MasterModule;

import javax.inject.Named;
import java.util.function.Function;

@Named
public class MasterModuleJpaToMasterModuleMapper implements Function<MasterModuleJpa, MasterModule> {
    @Override
    public MasterModule apply(MasterModuleJpa masterModuleJpa) {
        return MasterModule.builder()
                .withId(masterModuleJpa.getId())
                .withModuleId(masterModuleJpa.getModuleId())
                .withName(masterModuleJpa.getName())
                .build();
    }
}
