package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module.mapper;

import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module.MasterModuleJpa;
import org.albertsanso.mymasking.core.masterdata.domain.model.module.MasterModule;

import javax.inject.Named;
import java.util.function.Function;

@Named
public class MasterModuleToMasterModuleJpaMapper implements Function<MasterModule, MasterModuleJpa> {
    @Override
    public MasterModuleJpa apply(MasterModule masterModule) {
        return new MasterModuleJpa(
                masterModule.getId(),
                masterModule.getModuleId(),
                masterModule.getName()
        );
    }
}
