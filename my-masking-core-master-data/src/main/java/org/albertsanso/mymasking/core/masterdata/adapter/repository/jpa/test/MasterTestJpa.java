package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name="masking_masterdata_test")
public class MasterTestJpa {

    @Id
    @NotNull
    private String id;

    @Column(name="testId")
    @NotNull
    private String testId;

    @Column(name="name")
    private String name;

    @Column(name="abbreviation")
    @NotNull
    private String abbreviation;

    public MasterTestJpa() {
    }

    public MasterTestJpa(String id, String testId, String name, String abbreviation) {
        this.id = id;
        this.testId = testId;
        this.name = name;
        this.abbreviation = abbreviation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MasterTestJpa)) return false;
        MasterTestJpa that = (MasterTestJpa) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
