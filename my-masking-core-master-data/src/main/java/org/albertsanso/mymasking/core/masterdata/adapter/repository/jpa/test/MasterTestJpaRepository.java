package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test.mapper.MasterTestJpaToMasterTestMapper;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test.mapper.MasterTestToMasterTestJpaMapper;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping.MasterTestMappingJpa;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping.QMasterTestMappingJpa;
import org.albertsanso.mymasking.core.masterdata.domain.model.test.MasterTest;
import org.albertsanso.mymasking.core.masterdata.domain.port.MasterTestRepository;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Named
public class MasterTestJpaRepository implements MasterTestRepository {

    private final MasterTestJpaToMasterTestMapper masterTestJpaToMasterTestMapper;

    private final MasterTestToMasterTestJpaMapper masterTestToMasterTestJpaMapper;

    private final MasterTestJpaRepositoryHelper masterTestJpaRepositoryHelper;

    private final EntityManager entityManager;

    @Inject
    public MasterTestJpaRepository(MasterTestJpaToMasterTestMapper masterTestJpaToMasterTestMapper, MasterTestToMasterTestJpaMapper masterTestToMasterTestJpaMapper, MasterTestJpaRepositoryHelper masterTestJpaRepositoryHelper, EntityManager entityManager) {
        this.masterTestJpaToMasterTestMapper = masterTestJpaToMasterTestMapper;
        this.masterTestToMasterTestJpaMapper = masterTestToMasterTestJpaMapper;
        this.masterTestJpaRepositoryHelper = masterTestJpaRepositoryHelper;
        this.entityManager = entityManager;
    }

    @Override
    public Optional<MasterTest> findById(String id) {
        final Optional<MasterTestJpa> masterTestJpa = masterTestJpaRepositoryHelper.findById(id);
        if (masterTestJpa.isPresent()) {
            final MasterTest masterTest = masterTestJpaToMasterTestMapper.apply(masterTestJpa.get());
            return Optional.of(masterTest);
        }
        return Optional.empty();
    }

    @Override
    public Optional<MasterTest> findByTestId(String id) {
        final Optional<MasterTestJpa> masterTestJpa = masterTestJpaRepositoryHelper.findByTestId(id);
        if (masterTestJpa.isPresent()) {
            final MasterTest masterTest = masterTestJpaToMasterTestMapper.apply(masterTestJpa.get());
            return Optional.of(masterTest);
        }
        return Optional.empty();
    }

    @Override
    public List<MasterTest> findByInstrumentId(String instrumentId) {

        final QMasterTestJpa qMasterTestJpa = QMasterTestJpa.masterTestJpa;
        final QMasterTestMappingJpa qMasterTestMappingJpa = QMasterTestMappingJpa.masterTestMappingJpa;

        final JPAQuery<MasterTestMappingJpa> query = new JPAQuery<MasterTestMappingJpa>(entityManager);

        List<Tuple> results = query
                .distinct()
                .select(qMasterTestJpa.id, qMasterTestJpa.testId, qMasterTestJpa.name, qMasterTestJpa.abbreviation)
                .from(qMasterTestMappingJpa)
                .innerJoin(qMasterTestMappingJpa.masterTest, qMasterTestJpa).on(qMasterTestMappingJpa.masterTest.eq(qMasterTestJpa))
                .where(qMasterTestMappingJpa.masterInstrument.id.eq(instrumentId))
                .fetch();

        return fillMasterTestFromTuples(qMasterTestJpa, results);
    }

    @Override
    public List<MasterTest> findByModuleIdAndInstrumentId(String moduleId, String instrumentId) {
        final QMasterTestJpa qMasterTestJpa = QMasterTestJpa.masterTestJpa;
        final QMasterTestMappingJpa qMasterTestMappingJpa = QMasterTestMappingJpa.masterTestMappingJpa;

        final JPAQuery<MasterTestMappingJpa> query = new JPAQuery<>(entityManager);

        BooleanBuilder booleanBuilder = new BooleanBuilder();
        booleanBuilder.and(qMasterTestMappingJpa.masterModule.id.eq(moduleId));
        booleanBuilder.and(qMasterTestMappingJpa.masterInstrument.id.eq(instrumentId));

        List<Tuple> results = query
                .distinct()
                .select(qMasterTestJpa.id, qMasterTestJpa.testId, qMasterTestJpa.name, qMasterTestJpa.abbreviation)
                .from(qMasterTestMappingJpa)
                .innerJoin(qMasterTestMappingJpa.masterTest, qMasterTestJpa)
                    .on(qMasterTestMappingJpa.masterTest.eq(qMasterTestJpa))
                .where(booleanBuilder)
                .fetch();

        return fillMasterTestFromTuples(qMasterTestJpa, results);
    }

    private static List<MasterTest> fillMasterTestFromTuples(QMasterTestJpa qMasterTestJpa, List<Tuple> results) {
        return results
                .stream()
                .map(tuple -> MasterTest.builder()
                        .withId(tuple.get(qMasterTestJpa.id))
                        .withTestId(tuple.get(qMasterTestJpa.testId))
                        .withName(tuple.get(qMasterTestJpa.name))
                        .withAbbreviation(tuple.get(qMasterTestJpa.abbreviation))
                        .build()
                ).collect(Collectors.toList());
    }

    @Override
    public MasterTest save(MasterTest masterTest) {
        MasterTestJpa masterTestJpa = masterTestToMasterTestJpaMapper.apply(masterTest);
        masterTestJpa = masterTestJpaRepositoryHelper.save(masterTestJpa);
        return masterTestJpaToMasterTestMapper.apply(masterTestJpa);
    }
}
