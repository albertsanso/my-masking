package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MasterTestJpaRepositoryHelper extends CrudRepository<MasterTestJpa, String> {
    Optional<MasterTestJpa> findById(String id);
    Optional<MasterTestJpa> findByTestId(String id);
}
