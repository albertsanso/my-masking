package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test.mapper;

import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test.MasterTestJpa;
import org.albertsanso.mymasking.core.masterdata.domain.model.test.MasterTest;

import javax.inject.Named;
import java.util.function.Function;

@Named
public class MasterTestJpaToMasterTestMapper implements Function<MasterTestJpa, MasterTest> {
    @Override
    public MasterTest apply(MasterTestJpa masterTestJpa) {
        return MasterTest.builder()
                .withId(masterTestJpa.getId())
                .withName(masterTestJpa.getName())
                .withAbbreviation(masterTestJpa.getAbbreviation())
                .build();
    }
}
