package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test.mapper;

import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test.MasterTestJpa;
import org.albertsanso.mymasking.core.masterdata.domain.model.test.MasterTest;

import javax.inject.Named;
import java.util.function.Function;

@Named
public class MasterTestToMasterTestJpaMapper implements Function<MasterTest, MasterTestJpa> {
    @Override
    public MasterTestJpa apply(MasterTest masterTest) {
        return new MasterTestJpa(
                masterTest.getId(),
                masterTest.getTestId(),
                masterTest.getName(),
                masterTest.getAbbreviation()
        );
    }
}
