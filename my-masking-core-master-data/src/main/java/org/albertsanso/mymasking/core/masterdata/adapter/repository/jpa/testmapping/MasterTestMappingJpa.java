package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping;

import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument.MasterInstrumentJpa;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module.MasterModuleJpa;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test.MasterTestJpa;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="masking_masterdata_testmapping")
public class MasterTestMappingJpa {

    @Id
    @NotNull
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="masterTest")
    private MasterTestJpa masterTest;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="masterModule")
    private MasterModuleJpa masterModule;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="masterInstrument")
    private MasterInstrumentJpa masterInstrument;

    public MasterTestMappingJpa() {
    }

    public MasterTestMappingJpa(@NotNull String id, MasterTestJpa masterTest, MasterModuleJpa masterModule, MasterInstrumentJpa masterInstrument) {
        this.id = id;
        this.masterTest = masterTest;
        this.masterModule = masterModule;
        this.masterInstrument = masterInstrument;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MasterTestJpa getMasterTest() {
        return masterTest;
    }

    public void setMasterTest(MasterTestJpa masterTest) {
        this.masterTest = masterTest;
    }

    public MasterModuleJpa getMasterModule() {
        return masterModule;
    }

    public void setMasterModule(MasterModuleJpa masterModule) {
        this.masterModule = masterModule;
    }

    public MasterInstrumentJpa getMasterInstrument() {
        return masterInstrument;
    }

    public void setMasterInstrument(MasterInstrumentJpa masterInstrument) {
        this.masterInstrument = masterInstrument;
    }
}
