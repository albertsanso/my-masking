package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping;

import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping.mapper.MasterTestMappingJpaToMasterTestMappingMapper;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping.mapper.MasterTestMappingToMasterTestMappingJpaMapper;
import org.albertsanso.mymasking.core.masterdata.domain.model.testmapping.MasterTestMapping;
import org.albertsanso.mymasking.core.masterdata.domain.port.MasterTestMappingRepository;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
public class MasterTestMappingJpaRepository implements MasterTestMappingRepository {

    private final MasterTestMappingJpaRepositoryHelper masterTestMappingJpaRepositoryHelper;

    private final MasterTestMappingToMasterTestMappingJpaMapper masterTestMappingToMasterTestMappingJpaMapper;

    private final MasterTestMappingJpaToMasterTestMappingMapper masterTestMappingJpaToMasterTestMappingMapper;

    @Inject
    public MasterTestMappingJpaRepository(MasterTestMappingJpaRepositoryHelper masterTestMappingJpaRepositoryHelper, MasterTestMappingToMasterTestMappingJpaMapper masterTestMappingToMasterTestMappingJpaMapper, MasterTestMappingJpaToMasterTestMappingMapper masterTestMappingJpaToMasterTestMappingMapper) {
        this.masterTestMappingJpaRepositoryHelper = masterTestMappingJpaRepositoryHelper;
        this.masterTestMappingToMasterTestMappingJpaMapper = masterTestMappingToMasterTestMappingJpaMapper;
        this.masterTestMappingJpaToMasterTestMappingMapper = masterTestMappingJpaToMasterTestMappingMapper;
    }

    @Override
    public Optional<MasterTestMapping> findById(String id) {
        return Optional.empty();
    }

    @Override
    public Optional<MasterTestMapping> findByTestMappingId(String id) {
        return Optional.empty();
    }

    @Override
    public MasterTestMapping save(MasterTestMapping masterTestMapping) {
        return null;
    }
}
