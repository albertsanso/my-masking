package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MasterTestMappingJpaRepositoryHelper extends CrudRepository<MasterTestMappingJpa, String> {
    Optional<MasterTestMappingJpa> findById(String id);
}
