package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping.mapper;

import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping.MasterTestMappingJpa;
import org.albertsanso.mymasking.core.masterdata.domain.model.testmapping.MasterTestMapping;

import javax.inject.Named;
import java.util.function.Function;

@Named
public class MasterTestMappingJpaToMasterTestMappingMapper implements Function<MasterTestMappingJpa, MasterTestMapping> {
    @Override
    public MasterTestMapping apply(MasterTestMappingJpa masterTestMappingJpa) {
        return null;
    }
}
