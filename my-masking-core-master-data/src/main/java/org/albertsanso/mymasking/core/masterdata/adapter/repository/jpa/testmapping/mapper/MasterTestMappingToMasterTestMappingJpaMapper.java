package org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping.mapper;

import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping.MasterTestMappingJpa;
import org.albertsanso.mymasking.core.masterdata.domain.model.testmapping.MasterTestMapping;

import javax.inject.Named;
import java.util.function.Function;

@Named
public class MasterTestMappingToMasterTestMappingJpaMapper implements Function<MasterTestMapping, MasterTestMappingJpa> {
    @Override
    public MasterTestMappingJpa apply(MasterTestMapping masterTestMapping) {
        /*
        return new MasterTestMappingJpa(
                masterTestMapping.getId(),
                masterTestMapping.getMasterTestId(),
                masterTestMapping.getMasterModuleId(),
                masterTestMapping.getMasterInstrumentId()
        );
         */
        return null;
    }
}
