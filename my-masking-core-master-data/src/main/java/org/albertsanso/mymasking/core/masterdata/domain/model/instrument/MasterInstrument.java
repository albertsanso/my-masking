package org.albertsanso.mymasking.core.masterdata.domain.model.instrument;

import org.albertsanso.commons.model.ValueObject;

public class MasterInstrument extends ValueObject {

    private final String id;

    private final String instrumentId;

    private final String name;

    private MasterInstrument(String id, String instrumentId, String name) {
        this.id = id;
        this.instrumentId = instrumentId;
        this.name = name;
    }

    private static MasterInstrument createNewInstrument(InstrumentBuilder builder) {
        return new MasterInstrument(
                builder.getId(),
                builder.instrumentId,
                builder.getName()
        );
    }

    public static InstrumentBuilder builder() {
        return new InstrumentBuilder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public static final class InstrumentBuilder {

        private String id;

        private String name;

        private String instrumentId;

        public InstrumentBuilder() {
        }

        public InstrumentBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public InstrumentBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public InstrumentBuilder withInstrumentId(String instrumentId) {
            this.instrumentId = instrumentId;
            return this;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getInstrumentId() {
            return instrumentId;
        }

        public MasterInstrument build() { return createNewInstrument(this); }
    }
}
