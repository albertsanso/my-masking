package org.albertsanso.mymasking.core.masterdata.domain.model.module;

import org.albertsanso.commons.model.ValueObject;

public class MasterModule extends ValueObject {

    private final String id;

    private final String moduleId;


    private final String name;

    private MasterModule(String id, String moduleId, String name) {
        this.id = id;
        this.moduleId = moduleId;
        this.name = name;
    }

    private static MasterModule createNewModule(ModuleBuilder builder) {
        return new MasterModule(
                builder.getId(),
                builder.getModuleId(),
                builder.getName());
    }

    public static ModuleBuilder builder() { return new ModuleBuilder(); }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getModuleId() {
        return moduleId;
    }

    public static final class ModuleBuilder {
        private String id;
        private String name;
        private String moduleId;

        public ModuleBuilder() {
        }

        public ModuleBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public ModuleBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ModuleBuilder withModuleId(String moduleId) {
            this.moduleId = moduleId;
            return this;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getModuleId() {
            return moduleId;
        }

        public MasterModule build() { return createNewModule(this); }
    }
}
