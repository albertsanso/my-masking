package org.albertsanso.mymasking.core.masterdata.domain.model.test;

import org.albertsanso.commons.model.ValueObject;

public class MasterTest extends ValueObject {

    private final String id;

    private final String testId;

    private final String name;

    private final String abbreviation;

    private MasterTest(String id, String testId, String name, String abbreviation) {
        this.id = id;
        this.testId = testId;
        this.name = name;
        this.abbreviation = abbreviation;
    }

    private static MasterTest createNewTest(TestBuilder builder) {
        return new MasterTest(
                builder.getId(),
                builder.getTestId(),
                builder.getName(),
                builder.getAbbreviation()
        );
    }

    public static TestBuilder builder() { return new TestBuilder(); }

    public String getId() {
        return id;
    }

    public String getTestId() {
        return testId;
    }

    public String getName() {
        return name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public static final class TestBuilder {

        private String id;

        private String testId;

        private String name;

        private String abbreviation;

        public TestBuilder() {
        }

        public TestBuilder withTestId(String id) {
            this.testId = id;
            return this;
        }

        public TestBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public TestBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public TestBuilder withAbbreviation(String abbreviation) {
            this.abbreviation = abbreviation;
            return this;
        }

        public String getTestId() {
            return testId;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getAbbreviation() {
            return abbreviation;
        }

        public MasterTest build() { return createNewTest(this); }
    }
}
