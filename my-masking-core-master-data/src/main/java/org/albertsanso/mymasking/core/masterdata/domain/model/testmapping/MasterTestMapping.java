package org.albertsanso.mymasking.core.masterdata.domain.model.testmapping;

import org.albertsanso.commons.model.ValueObject;

public class MasterTestMapping extends ValueObject {

    private final String id;

    private final String masterTestId;

    private final String masterModuleId;

    private final String masterInstrumentId;

    public MasterTestMapping(String id, String masterTestId, String masterModuleId, String masterInstrumentId) {
        this.id = id;
        this.masterTestId = masterTestId;
        this.masterModuleId = masterModuleId;
        this.masterInstrumentId = masterInstrumentId;
    }

    public String getId() {
        return id;
    }

    public String getMasterTestId() {
        return masterTestId;
    }

    public String getMasterModuleId() {
        return masterModuleId;
    }

    public String getMasterInstrumentId() {
        return masterInstrumentId;
    }
}
