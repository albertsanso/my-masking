package org.albertsanso.mymasking.core.masterdata.domain.port;

import org.albertsanso.mymasking.core.masterdata.domain.model.instrument.MasterInstrument;

import java.util.Optional;

public interface MasterInstrumentRepository {
    Optional<MasterInstrument> findById(String id);
    Optional<MasterInstrument> findByInstrumentId(String id);
    MasterInstrument save(MasterInstrument masterInstrument);
}
