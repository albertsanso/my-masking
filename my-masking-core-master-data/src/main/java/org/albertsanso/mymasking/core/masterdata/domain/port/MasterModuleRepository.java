package org.albertsanso.mymasking.core.masterdata.domain.port;

import org.albertsanso.mymasking.core.masterdata.domain.model.module.MasterModule;

import java.util.List;
import java.util.Optional;

public interface MasterModuleRepository {
    Optional<MasterModule> findById(String id);
    Optional<MasterModule> findByModuleId(String id);
    List<MasterModule> findByInstrumentId(String instrumentId);
    MasterModule save(MasterModule masterModule);
}
