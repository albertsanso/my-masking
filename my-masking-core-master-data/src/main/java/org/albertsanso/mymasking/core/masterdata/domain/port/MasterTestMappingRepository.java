package org.albertsanso.mymasking.core.masterdata.domain.port;

import org.albertsanso.mymasking.core.masterdata.domain.model.testmapping.MasterTestMapping;

import java.util.Optional;

public interface MasterTestMappingRepository {
    Optional<MasterTestMapping> findById(String id);
    Optional<MasterTestMapping> findByTestMappingId(String id);
    MasterTestMapping save(MasterTestMapping masterTestMapping);

}
