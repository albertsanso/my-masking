package org.albertsanso.mymasking.core.masterdata.domain.port;

import org.albertsanso.mymasking.core.masterdata.domain.model.test.MasterTest;

import java.util.List;
import java.util.Optional;

public interface MasterTestRepository {
    Optional<MasterTest> findById(String id);
    Optional<MasterTest> findByTestId(String id);
    List<MasterTest> findByInstrumentId(String instrumentId);
    List<MasterTest> findByModuleIdAndInstrumentId(String moduleId, String instrumentId);
    MasterTest save(MasterTest masterTest);
}
