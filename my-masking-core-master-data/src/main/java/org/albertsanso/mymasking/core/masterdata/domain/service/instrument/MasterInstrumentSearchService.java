package org.albertsanso.mymasking.core.masterdata.domain.service.instrument;

import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument.MasterInstrumentJpaRepository;
import org.albertsanso.mymasking.core.masterdata.domain.model.instrument.MasterInstrument;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
public class MasterInstrumentSearchService {

    private final MasterInstrumentJpaRepository masterInstrumentJpaRepository;

    @Inject
    public MasterInstrumentSearchService(MasterInstrumentJpaRepository masterInstrumentJpaRepository) {
        this.masterInstrumentJpaRepository = masterInstrumentJpaRepository;
    }

    public Optional<MasterInstrument> findById(String id) {
        return masterInstrumentJpaRepository.findById(id);
    }

    public Optional<MasterInstrument> findByInstrumentId(String id) {
        return masterInstrumentJpaRepository.findByInstrumentId(id);
    }
}
