package org.albertsanso.mymasking.core.masterdata.domain.service.module;

import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module.MasterModuleJpaRepository;
import org.albertsanso.mymasking.core.masterdata.domain.model.module.MasterModule;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Optional;

@Named
public class MasterModuleSearchService {

    private final MasterModuleJpaRepository masterModuleJpaRepository;

    @Inject
    public MasterModuleSearchService(MasterModuleJpaRepository masterModuleJpaRepository) {
        this.masterModuleJpaRepository = masterModuleJpaRepository;
    }

    public Optional<MasterModule> findById(String id) {
        return masterModuleJpaRepository.findById(id);
    }

    public Optional<MasterModule> findByModuleId(String id) {
        return masterModuleJpaRepository.findByModuleId(id);
    }

    public List<MasterModule> findModulesInInstrument(String instrumentId) {
        return masterModuleJpaRepository.findByInstrumentId(instrumentId);
    }

}
