package org.albertsanso.mymasking.core.masterdata.domain.service.test;

import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test.MasterTestJpaRepository;
import org.albertsanso.mymasking.core.masterdata.domain.model.test.MasterTest;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Optional;

@Named
public class MasterTestSearchService {

    private final MasterTestJpaRepository masterTestJpaRepository;

    @Inject
    public MasterTestSearchService(MasterTestJpaRepository masterTestJpaRepository) {
        this.masterTestJpaRepository = masterTestJpaRepository;
    }

    public Optional<MasterTest> findById(String id) {
        return masterTestJpaRepository.findById(id);
    }

    public Optional<MasterTest> findByTestId(String testId) {
        return masterTestJpaRepository.findByTestId(testId);
    }

    public List<MasterTest> findTestsInModule(String moduleId, String instrumentId) {
        return masterTestJpaRepository.findByModuleIdAndInstrumentId(moduleId, instrumentId);
    }

    public List<MasterTest> findTestsInInstrument(String instrumentId) {
        return masterTestJpaRepository.findByInstrumentId(instrumentId);
    }
}
