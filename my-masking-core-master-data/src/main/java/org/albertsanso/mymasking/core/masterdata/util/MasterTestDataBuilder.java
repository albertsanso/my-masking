package org.albertsanso.mymasking.core.masterdata.util;

import org.albertsanso.mymasking.core.masterdata.domain.model.test.MasterTest;

import java.util.UUID;

public class MasterTestDataBuilder {
    public static MasterTest createMasterTest(String name) {
        MasterTest masterTest = MasterTest.builder()
                .withId(UUID.randomUUID().toString())
                //.withTestId(UUID.randomUUID().toString())
                .withTestId(name)
                .withName(name)
                .withAbbreviation(name)
                .build();
        return masterTest;
    }
}
