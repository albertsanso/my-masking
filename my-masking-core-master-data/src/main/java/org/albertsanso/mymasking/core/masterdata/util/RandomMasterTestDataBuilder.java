package org.albertsanso.mymasking.core.masterdata.util;

import org.albertsanso.mymasking.core.masterdata.domain.model.test.MasterTest;

import java.util.Random;
import java.util.UUID;

public class RandomMasterTestDataBuilder {

    public static MasterTest createRandomMasterTest(int n) {
        MasterTest masterTest = MasterTest.builder()
                .withTestId(UUID.randomUUID().toString())
                .withId(UUID.randomUUID().toString())
                .withName(createRandomTestName(n))
                .withAbbreviation(createRandomTestName(n))
                .build();
        return masterTest;
    }

    private static String createRandomTestName(int n) {
        String randomTestNamePattern = "T%d";
        return String.format(randomTestNamePattern, getRandomNumberInts(1, n));
    }

    private static int getRandomNumberInts(int min, int max){
        Random random = new Random();
        return random.ints(min,(max+1)).findFirst().getAsInt();
    }
}
