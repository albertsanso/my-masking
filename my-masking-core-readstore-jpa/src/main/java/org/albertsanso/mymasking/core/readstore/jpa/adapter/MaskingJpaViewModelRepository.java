package org.albertsanso.mymasking.core.readstore.jpa.adapter;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;
import org.albertsanso.mymasking.core.readstore.jpa.model.MaskingJpaViewModel;
import org.albertsanso.mymasking.core.readstore.jpa.model.QMaskingJpaViewModel;
import org.albertsanso.mymasking.core.readstore.jpa.repository.MaskingJpaViewModelRepositoryHelper;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Named
public class MaskingJpaViewModelRepository {

    private final MaskingJpaViewModelRepositoryHelper maskingJpaViewModelRepositoryHelper;

    private final EntityManager entityManager;

    @Inject
    public MaskingJpaViewModelRepository(MaskingJpaViewModelRepositoryHelper maskingJpaViewModelRepositoryHelper, EntityManager entityManager) {
        this.maskingJpaViewModelRepositoryHelper = maskingJpaViewModelRepositoryHelper;
        this.entityManager = entityManager;
    }

    public Optional<MaskingJpaViewModel> findById(String id) {
        return maskingJpaViewModelRepositoryHelper.findById(id);
    }

    public List<MaskingJpaViewModel> findFor(
            String masterTestId,
            String masterInstrumentId,
            String masterModuleId) {

        final JPAQuery<MaskingJpaViewModel> query = new JPAQuery<MaskingJpaViewModel>(entityManager);
        final QMaskingJpaViewModel qMaskingJpaViewModel = QMaskingJpaViewModel.maskingJpaViewModel;

        BooleanBuilder booleanBuilder = new BooleanBuilder();
        booleanBuilder.and(qMaskingJpaViewModel.masterTestId.eq(masterTestId));
        booleanBuilder.and(qMaskingJpaViewModel.masterInstrumentId.eq(masterInstrumentId));
        if (!Objects.isNull(masterModuleId)) {
            booleanBuilder.and(qMaskingJpaViewModel.masterModuleId.eq(masterModuleId));
        }

        List<Tuple> results = query
                .distinct()
                .select()
                .from(qMaskingJpaViewModel)
                .where(booleanBuilder)
                .fetch();

        return fillFromTuples(qMaskingJpaViewModel, results);
    }

    public Optional<MaskingJpaViewModel> findOneFor(
            String masterTestId,
            String masterInstrumentId,
            String masterModuleId) {

        final JPAQuery<MaskingJpaViewModel> query = new JPAQuery<MaskingJpaViewModel>(entityManager);
        final QMaskingJpaViewModel qMaskingJpaViewModel = QMaskingJpaViewModel.maskingJpaViewModel;

        BooleanBuilder booleanBuilder = new BooleanBuilder();
        booleanBuilder.and(qMaskingJpaViewModel.masterTestId.eq(masterTestId));
        booleanBuilder.and(qMaskingJpaViewModel.masterInstrumentId.eq(masterInstrumentId));
        if (!Objects.isNull(masterModuleId)) {
            booleanBuilder.and(qMaskingJpaViewModel.masterModuleId.eq(masterModuleId));
        }

        Tuple result = query
                .distinct()
                .select(qMaskingJpaViewModel.id,
                        qMaskingJpaViewModel.masterTestId,
                        qMaskingJpaViewModel.masterModuleId,
                        qMaskingJpaViewModel.masterInstrumentId,
                        qMaskingJpaViewModel.masterTestName,
                        qMaskingJpaViewModel.masterTestAbbreviation,
                        qMaskingJpaViewModel.masterModuleName,
                        qMaskingJpaViewModel.masterInstrumentName,
                        qMaskingJpaViewModel.isMasked
                )
                .from(qMaskingJpaViewModel)
                .where(booleanBuilder)
                .fetchFirst();

        return fillFromTuple(qMaskingJpaViewModel, result);
    }

    public Optional<MaskingJpaViewModel> findOneFor(
            String masterInstrumentId,
            String masterModuleId) {

        final JPAQuery<MaskingJpaViewModel> query = new JPAQuery<MaskingJpaViewModel>(entityManager);
        final QMaskingJpaViewModel qMaskingJpaViewModel = QMaskingJpaViewModel.maskingJpaViewModel;

        BooleanBuilder booleanBuilder = new BooleanBuilder();
        booleanBuilder.and(qMaskingJpaViewModel.masterInstrumentId.eq(masterInstrumentId));
        if (!Objects.isNull(masterModuleId)) {
            booleanBuilder.and(qMaskingJpaViewModel.masterModuleId.eq(masterModuleId));
        }

        Tuple result = query
                .distinct()
                .select(qMaskingJpaViewModel.id,
                        qMaskingJpaViewModel.masterTestId,
                        qMaskingJpaViewModel.masterModuleId,
                        qMaskingJpaViewModel.masterInstrumentId,
                        qMaskingJpaViewModel.masterTestName,
                        qMaskingJpaViewModel.masterTestAbbreviation,
                        qMaskingJpaViewModel.masterModuleName,
                        qMaskingJpaViewModel.masterInstrumentName,
                        qMaskingJpaViewModel.isMasked
                )
                .from(qMaskingJpaViewModel)
                .where(booleanBuilder)
                .fetchFirst();

        return fillFromTuple(qMaskingJpaViewModel, result);
    }

    public Optional<MaskingJpaViewModel> findOneFor(String masterInstrumentId) {

        final JPAQuery<MaskingJpaViewModel> query = new JPAQuery<MaskingJpaViewModel>(entityManager);
        final QMaskingJpaViewModel qMaskingJpaViewModel = QMaskingJpaViewModel.maskingJpaViewModel;

        BooleanBuilder booleanBuilder = new BooleanBuilder();
        booleanBuilder.and(qMaskingJpaViewModel.masterInstrumentId.eq(masterInstrumentId));

        Tuple result = query
                .distinct()
                .select(qMaskingJpaViewModel.id,
                        qMaskingJpaViewModel.masterTestId,
                        qMaskingJpaViewModel.masterModuleId,
                        qMaskingJpaViewModel.masterInstrumentId,
                        qMaskingJpaViewModel.masterTestName,
                        qMaskingJpaViewModel.masterTestAbbreviation,
                        qMaskingJpaViewModel.masterModuleName,
                        qMaskingJpaViewModel.masterInstrumentName,
                        qMaskingJpaViewModel.isMasked
                )
                .from(qMaskingJpaViewModel)
                .where(booleanBuilder)
                .fetchFirst();

        return fillFromTuple(qMaskingJpaViewModel, result);
    }

    private static List<MaskingJpaViewModel> fillFromTuples(QMaskingJpaViewModel qMaskingJpaViewModel, List<Tuple> results) {
        return results
                .stream()
                .map(tuple -> {
                    return fillFromTuple(qMaskingJpaViewModel, tuple).get();
                })
                .collect(Collectors.toList());
    }

    private static Optional<MaskingJpaViewModel> fillFromTuple(QMaskingJpaViewModel qMaskingJpaViewModel, Tuple tuple) {

        if (Objects.isNull(tuple)) {
            return Optional.empty();
        }

        MaskingJpaViewModel maskingJpaViewModel = new MaskingJpaViewModel();
        maskingJpaViewModel.setId(tuple.get(qMaskingJpaViewModel.id));
        maskingJpaViewModel.setMasterInstrumentId(tuple.get(qMaskingJpaViewModel.masterInstrumentId));
        maskingJpaViewModel.setMasterModuleId(tuple.get(qMaskingJpaViewModel.masterModuleId));
        maskingJpaViewModel.setMasterTestId(tuple.get(qMaskingJpaViewModel.masterTestId));

        maskingJpaViewModel.setMasterTestName(tuple.get(qMaskingJpaViewModel.masterTestName));
        maskingJpaViewModel.setMasterTestAbbreviation(tuple.get(qMaskingJpaViewModel.masterTestAbbreviation));

        maskingJpaViewModel.setMasterModuleName(tuple.get(qMaskingJpaViewModel.masterModuleName));

        maskingJpaViewModel.setMasterInstrumentName(tuple.get(qMaskingJpaViewModel.masterInstrumentName));

        maskingJpaViewModel.setMasked(tuple.get(qMaskingJpaViewModel.isMasked));

        return Optional.of(maskingJpaViewModel);
    }

    public MaskingJpaViewModel save(MaskingJpaViewModel maskingJpaViewModel) {
        return maskingJpaViewModelRepositoryHelper.save(maskingJpaViewModel);
    }
}
