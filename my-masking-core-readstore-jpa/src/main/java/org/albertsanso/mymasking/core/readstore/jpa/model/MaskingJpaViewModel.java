package org.albertsanso.mymasking.core.readstore.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="masking_readstore_masking_view")
public class MaskingJpaViewModel {

    @Id
    private String id;

    private String masterTestId;

    private String masterModuleId;

    private String masterInstrumentId;

    private String masterTestName;

    private String masterTestAbbreviation;

    private String masterModuleName;

    private String masterInstrumentName;

    private boolean isMasked;

    public MaskingJpaViewModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMasterTestId() {
        return masterTestId;
    }

    public void setMasterTestId(String masterTestId) {
        this.masterTestId = masterTestId;
    }

    public String getMasterModuleId() {
        return masterModuleId;
    }

    public void setMasterModuleId(String masterModuleId) {
        this.masterModuleId = masterModuleId;
    }

    public String getMasterInstrumentId() {
        return masterInstrumentId;
    }

    public void setMasterInstrumentId(String masterInstrumentId) {
        this.masterInstrumentId = masterInstrumentId;
    }

    public String getMasterTestName() {
        return masterTestName;
    }

    public void setMasterTestName(String masterTestName) {
        this.masterTestName = masterTestName;
    }

    public String getMasterTestAbbreviation() {
        return masterTestAbbreviation;
    }

    public void setMasterTestAbbreviation(String masterTestAbbreviation) {
        this.masterTestAbbreviation = masterTestAbbreviation;
    }

    public String getMasterModuleName() {
        return masterModuleName;
    }

    public void setMasterModuleName(String masterModuleName) {
        this.masterModuleName = masterModuleName;
    }

    public String getMasterInstrumentName() {
        return masterInstrumentName;
    }

    public void setMasterInstrumentName(String masterInstrumentName) {
        this.masterInstrumentName = masterInstrumentName;
    }

    public boolean isMasked() {
        return isMasked;
    }

    public void setMasked(boolean masked) {
        isMasked = masked;
    }
}
