package org.albertsanso.mymasking.core.readstore.jpa.repository;

import org.albertsanso.mymasking.core.readstore.jpa.model.MaskingJpaViewModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface MaskingJpaViewModelRepositoryHelper extends CrudRepository<MaskingJpaViewModel, String> {
    Optional<MaskingJpaViewModel> findById(String id);
    /*
    Optional<MaskingJpaViewModel> findByMasterTestIdAndMasterModuleIdAndMasterInstrumentId(String testid, String moduleId, String instrumentId);
    List<MaskingJpaViewModel> findFor(String testid, String moduleId, String instrumentId);
     */
}
