package org.albertsanso.mymasking.core.readstore.projection.instrument;

import org.albertsanso.commons.event.DomainEventSubscriber;
import org.albertsanso.mymasking.core.domain.event.instrument.MaskingInstrumentDirectlyMaskedEvent;
import org.albertsanso.mymasking.core.masterdata.domain.model.instrument.MasterInstrument;
import org.albertsanso.mymasking.core.masterdata.domain.service.instrument.MasterInstrumentSearchService;
import org.albertsanso.mymasking.core.readstore.jpa.adapter.MaskingJpaViewModelRepository;
import org.albertsanso.mymasking.core.readstore.jpa.model.MaskingJpaViewModel;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;
import java.util.UUID;

@Named
public class MaskingInstrumentDirectlyMaskedEventSubscriber extends DomainEventSubscriber<MaskingInstrumentDirectlyMaskedEvent> {

    private final MaskingJpaViewModelRepository maskingJpaViewModelRepository;

    private final MasterInstrumentSearchService masterInstrumentSearchService;

    @Inject
    public MaskingInstrumentDirectlyMaskedEventSubscriber(MaskingJpaViewModelRepository maskingJpaViewModelRepository, MasterInstrumentSearchService masterInstrumentSearchService) {
        this.maskingJpaViewModelRepository = maskingJpaViewModelRepository;
        this.masterInstrumentSearchService = masterInstrumentSearchService;
    }

    @Override
    public void handle(MaskingInstrumentDirectlyMaskedEvent maskingInstrumentDirectlyMaskedEvent) {

        String masterInstrumentId = null;
        MasterInstrument masterInstrument = null;

        final String instrumentId = maskingInstrumentDirectlyMaskedEvent.getInstrumentId();
        final Optional<MasterInstrument> optionalMasterInstrument = masterInstrumentSearchService.findByInstrumentId(instrumentId);
        if (optionalMasterInstrument.isPresent()) {
            masterInstrument = optionalMasterInstrument.get();
            masterInstrumentId = masterInstrument.getId();
        }

        final Optional<MaskingJpaViewModel> optionalJpaViewModel = maskingJpaViewModelRepository.findOneFor(masterInstrumentId);

        MaskingJpaViewModel maskingJpaViewModel;
        if (optionalJpaViewModel.isPresent()) {
            maskingJpaViewModel = optionalJpaViewModel.get();
        }
        else {
            maskingJpaViewModel = new MaskingJpaViewModel();
            maskingJpaViewModel.setId(UUID.randomUUID().toString());
            maskingJpaViewModel.setMasterInstrumentId(masterInstrumentId);
            maskingJpaViewModel.setMasked(true);
        }

        if (optionalMasterInstrument.isPresent()) {
            maskingJpaViewModel.setMasterInstrumentName(optionalMasterInstrument.get().getName());
        }

        maskingJpaViewModelRepository.save(maskingJpaViewModel);
    }
}
