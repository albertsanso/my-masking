package org.albertsanso.mymasking.core.readstore.projection.module;

import org.albertsanso.commons.event.DomainEventSubscriber;
import org.albertsanso.mymasking.core.domain.event.module.MaskingModuleIndirectlyMaskedEvent;
import org.albertsanso.mymasking.core.masterdata.domain.model.instrument.MasterInstrument;
import org.albertsanso.mymasking.core.masterdata.domain.model.module.MasterModule;
import org.albertsanso.mymasking.core.masterdata.domain.service.instrument.MasterInstrumentSearchService;
import org.albertsanso.mymasking.core.masterdata.domain.service.module.MasterModuleSearchService;
import org.albertsanso.mymasking.core.readstore.jpa.adapter.MaskingJpaViewModelRepository;
import org.albertsanso.mymasking.core.readstore.jpa.model.MaskingJpaViewModel;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;
import java.util.UUID;

@Named
public class MaskingModuleIndirectlyMaskedEventSubscriber extends DomainEventSubscriber<MaskingModuleIndirectlyMaskedEvent> {

    private final MaskingJpaViewModelRepository maskingJpaViewModelRepository;

    private final MasterInstrumentSearchService masterInstrumentSearchService;

    private final MasterModuleSearchService masterModuleSearchService;

    @Inject
    public MaskingModuleIndirectlyMaskedEventSubscriber(MaskingJpaViewModelRepository maskingJpaViewModelRepository, MasterInstrumentSearchService masterInstrumentSearchService, MasterModuleSearchService masterModuleSearchService) {
        this.maskingJpaViewModelRepository = maskingJpaViewModelRepository;
        this.masterInstrumentSearchService = masterInstrumentSearchService;
        this.masterModuleSearchService = masterModuleSearchService;
    }

    @Override
    public void handle(MaskingModuleIndirectlyMaskedEvent maskingModuleIndirectlyMaskedEvent) {

        String masterModuleId = null;
        String masterInstrumentId = null;

        MasterInstrument masterInstrument = null;
        MasterModule masterModule = null;

        final String instrumentId = maskingModuleIndirectlyMaskedEvent.getInstrumentId();
        final Optional<MasterInstrument> optionalMasterInstrument = masterInstrumentSearchService.findByInstrumentId(instrumentId);
        if (optionalMasterInstrument.isPresent()) {
            masterInstrument = optionalMasterInstrument.get();
            masterInstrumentId = masterInstrument.getId();
        }

        final String moduleId = maskingModuleIndirectlyMaskedEvent.getModuleId();
        Optional<MasterModule> optionalMasterModule = masterModuleSearchService.findByModuleId(moduleId);
        if (optionalMasterModule.isPresent()) {
            masterModule = optionalMasterModule.get();
            masterModuleId = masterModule.getId();
        }

        final Optional<MaskingJpaViewModel> optionalJpaViewModel = maskingJpaViewModelRepository.findOneFor(masterInstrumentId, masterModuleId);

        MaskingJpaViewModel maskingJpaViewModel;
        if (optionalJpaViewModel.isPresent()) {
            maskingJpaViewModel = optionalJpaViewModel.get();
        }
        else {
            maskingJpaViewModel = new MaskingJpaViewModel();
            maskingJpaViewModel.setId(UUID.randomUUID().toString());
            maskingJpaViewModel.setMasterModuleId(masterModuleId);
            maskingJpaViewModel.setMasterInstrumentId(masterInstrumentId);
            maskingJpaViewModel.setMasked(true);
        }

        if (optionalMasterModule.isPresent() && optionalMasterModule.isPresent()) {
            maskingJpaViewModel.setMasterModuleName(optionalMasterModule.get().getName());
        }
        if (optionalMasterInstrument.isPresent()) {
            maskingJpaViewModel.setMasterInstrumentName(optionalMasterInstrument.get().getName());
        }

        maskingJpaViewModelRepository.save(maskingJpaViewModel);
    }
}
