package org.albertsanso.mymasking.core.readstore.projection.test;

import org.albertsanso.commons.event.DomainEventSubscriber;
import org.albertsanso.mymasking.core.domain.event.test.MaskingTestDirectlyMaskedEvent;
import org.albertsanso.mymasking.core.masterdata.domain.model.instrument.MasterInstrument;
import org.albertsanso.mymasking.core.masterdata.domain.model.module.MasterModule;
import org.albertsanso.mymasking.core.masterdata.domain.model.test.MasterTest;
import org.albertsanso.mymasking.core.masterdata.domain.service.instrument.MasterInstrumentSearchService;
import org.albertsanso.mymasking.core.masterdata.domain.service.module.MasterModuleSearchService;
import org.albertsanso.mymasking.core.masterdata.domain.service.test.MasterTestSearchService;
import org.albertsanso.mymasking.core.readstore.jpa.adapter.MaskingJpaViewModelRepository;
import org.albertsanso.mymasking.core.readstore.jpa.model.MaskingJpaViewModel;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;
import java.util.UUID;

@Named
public class MaskingTestDirectlyMaskedEventSubscriber extends DomainEventSubscriber<MaskingTestDirectlyMaskedEvent> {

    private final MaskingJpaViewModelRepository maskingJpaViewModelRepository;

    private final MasterInstrumentSearchService masterInstrumentSearchService;

    private final MasterModuleSearchService masterModuleSearchService;

    private final MasterTestSearchService masterTestSearchService;

    @Inject
    public MaskingTestDirectlyMaskedEventSubscriber(MaskingJpaViewModelRepository maskingJpaViewModelRepository, MasterInstrumentSearchService masterInstrumentSearchService, MasterModuleSearchService masterModuleSearchService, MasterTestSearchService masterTestSearchService) {
        this.maskingJpaViewModelRepository = maskingJpaViewModelRepository;
        this.masterInstrumentSearchService = masterInstrumentSearchService;
        this.masterModuleSearchService = masterModuleSearchService;
        this.masterTestSearchService = masterTestSearchService;
    }

    @Override
    public void handle(MaskingTestDirectlyMaskedEvent maskingTestDirectlyMaskedEvent) {

        String masterTestId = null;
        String masterModuleId = null;
        String masterInstrumentId = null;

        MasterInstrument masterInstrument = null;
        MasterModule masterModule = null;
        MasterTest masterTest = null;

        final String instrumentId = maskingTestDirectlyMaskedEvent.getInstrumentId();
        final Optional<MasterInstrument> optionalMasterInstrument = masterInstrumentSearchService.findByInstrumentId(instrumentId);
        if (optionalMasterInstrument.isPresent()) {
            masterInstrument = optionalMasterInstrument.get();
            masterInstrumentId = masterInstrument.getId();
        }

        final Optional<String> optionalModuleId = maskingTestDirectlyMaskedEvent.getModuleId();
        Optional<MasterModule> optionalMasterModule = Optional.empty();
        if (optionalModuleId.isPresent()) {
            optionalMasterModule = masterModuleSearchService.findByModuleId(optionalModuleId.get());
            if (optionalMasterModule.isPresent()) {
                masterModule = optionalMasterModule.get();
                masterModuleId = masterModule.getId();
            }
        }

        final String testId = maskingTestDirectlyMaskedEvent.getTestId();
        final Optional<MasterTest> optionalMasterTest = masterTestSearchService.findByTestId(testId);
        if (optionalMasterTest.isPresent()) {
            masterTest = optionalMasterTest.get();
            masterTestId = masterTest.getId();
        }

        final Optional<MaskingJpaViewModel> optionalJpaViewModel = maskingJpaViewModelRepository.findOneFor(masterTestId, masterInstrumentId, masterModuleId);

        MaskingJpaViewModel maskingJpaViewModel;
        if (optionalJpaViewModel.isPresent()) {
            maskingJpaViewModel = optionalJpaViewModel.get();
        }
        else {
            maskingJpaViewModel = new MaskingJpaViewModel();
            maskingJpaViewModel.setId(UUID.randomUUID().toString());
            maskingJpaViewModel.setMasterTestId(masterTestId);
            maskingJpaViewModel.setMasterModuleId(masterModuleId);
            maskingJpaViewModel.setMasterInstrumentId(masterInstrumentId);
            maskingJpaViewModel.setMasked(true);
        }

        if (optionalMasterTest.isPresent()) {
            maskingJpaViewModel.setMasterTestName(optionalMasterTest.get().getName());
            maskingJpaViewModel.setMasterTestAbbreviation(optionalMasterTest.get().getAbbreviation());
        }
        if (optionalMasterModule.isPresent() && optionalMasterModule.isPresent()) {
            maskingJpaViewModel.setMasterModuleName(optionalMasterModule.get().getName());
        }
        if (optionalMasterInstrument.isPresent()) {
            maskingJpaViewModel.setMasterInstrumentName(optionalMasterInstrument.get().getName());
        }

        maskingJpaViewModelRepository.save(maskingJpaViewModel);
    }
}
