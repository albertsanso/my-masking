package org.albertsanso.mymasking.core.repository.jpa.adapter;

import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrument;
import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrumentId;
import org.albertsanso.mymasking.core.domain.port.InstrumentRepository;
import org.albertsanso.mymasking.core.repository.jpa.mapper.InstrumentToJpaMaskingInstrumentMapper;
import org.albertsanso.mymasking.core.repository.jpa.mapper.JpaMaskingInstrumentToInstrumentMapper;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingInstrument;
import org.albertsanso.mymasking.core.repository.jpa.repository.InstrumentJpaRepositoryHelper;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
public class InstrumentJpaRepository implements InstrumentRepository {

    private final InstrumentJpaRepositoryHelper instrumentJpaRepositoryHelper;
    private final JpaMaskingInstrumentToInstrumentMapper jpaMaskingInstrumentToInstrumentMapper;
    private final InstrumentToJpaMaskingInstrumentMapper instrumentToJpaMaskingInstrumentMapper;

    @Inject
    public InstrumentJpaRepository(InstrumentJpaRepositoryHelper instrumentJpaRepositoryHelper, JpaMaskingInstrumentToInstrumentMapper jpaMaskingInstrumentToInstrumentMapper, InstrumentToJpaMaskingInstrumentMapper instrumentToJpaMaskingInstrumentMapper) {
        this.instrumentJpaRepositoryHelper = instrumentJpaRepositoryHelper;
        this.jpaMaskingInstrumentToInstrumentMapper = jpaMaskingInstrumentToInstrumentMapper;
        this.instrumentToJpaMaskingInstrumentMapper = instrumentToJpaMaskingInstrumentMapper;
    }

    @Override
    public Optional<MaskingInstrument> findById(MaskingInstrumentId id) {
        Optional<JpaMaskingInstrument> jpaMaskingInstrument = instrumentJpaRepositoryHelper.findById(id.getId().toString());
        if (jpaMaskingInstrument.isPresent()) {
            MaskingInstrument maskingInstrument = jpaMaskingInstrumentToInstrumentMapper.apply(jpaMaskingInstrument.get());
            return Optional.of(maskingInstrument);
        }
        return Optional.empty();
    }

    @Override
    public Optional<MaskingInstrument> findByInstrumentId(String instrumentId) {
        instrumentId = instrumentId.replace("||", ".");

        Optional<JpaMaskingInstrument> jpaMaskingInstrument = instrumentJpaRepositoryHelper.findByInstrumentId(instrumentId);
        if (jpaMaskingInstrument.isPresent()) {
            MaskingInstrument maskingInstrument = jpaMaskingInstrumentToInstrumentMapper.apply(jpaMaskingInstrument.get());
            return Optional.of(maskingInstrument);
        }
        return Optional.empty();
    }

    @Override
    public MaskingInstrument save(MaskingInstrument maskingInstrument) {
        JpaMaskingInstrument jpaMaskingInstrument = instrumentToJpaMaskingInstrumentMapper.apply(maskingInstrument);
        instrumentJpaRepositoryHelper.save(jpaMaskingInstrument);
        return jpaMaskingInstrumentToInstrumentMapper.apply(jpaMaskingInstrument);
    }
}
