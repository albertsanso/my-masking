package org.albertsanso.mymasking.core.repository.jpa.adapter;

import org.albertsanso.mymasking.core.domain.model.MaskingEntityType;
import org.albertsanso.mymasking.core.domain.model.relation.MaskingRelation;
import org.albertsanso.mymasking.core.domain.port.MaskingRelationRepository;
import org.albertsanso.mymasking.core.repository.jpa.mapper.JpaMaskingRelationToMaskingRelationMapper;
import org.albertsanso.mymasking.core.repository.jpa.mapper.MaskingRelationToJpaMaskingRelationMapper;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingRelation;
import org.albertsanso.mymasking.core.repository.jpa.repository.MaskingRelationJpaRepositoryHelper;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Named
public class MaskingRelationJpaRepository implements MaskingRelationRepository {

    private final MaskingRelationJpaRepositoryHelper maskingRelationJpaRepositoryHelper;
    private final JpaMaskingRelationToMaskingRelationMapper jpaMaskingRelationToMaskingRelationMapper;
    private final MaskingRelationToJpaMaskingRelationMapper maskingRelationToJpaMaskingRelationMapper;

    @Inject
    public MaskingRelationJpaRepository(
            MaskingRelationJpaRepositoryHelper maskingRelationJpaRepositoryHelper,
            JpaMaskingRelationToMaskingRelationMapper jpaMaskingRelationToMaskingRelationMapper, MaskingRelationToJpaMaskingRelationMapper maskingRelationToJpaMaskingRelationMapper) {
        this.maskingRelationJpaRepositoryHelper = maskingRelationJpaRepositoryHelper;
        this.jpaMaskingRelationToMaskingRelationMapper = jpaMaskingRelationToMaskingRelationMapper;
        this.maskingRelationToJpaMaskingRelationMapper = maskingRelationToJpaMaskingRelationMapper;
    }

    @Override
    public Optional<MaskingRelation> findById(UUID id) {
        Optional<JpaMaskingRelation> jpaMaskingRelation = maskingRelationJpaRepositoryHelper.findById(id.toString());

        if (jpaMaskingRelation.isPresent()) {
            MaskingRelation maskingRelation = jpaMaskingRelationToMaskingRelationMapper.apply(jpaMaskingRelation.get());
            return Optional.of(maskingRelation);
        }

        return Optional.empty();
    }

    @Override
    public List<MaskingRelation> findByMaskingEntityIdAndType(String externalId, MaskingEntityType type) {
        List<JpaMaskingRelation> jpaMaskingRelations = maskingRelationJpaRepositoryHelper.findByMaskingEntityIdAndType(externalId, type);
        return mapJpaToDomainList(jpaMaskingRelations);
    }

    @Override
    public List<MaskingRelation> findByMaskingEntityIdAndTypeAndRefererType(String maskingEntityId, MaskingEntityType type, MaskingEntityType refererType) {
        List<JpaMaskingRelation> jpaMaskingRelations = maskingRelationJpaRepositoryHelper.findByMaskingEntityIdAndTypeAndRefererType(maskingEntityId, type, refererType);
        return mapJpaToDomainList(jpaMaskingRelations);
    }

    @Override
    public MaskingRelation findByMaskingEntityIdAndTypeAndRefererEntityIdAndRefererType(String maskingEntityId, MaskingEntityType type, String refererEntityId, MaskingEntityType refererType) {
        JpaMaskingRelation jpaMaskingRelation = maskingRelationJpaRepositoryHelper.findByMaskingEntityIdAndTypeAndRefererEntityIdAndRefererType(maskingEntityId, type, refererEntityId, refererType);
        return jpaMaskingRelationToMaskingRelationMapper.apply(jpaMaskingRelation);
    }

    private List<MaskingRelation> mapJpaToDomainList(List<JpaMaskingRelation> jpaMaskingRelationList) {
        return jpaMaskingRelationList.stream()
                .map( jpaMaskingRelation -> jpaMaskingRelationToMaskingRelationMapper.apply(jpaMaskingRelation))
                .collect(Collectors.toList());
    }

    @Override
    public MaskingRelation save(MaskingRelation maskingRelation) {
        JpaMaskingRelation jpaMaskingRelation = maskingRelationToJpaMaskingRelationMapper.apply(maskingRelation);
        maskingRelationJpaRepositoryHelper.save(jpaMaskingRelation);
        return jpaMaskingRelationToMaskingRelationMapper.apply(jpaMaskingRelation);
    }
}
