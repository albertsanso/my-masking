package org.albertsanso.mymasking.core.repository.jpa.adapter;

import org.albertsanso.mymasking.core.domain.model.module.MaskingModule;
import org.albertsanso.mymasking.core.domain.model.module.MaskingModuleId;
import org.albertsanso.mymasking.core.domain.port.ModuleRepository;
import org.albertsanso.mymasking.core.repository.jpa.mapper.JpaMaskingModuleToModuleMapper;
import org.albertsanso.mymasking.core.repository.jpa.mapper.ModuleToJpaMaskingModuleMapper;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingModule;
import org.albertsanso.mymasking.core.repository.jpa.repository.ModuleJpaRepositoryHelper;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
public class ModuleJpaRepository implements ModuleRepository {

    private final JpaMaskingModuleToModuleMapper jpaMaskingModuleToModuleMapper;
    private final ModuleToJpaMaskingModuleMapper moduleToJpaMaskingModuleMapper;
    private final ModuleJpaRepositoryHelper moduleJpaRepositoryHelper;

    @Inject
    public ModuleJpaRepository(JpaMaskingModuleToModuleMapper jpaMaskingModuleToModuleMapper, ModuleToJpaMaskingModuleMapper moduleToJpaMaskingModuleMapper, ModuleJpaRepositoryHelper moduleJpaRepositoryHelper) {
        this.jpaMaskingModuleToModuleMapper = jpaMaskingModuleToModuleMapper;
        this.moduleToJpaMaskingModuleMapper = moduleToJpaMaskingModuleMapper;
        this.moduleJpaRepositoryHelper = moduleJpaRepositoryHelper;
    }

    @Override
    public Optional<MaskingModule> findById(MaskingModuleId id) {
        Optional<JpaMaskingModule> jpaMaskingModule = moduleJpaRepositoryHelper.findById(id.getId().toString());
        if (jpaMaskingModule.isPresent()) {
            MaskingModule maskingModule = jpaMaskingModuleToModuleMapper.apply(jpaMaskingModule.get());
            return Optional.of(maskingModule);
        }
        return Optional.empty();
    }

    @Override
    public Optional<MaskingModule> findOneForIds(String moduleId, String instrumentId) {
        moduleId = moduleId.replace("||", ".");
        instrumentId = instrumentId.replace("||", ".");

        Optional<JpaMaskingModule> jpaMaskingModule = moduleJpaRepositoryHelper.findByModuleIdAndInstrumentId(moduleId, instrumentId);
        if (jpaMaskingModule.isPresent()) {
            MaskingModule maskingModule = jpaMaskingModuleToModuleMapper.apply(jpaMaskingModule.get());
            return Optional.of(maskingModule);
        }
        return Optional.empty();
    }

    @Override
    public MaskingModule save(MaskingModule maskingModule) {
        JpaMaskingModule jpaMaskingModule = moduleToJpaMaskingModuleMapper.apply(maskingModule);
        try {
            moduleJpaRepositoryHelper.save(jpaMaskingModule);
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
        MaskingModule maskingModule1 = jpaMaskingModuleToModuleMapper.apply(jpaMaskingModule);
        return maskingModule1;
    }
}
