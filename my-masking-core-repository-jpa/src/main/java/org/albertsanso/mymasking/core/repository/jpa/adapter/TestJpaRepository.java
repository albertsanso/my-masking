package org.albertsanso.mymasking.core.repository.jpa.adapter;

import org.albertsanso.mymasking.core.domain.model.test.MaskingTest;
import org.albertsanso.mymasking.core.domain.model.test.MaskingTestId;
import org.albertsanso.mymasking.core.domain.port.TestRepository;
import org.albertsanso.mymasking.core.repository.jpa.mapper.JpaMaskingTestToTestMapper;
import org.albertsanso.mymasking.core.repository.jpa.mapper.TestToJpaMaskingTestMapper;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingTest;
import org.albertsanso.mymasking.core.repository.jpa.repository.TestJpaRepositoryHelper;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Named
public class TestJpaRepository implements TestRepository {

    private final TestJpaRepositoryHelper testJpaRepositoryHelper;
    private final JpaMaskingTestToTestMapper jpaMaskingTestToTestMapper;
    private final TestToJpaMaskingTestMapper testToJpaMaskingTestMapper;

    @Inject
    public TestJpaRepository(TestJpaRepositoryHelper testJpaRepositoryHelper, JpaMaskingTestToTestMapper jpaMaskingTestToTestMapper, TestToJpaMaskingTestMapper testToJpaMaskingTestMapper) {
        this.testJpaRepositoryHelper = testJpaRepositoryHelper;
        this.jpaMaskingTestToTestMapper = jpaMaskingTestToTestMapper;
        this.testToJpaMaskingTestMapper = testToJpaMaskingTestMapper;
    }

    @Override
    public Optional<MaskingTest> findById(MaskingTestId id) {
        Optional<JpaMaskingTest> jpaMaskingTest = testJpaRepositoryHelper.findById(id.getId().toString());
        if (jpaMaskingTest.isPresent()) {
            MaskingTest maskingTest = jpaMaskingTestToTestMapper.apply(jpaMaskingTest.get());
            return Optional.of(maskingTest);
        }
        return Optional.empty();
    }

    @Override
    public Optional<MaskingTest> findOneForIds(String testId, String instrumentId, Optional<String> moduleId) {

        testId = testId.replace("||", ".");
        instrumentId = instrumentId.replace("||", ".");
        if (moduleId.isPresent()) {
            moduleId = Optional.of(moduleId.orElse(null).replace("||", "."));
        }

        Optional<JpaMaskingTest> jpaMaskingTest = testJpaRepositoryHelper.findByTestIdAndInstrumentIdAndModuleId(testId, instrumentId, moduleId);
        if (jpaMaskingTest.isPresent()) {
            MaskingTest maskingTest = jpaMaskingTestToTestMapper.apply(jpaMaskingTest.get());
            return Optional.of(maskingTest);
        }
        return Optional.empty();
    }

    @Override
    public List<MaskingTest> findByTestId(String testId) {
        List<JpaMaskingTest> jpaMaskingTestList = testJpaRepositoryHelper.findByTestId(testId);
        return jpaMaskingTestList.stream()
                .map(jpaMaskingTest -> { return jpaMaskingTestToTestMapper.apply(jpaMaskingTest); })
                .collect(Collectors.toList());
    }


    @Override
    public MaskingTest save(MaskingTest maskingTest) {
        JpaMaskingTest jpaMaskingTest = testToJpaMaskingTestMapper.apply(maskingTest);
        jpaMaskingTest = testJpaRepositoryHelper.save(jpaMaskingTest);
        MaskingTest maskingTest1 = jpaMaskingTestToTestMapper.apply(jpaMaskingTest);
        return maskingTest1;
    }

    public static void main(String[] args) {
        //Optional<String> moduleId = Optional.of("572||1||1");
        Optional<String> moduleId = Optional.empty();
        System.out.println(Optional.empty().orElse(null));
    }
}
