package org.albertsanso.mymasking.core.repository.jpa.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.albertsanso.mymasking.core.domain.model.MaskingEntity;

import java.util.*;
import java.util.stream.Collectors;

public abstract class EntityToJpaMaskingEntityMapper {

    private final ObjectMapper objectMapper;

    public EntityToJpaMaskingEntityMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    protected String extractMaskingActs(MaskingEntity entity) {
        String maskingActivityJson = "";
        try {
            maskingActivityJson = objectMapper.writeValueAsString(entity.getMaskingActs());
        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return maskingActivityJson;
    }
}
