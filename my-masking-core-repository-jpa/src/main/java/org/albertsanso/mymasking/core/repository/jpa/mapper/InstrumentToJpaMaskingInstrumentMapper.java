package org.albertsanso.mymasking.core.repository.jpa.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrument;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingInstrument;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.function.Function;

@Named
public class InstrumentToJpaMaskingInstrumentMapper extends EntityToJpaMaskingEntityMapper implements Function<MaskingInstrument, JpaMaskingInstrument> {

    @Inject
    public InstrumentToJpaMaskingInstrumentMapper(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public JpaMaskingInstrument apply(MaskingInstrument maskingInstrument) {
        String maskingActivityJson = extractMaskingActs(maskingInstrument);
        return new JpaMaskingInstrument(
                maskingInstrument.getId().getId().toString(),
                maskingActivityJson,
                maskingInstrument.getInstrumentId().replace("||", ".")
                );
    }
}
