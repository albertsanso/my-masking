package org.albertsanso.mymasking.core.repository.jpa.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.albertsanso.mymasking.core.domain.model.MaskingAct;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class JpaMaskingEntityToEntityMapper {

    private final ObjectMapper objectMapper;

    public JpaMaskingEntityToEntityMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    protected List<MaskingAct> extractMaskingActs(JpaMaskingEntity entity) {

        List<MaskingAct> maskingActsList = Collections.EMPTY_LIST;
        try {
            MaskingAct[] maskingActs = objectMapper.readValue(entity.getMaskingActs(), MaskingAct[].class);
            maskingActsList = new ArrayList<>(Arrays.asList(maskingActs));
        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return maskingActsList;
    }
}
