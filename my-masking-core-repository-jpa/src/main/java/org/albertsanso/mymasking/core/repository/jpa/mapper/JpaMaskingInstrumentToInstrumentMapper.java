package org.albertsanso.mymasking.core.repository.jpa.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.albertsanso.mymasking.core.domain.model.MaskingAct;
import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrument;
import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrumentId;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingInstrument;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;

@Named
public class JpaMaskingInstrumentToInstrumentMapper extends JpaMaskingEntityToEntityMapper implements Function<JpaMaskingInstrument, MaskingInstrument> {

    @Inject
    public JpaMaskingInstrumentToInstrumentMapper(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public MaskingInstrument apply(JpaMaskingInstrument jpaMaskingInstrument) {

        if (Objects.isNull(jpaMaskingInstrument)) return null;

        List<MaskingAct> maskingActsList = extractMaskingActs(jpaMaskingInstrument);

        return MaskingInstrument.builder(jpaMaskingInstrument.getId().replace(".", "||"))
                .withMaskingActs(maskingActsList)
                .WithId(MaskingInstrumentId.of(UUID.fromString(jpaMaskingInstrument.getId())))
                .build();
    }
}
