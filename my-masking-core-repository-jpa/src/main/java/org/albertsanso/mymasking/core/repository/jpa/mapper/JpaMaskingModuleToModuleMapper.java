package org.albertsanso.mymasking.core.repository.jpa.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.albertsanso.mymasking.core.domain.model.MaskingAct;
import org.albertsanso.mymasking.core.domain.model.module.MaskingModule;
import org.albertsanso.mymasking.core.domain.model.module.MaskingModuleId;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingModule;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;

@Named
public class JpaMaskingModuleToModuleMapper extends JpaMaskingEntityToEntityMapper implements Function<JpaMaskingModule, MaskingModule> {

    @Inject
    public JpaMaskingModuleToModuleMapper(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public MaskingModule apply(JpaMaskingModule jpaMaskingModule) {

        if (Objects.isNull(jpaMaskingModule)) return null;

        List<MaskingAct> maskingActsList = extractMaskingActs(jpaMaskingModule);

        return MaskingModule.builder(
                jpaMaskingModule.getModuleId().replace(".", "||"),
                jpaMaskingModule.getInstrumentId().replace(".", "||"))
                    .withMaskingActs(maskingActsList)
                    .withId(MaskingModuleId.of(UUID.fromString(jpaMaskingModule.getId())))
                    .build();

    }
}
