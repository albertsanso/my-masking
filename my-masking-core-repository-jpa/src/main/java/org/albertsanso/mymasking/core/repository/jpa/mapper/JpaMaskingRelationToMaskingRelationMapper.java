package org.albertsanso.mymasking.core.repository.jpa.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.albertsanso.mymasking.core.domain.model.MaskingAct;
import org.albertsanso.mymasking.core.domain.model.relation.MaskingRelation;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingRelation;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

@Named
public class JpaMaskingRelationToMaskingRelationMapper implements Function<JpaMaskingRelation, MaskingRelation> {

    private final ObjectMapper objectMapper;

    @Inject
    public JpaMaskingRelationToMaskingRelationMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public MaskingRelation apply(JpaMaskingRelation jpaMaskingRelation) {

        if (Objects.isNull(jpaMaskingRelation)) return null;

        List<MaskingAct> maskingActsList = null;
        try {
            MaskingAct[] maskingActs = objectMapper.readValue(jpaMaskingRelation.getMaskingActs(), MaskingAct[].class);
            maskingActsList = Arrays.asList(maskingActs);
        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return new MaskingRelation(
                jpaMaskingRelation.getId(),
                jpaMaskingRelation.getMaskingEntityId(),
                jpaMaskingRelation.getType(),
                jpaMaskingRelation.getRefererEntityId(),
                jpaMaskingRelation.getRefererType(),
                maskingActsList
        );
    }
}
