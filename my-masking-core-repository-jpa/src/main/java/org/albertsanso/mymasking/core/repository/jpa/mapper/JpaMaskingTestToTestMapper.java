package org.albertsanso.mymasking.core.repository.jpa.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.albertsanso.mymasking.core.domain.model.MaskingAct;
import org.albertsanso.mymasking.core.domain.model.test.MaskingTest;
import org.albertsanso.mymasking.core.domain.model.test.MaskingTestId;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingTest;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;

@Named
public class JpaMaskingTestToTestMapper extends JpaMaskingEntityToEntityMapper implements Function<JpaMaskingTest, MaskingTest> {

    @Inject
    public JpaMaskingTestToTestMapper(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public MaskingTest apply(JpaMaskingTest jpaMaskingTest) {

        if (Objects.isNull(jpaMaskingTest)) return null;

        List<MaskingAct> maskingActsList = extractMaskingActs(jpaMaskingTest);

        MaskingTest.TestBuilder builder = MaskingTest.builder(
                jpaMaskingTest.getTestId(),
                jpaMaskingTest.getInstrumentId().replace(".", "||"))
                    .withId(MaskingTestId.of(UUID.fromString(jpaMaskingTest.getId())))
                    .withMaskingActs(maskingActsList);

        if (!Objects.isNull(jpaMaskingTest.getModuleId()) || !"".equals(jpaMaskingTest.getModuleId())) {
            builder.withModuleId(jpaMaskingTest.getModuleId());
        }
        MaskingTest maskingTest = builder.build();
        return maskingTest;
    }
}
