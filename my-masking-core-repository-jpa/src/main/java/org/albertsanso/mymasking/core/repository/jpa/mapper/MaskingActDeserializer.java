package org.albertsanso.mymasking.core.repository.jpa.mapper;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.albertsanso.mymasking.core.domain.model.*;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.*;

public class MaskingActDeserializer extends StdDeserializer<MaskingAct[]> {

    public MaskingActDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public MaskingAct[] deserialize(
            JsonParser jsonParser,
            DeserializationContext deserializationContext) throws IOException, JsonProcessingException {

        List<MaskingAct> maskingActs = new ArrayList<>();

        JsonNode jsonRootNode = jsonParser.getCodec().readTree(jsonParser);
        System.out.println(jsonRootNode);

        if (!jsonRootNode.isEmpty()) {
            if (jsonRootNode.isArray()) {
                for (final JsonNode jsonMaskingAct : jsonRootNode) {
                    MaskingAct.MaskingActBuilder maskingActBuilder = MaskingAct.builder();

                    String actor = jsonMaskingAct.get("actor").asText("");
                    String type = jsonMaskingAct.get("type").asText("");
                    String userName = jsonMaskingAct.get("userName").asText("");
                    String comment = jsonMaskingAct.get("comment").asText("");
                    String strZonedDateTime = jsonMaskingAct.get("zonedDateTime").asText("");
                    String style = jsonMaskingAct.get("style").asText("");

                    maskingActBuilder.withActor(MaskingActor.valueOf(actor));
                    maskingActBuilder.withType(MaskingType.valueOf(type));
                    maskingActBuilder.withUserName(userName);
                    maskingActBuilder.withComment(comment);
                    maskingActBuilder.withZonedDateTime(ZonedDateTime.parse(strZonedDateTime));
                    maskingActBuilder.withMaskingStyle(MaskingStyle.valueOf(style));

                    Optional<IndirectContextInfo> optionalMapContextInfo = Optional.empty();

                    if (jsonMaskingAct.has("contextInfo")) {
                        JsonNode jsonContextInfo = jsonMaskingAct.get("contextInfo");

                        //IndirectContextInfo indirectContextInfo = new IndirectContextInfo();
                        /*
                        Map<String, String> mapContextInfo = new HashMap<>();

                        final Iterator<String> stringIterator = jsonContextInfo.fieldNames();
                        while (stringIterator.hasNext()) {
                            String key = stringIterator.next();
                            String value = jsonContextInfo.get(key).asText("");
                            mapContextInfo.put(key, value);
                        }
                        optionalMapContextInfo = Optional.of(mapContextInfo);
                         */
                    }

                    maskingActBuilder.withContextInfo(optionalMapContextInfo);
                    maskingActs.add(maskingActBuilder.build());
                }
            }
        }
        return maskingActs.toArray(new MaskingAct[maskingActs.size()]);
    }
}
