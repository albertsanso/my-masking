package org.albertsanso.mymasking.core.repository.jpa.mapper;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.albertsanso.mymasking.core.domain.model.MaskingAct;

import java.io.IOException;

public class MaskingActSerializer extends StdSerializer<MaskingAct> {

    public MaskingActSerializer(Class<MaskingAct> t) {
        super(t);
    }

    @Override
    public void serialize(
            MaskingAct maskingAct,
            JsonGenerator jsonGenerator,
            SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("actor", maskingAct.getActor().name());
        jsonGenerator.writeStringField("type", maskingAct.getType().name());
        jsonGenerator.writeStringField("userName", maskingAct.getUserName());
        jsonGenerator.writeStringField("comment", maskingAct.getComment());
        jsonGenerator.writeStringField("zonedDateTime", maskingAct.getZonedDateTime().toString());
        jsonGenerator.writeStringField("style", maskingAct.getStyle().toString());
        if (maskingAct.getContextInfo().isPresent()) {
            jsonGenerator.writeObjectField("contextInfo", maskingAct.getContextInfo().get());
        }
        jsonGenerator.writeEndObject();
    }
}
