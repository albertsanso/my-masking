package org.albertsanso.mymasking.core.repository.jpa.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.albertsanso.mymasking.core.domain.model.relation.MaskingRelation;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingRelation;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.util.function.Function;

@Named
public class MaskingRelationToJpaMaskingRelationMapper implements Function<MaskingRelation, JpaMaskingRelation> {

    private final ObjectMapper objectMapper;

    @Autowired
    public MaskingRelationToJpaMaskingRelationMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public JpaMaskingRelation apply(MaskingRelation maskingRelation) {

        String maskingActivityJson = "";
        try {
            maskingActivityJson = objectMapper.writeValueAsString(maskingRelation.getMaskingActs());
        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return new JpaMaskingRelation(
                maskingRelation.getId(),
                maskingRelation.getMaskingEntityId(),
                maskingRelation.getType(),
                maskingRelation.getRefererEntityId(),
                maskingRelation.getRefererType(),
                maskingActivityJson
        );
    }
}
