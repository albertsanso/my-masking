package org.albertsanso.mymasking.core.repository.jpa.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.albertsanso.mymasking.core.domain.model.module.MaskingModule;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingModule;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.function.Function;

@Named
public class ModuleToJpaMaskingModuleMapper extends EntityToJpaMaskingEntityMapper implements Function<MaskingModule, JpaMaskingModule> {

    @Inject
    public ModuleToJpaMaskingModuleMapper(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public JpaMaskingModule apply(MaskingModule maskingModule) {
        String maskingActivityJson = extractMaskingActs(maskingModule);
        return new JpaMaskingModule(
                maskingModule.getId().getId().toString(),
                maskingActivityJson,
                maskingModule.getModuleId().replace("||", "."),
                maskingModule.getInstrumentId().replace("||", "."));
    }
}
