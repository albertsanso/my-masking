package org.albertsanso.mymasking.core.repository.jpa.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.albertsanso.mymasking.core.domain.model.test.MaskingTest;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingTest;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;
import java.util.function.Function;

@Named
public class TestToJpaMaskingTestMapper extends EntityToJpaMaskingEntityMapper implements Function<MaskingTest, JpaMaskingTest> {

    @Inject
    public TestToJpaMaskingTestMapper(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public JpaMaskingTest apply(MaskingTest maskingTest) {
        String maskingActs = extractMaskingActs(maskingTest);
        String strModuleId = null;

        if (maskingTest.getModuleId().isPresent()) {
            strModuleId = maskingTest.getModuleId().get().replace("||", ".");
        };

        return new JpaMaskingTest(
                maskingTest.getId().getId().toString(),
                maskingTest.getTestId(),
                maskingTest.getInstrumentId().replace("||", "."),
                strModuleId,
                maskingActs
        );
    }

    public static void main(String[] args) {
        System.out.println(Optional.of(null));
    }
}
