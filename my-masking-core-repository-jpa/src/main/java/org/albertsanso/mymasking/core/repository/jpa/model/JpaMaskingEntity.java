package org.albertsanso.mymasking.core.repository.jpa.model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

@MappedSuperclass
public abstract class JpaMaskingEntity {
    private String id;
    private String maskingActs;

    public JpaMaskingEntity() {
    }

    public JpaMaskingEntity(String id, String maskingActs) {
        this.id = id;
        this.maskingActs = maskingActs;
    }

    @Id
    @NotNull
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name="masking_activity", length = 5000)
    public String getMaskingActs() {
        return maskingActs;
    }

    public void setMaskingActs(String maskingActs) {
        this.maskingActs = maskingActs;
    }
}
