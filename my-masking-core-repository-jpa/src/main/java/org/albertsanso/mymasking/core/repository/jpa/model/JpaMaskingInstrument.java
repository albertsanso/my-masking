package org.albertsanso.mymasking.core.repository.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="masking_instrument")
public class JpaMaskingInstrument extends JpaMaskingEntity {

    private String instrumentId;

    public JpaMaskingInstrument() {
    }

    public JpaMaskingInstrument(String id, String maskingActs) {
        super(id, maskingActs);
    }

    public JpaMaskingInstrument(String id, String maskingActs, String instrumentId) {
        super(id, maskingActs);
        this.instrumentId = instrumentId;
    }

    @Column(name="instrument_id")
    @NotNull
    public String getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(String instrumentId) {
        this.instrumentId = instrumentId;
    }
}
