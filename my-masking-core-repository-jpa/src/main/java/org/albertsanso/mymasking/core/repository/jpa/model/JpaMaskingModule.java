package org.albertsanso.mymasking.core.repository.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="masking_module")
public class JpaMaskingModule extends JpaMaskingEntity {

    private String moduleId;
    private String instrumentId;

    public JpaMaskingModule() {
    }

    public JpaMaskingModule(String id, String maskingActs, String moduleId, String instrumentId) {
        super(id, maskingActs);
        this.moduleId = moduleId;
        this.instrumentId = instrumentId;
    }

    @Column(name="module_id")
    @NotNull
    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    @Column(name="instrument_id")
    @NotNull
    public String getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(String instrumentId) {
        this.instrumentId = instrumentId;
    }
}
