package org.albertsanso.mymasking.core.repository.jpa.model;

import org.albertsanso.mymasking.core.domain.model.MaskingEntityType;

import javax.persistence.*;

@Entity
@Table(name="masking_relation")
public class JpaMaskingRelation {

    private String id;
    private String maskingEntityId;
    private MaskingEntityType type;
    private String refererEntityId;
    private MaskingEntityType refererType;
    private String maskingActs;

    public JpaMaskingRelation() {
    }

    public JpaMaskingRelation(String id, String maskingEntityId, MaskingEntityType type, String refererEntityId, MaskingEntityType refererType, String maskingActs) {
        this.id = id;
        this.maskingEntityId = maskingEntityId;
        this.type = type;
        this.refererEntityId = refererEntityId;
        this.refererType = refererType;
        this.maskingActs = maskingActs;
    }

    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name="external_id")
    public String getMaskingEntityId() {
        return maskingEntityId;
    }

    public void setMaskingEntityId(String maskingEntityId) {
        this.maskingEntityId = maskingEntityId;
    }

    @Enumerated(EnumType.STRING)
    @Column(name="type")
    public MaskingEntityType getType() {
        return type;
    }

    public void setType(MaskingEntityType type) {
        this.type = type;
    }

    @Column(name="referer_id")
    public String getRefererEntityId() {
        return refererEntityId;
    }

    public void setRefererEntityId(String refererEntityId) {
        this.refererEntityId = refererEntityId;
    }

    @Enumerated(EnumType.STRING)
    @Column(name="referer_type")
    public MaskingEntityType getRefererType() {
        return refererType;
    }

    public void setRefererType(MaskingEntityType refererType) {
        this.refererType = refererType;
    }

    @Column(name="masking_acts", length=5000)
    public String getMaskingActs() {
        return maskingActs;
    }

    public void setMaskingActs(String maskingActs) {
        this.maskingActs = maskingActs;
    }
}
