package org.albertsanso.mymasking.core.repository.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="masking_test")
public class JpaMaskingTest extends JpaMaskingEntity {

    private String testId;
    private String moduleId;
    private String instrumentId;

    public JpaMaskingTest() {
    }

    public JpaMaskingTest(String id, String testId, String instrumentId, String moduleId, String maskingActs) {
        super(id, maskingActs);
        this.testId = testId;
        this.instrumentId = instrumentId;
        this.moduleId = moduleId;
    }

    @Column(name="test_id")
    @NotNull
    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    @Column(name="module_id")
    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    @Column(name="instrument_id")
    @NotNull
    public String getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(String instrumentId) {
        this.instrumentId = instrumentId;
    }
}
