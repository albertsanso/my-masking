package org.albertsanso.mymasking.core.repository.jpa.repository;

import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrument;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingInstrument;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InstrumentJpaRepositoryHelper extends CrudRepository<JpaMaskingInstrument, MaskingInstrument> {
    Optional<JpaMaskingInstrument> findById(String id);
    Optional<JpaMaskingInstrument> findByInstrumentId(String id);
}
