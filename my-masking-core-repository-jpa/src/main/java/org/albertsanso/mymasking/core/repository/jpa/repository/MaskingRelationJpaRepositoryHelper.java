package org.albertsanso.mymasking.core.repository.jpa.repository;

import org.albertsanso.mymasking.core.domain.model.MaskingEntityType;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingRelation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MaskingRelationJpaRepositoryHelper extends CrudRepository<JpaMaskingRelation, String> {
    Optional<JpaMaskingRelation> findById(String id);
    List<JpaMaskingRelation> findByMaskingEntityIdAndType(String maskingEntityId, MaskingEntityType type);
    List<JpaMaskingRelation> findByMaskingEntityIdAndTypeAndRefererType(String maskingEntityId, MaskingEntityType type, MaskingEntityType refererType);
    JpaMaskingRelation findByMaskingEntityIdAndTypeAndRefererEntityIdAndRefererType(String maskingEntityId, MaskingEntityType type, String refererEntityId, MaskingEntityType refererType);

}
