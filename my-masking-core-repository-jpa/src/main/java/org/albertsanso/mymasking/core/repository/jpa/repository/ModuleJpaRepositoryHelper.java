package org.albertsanso.mymasking.core.repository.jpa.repository;

import org.albertsanso.mymasking.core.domain.model.module.MaskingModule;
import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingModule;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ModuleJpaRepositoryHelper extends CrudRepository<JpaMaskingModule, MaskingModule> {
    //JpaMaskingModule save(JpaMaskingModule jpaMaskingModule);
    Optional<JpaMaskingModule> findById(String id);
    Optional<JpaMaskingModule> findByModuleIdAndInstrumentId(String moduleId, String instrumentId);
}
