package org.albertsanso.mymasking.core.repository.jpa.repository;

import org.albertsanso.mymasking.core.repository.jpa.model.JpaMaskingTest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TestJpaRepositoryHelper extends CrudRepository<JpaMaskingTest, String> {
    Optional<JpaMaskingTest> findById(String id);
    Optional<JpaMaskingTest> findByTestIdAndInstrumentIdAndModuleId(String testId, String instrumentId, Optional<String> moduleId);
    List<JpaMaskingTest> findByTestId(String testId);
}
