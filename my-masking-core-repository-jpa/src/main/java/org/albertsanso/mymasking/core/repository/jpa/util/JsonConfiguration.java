package org.albertsanso.mymasking.core.repository.jpa.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.albertsanso.mymasking.core.domain.model.MaskingAct;
import org.albertsanso.mymasking.core.repository.jpa.mapper.MaskingActDeserializer;
import org.albertsanso.mymasking.core.repository.jpa.mapper.MaskingActSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JsonConfiguration {

    /*
    private final ObjectMapper objectMapper;

    @Inject
    public JsonConfiguration(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
    */

    @Bean(name = "ObjectMapper")
    public ObjectMapper getObjectMapper() {

        MaskingActSerializer maskingActSerializer = new MaskingActSerializer(MaskingAct.class);
        MaskingActDeserializer maskingActDeserializer = new MaskingActDeserializer(MaskingAct[].class);

        ObjectMapper objectMapper = new ObjectMapper();

        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(MaskingAct.class, maskingActSerializer);
        simpleModule.addDeserializer(MaskingAct[].class, maskingActDeserializer);

        objectMapper.registerModule(simpleModule);
        //objectMapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
        //objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);

        return objectMapper;
    }
}
