package org.albertsanso.mymasking.core.application.instrument;

import org.albertsanso.commons.command.DomainCommandHandler;
import org.albertsanso.commons.command.DomainCommandResponse;
import org.albertsanso.mymasking.core.command.instrument.MaskInstrumentCommand;
import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrument;
import org.albertsanso.mymasking.core.domain.service.instrument.InstrumentDirectMaskService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
public class MaskInstrumentCommandHandler extends DomainCommandHandler<MaskInstrumentCommand> {

    private final InstrumentDirectMaskService instrumentDirectMaskService;

    @Inject
    public MaskInstrumentCommandHandler(InstrumentDirectMaskService instrumentDirectMaskService) {
        this.instrumentDirectMaskService = instrumentDirectMaskService;
    }

    @Override
    public DomainCommandResponse handle(MaskInstrumentCommand command) {
        DomainCommandResponse response;
        try {
            MaskingInstrument maskingInstrument = instrumentDirectMaskService.maskInstrument(
                    command.getInstrumentId(),
                    command.getActor(),
                    command.getMaskingType(),
                    command.getUser(),
                    command.getComment(),
                    Optional.empty()
            );
            response = DomainCommandResponse.successResponse(maskingInstrument);
        }
        catch (IllegalStateException ise) {
            response = DomainCommandResponse.failResponse(ise);
        }
        return response;
    }
}
