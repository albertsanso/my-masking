package org.albertsanso.mymasking.core.application.instrument;

import org.albertsanso.commons.command.DomainCommandHandler;
import org.albertsanso.commons.command.DomainCommandResponse;
import org.albertsanso.mymasking.core.command.instrument.UnmaskInstrumentCommand;
import org.albertsanso.mymasking.core.domain.service.instrument.InstrumentDirectMaskService;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class UnmaskInstrumentCommandHandler extends DomainCommandHandler<UnmaskInstrumentCommand> {

    private final InstrumentDirectMaskService instrumentDirectMaskService;

    @Inject
    public UnmaskInstrumentCommandHandler(InstrumentDirectMaskService instrumentDirectMaskService) {
        this.instrumentDirectMaskService = instrumentDirectMaskService;
    }

    @Override
    public DomainCommandResponse handle(UnmaskInstrumentCommand command) {
        DomainCommandResponse response;
        try {
            instrumentDirectMaskService.unmaskInstrument(
                    command.getInstrumentId(),
                    command.getActor(),
                    command.getMaskingType()
            );
            response = DomainCommandResponse.successResponse("");
        }
        catch (IllegalStateException ise) {
            response = DomainCommandResponse.failResponse(ise);
        }
        return response;
    }
}
