package org.albertsanso.mymasking.core.application.module;

import org.albertsanso.commons.command.DomainCommandHandler;
import org.albertsanso.commons.command.DomainCommandResponse;
import org.albertsanso.mymasking.core.command.module.MaskModuleInInstrumentCommand;
import org.albertsanso.mymasking.core.domain.model.module.MaskingModule;
import org.albertsanso.mymasking.core.domain.service.module.ModuleDirectMaskService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
public class MaskModuleInInstrumentCommandHandler extends DomainCommandHandler<MaskModuleInInstrumentCommand> {

    private final ModuleDirectMaskService moduleDirectMaskService;

    @Inject
    public MaskModuleInInstrumentCommandHandler(ModuleDirectMaskService moduleDirectMaskService) {
        this.moduleDirectMaskService = moduleDirectMaskService;
    }

    @Override
    public DomainCommandResponse handle(MaskModuleInInstrumentCommand command) {
        DomainCommandResponse response;
        try {
            MaskingModule maskingModule = moduleDirectMaskService.maskModule(
                    command.getModuleId(),
                    command.getInstrumentId(),
                    command.getActor(),
                    command.getMaskingType(),
                    command.getUser(),
                    command.getComment(),
                    Optional.empty()
            );
            response = DomainCommandResponse.successResponse(maskingModule);
        }
        catch (IllegalStateException ise) {
            ise.printStackTrace();
            response = DomainCommandResponse.failResponse(ise);
        }
        return response;
    }
}
