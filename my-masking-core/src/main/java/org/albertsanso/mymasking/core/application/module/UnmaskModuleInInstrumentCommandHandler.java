package org.albertsanso.mymasking.core.application.module;

import org.albertsanso.commons.command.DomainCommandHandler;
import org.albertsanso.commons.command.DomainCommandResponse;
import org.albertsanso.mymasking.core.command.module.UnmaskModuleInInstrumentCommand;
import org.albertsanso.mymasking.core.domain.service.module.ModuleDirectMaskService;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class UnmaskModuleInInstrumentCommandHandler extends DomainCommandHandler<UnmaskModuleInInstrumentCommand> {

    private final ModuleDirectMaskService moduleDirectMaskService;

    @Inject
    public UnmaskModuleInInstrumentCommandHandler(ModuleDirectMaskService moduleDirectMaskService) {
        this.moduleDirectMaskService = moduleDirectMaskService;
    }

    @Override
    public DomainCommandResponse handle(UnmaskModuleInInstrumentCommand command) {
        DomainCommandResponse response;
        try {
            moduleDirectMaskService.unmaskModule(
                    command.getModuleId(),
                    command.getInstrumentId(),
                    command.getActor(),
                    command.getMaskingType()
            );
            response = DomainCommandResponse.successResponse("");
        }
        catch (IllegalStateException ise) {
            response = DomainCommandResponse.failResponse(ise);
        }
        return response;
    }
}
