package org.albertsanso.mymasking.core.application.test;

import org.albertsanso.commons.query.DomainQueryHandler;
import org.albertsanso.commons.query.DomainQueryResponse;
import org.albertsanso.mymasking.core.domain.service.test.TestQueryMaskService;
import org.albertsanso.mymasking.core.query.test.IsTestMaskedQuery;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class IsTestMaskedQueryHandler extends DomainQueryHandler<IsTestMaskedQuery> {

    private final TestQueryMaskService testQueryMaskService;

    @Inject
    public IsTestMaskedQueryHandler(TestQueryMaskService testQueryMaskService) {
        this.testQueryMaskService = testQueryMaskService;
    }

    @Override
    public DomainQueryResponse handle(IsTestMaskedQuery isTestMaskedQuery) {
        String testId = isTestMaskedQuery.getTestId();
        return DomainQueryResponse.sucessResponse(testQueryMaskService.isTestMasked(testId));
    }
}
