package org.albertsanso.mymasking.core.application.test;

import org.albertsanso.commons.command.DomainCommandHandler;
import org.albertsanso.commons.command.DomainCommandResponse;
import org.albertsanso.mymasking.core.command.test.MaskTestInInstrumentCommand;
import org.albertsanso.mymasking.core.domain.model.test.MaskingTest;
import org.albertsanso.mymasking.core.domain.service.test.TestDirectMaskService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
public class MaskTestInInstrumentCommandHandler extends DomainCommandHandler<MaskTestInInstrumentCommand> {

    private final TestDirectMaskService testDirectMaskService;

    @Inject
    public MaskTestInInstrumentCommandHandler(TestDirectMaskService testDirectMaskService) {
        this.testDirectMaskService = testDirectMaskService;
    }

    @Override
    public DomainCommandResponse handle(MaskTestInInstrumentCommand command) {
        DomainCommandResponse response;
        try {
            MaskingTest maskingTest = testDirectMaskService.maskTestInInstrument(
                    command.getTestId(),
                    command.getInstrumentId(),
                    command.getActor(),
                    command.getMaskingType(),
                    command.getUser(),
                    command.getComment(),
                    Optional.empty()
            );
            response = DomainCommandResponse.successResponse(maskingTest);
        }
        catch (IllegalStateException ise) {
            response = DomainCommandResponse.failResponse(ise);
        }
        return response;
    }
}
