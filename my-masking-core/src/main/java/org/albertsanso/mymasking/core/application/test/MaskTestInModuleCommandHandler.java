package org.albertsanso.mymasking.core.application.test;

import org.albertsanso.commons.command.DomainCommandHandler;
import org.albertsanso.commons.command.DomainCommandResponse;
import org.albertsanso.mymasking.core.command.test.MaskTestInModuleCommand;
import org.albertsanso.mymasking.core.domain.model.test.MaskingTest;
import org.albertsanso.mymasking.core.domain.service.test.TestDirectMaskService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
public class MaskTestInModuleCommandHandler extends DomainCommandHandler<MaskTestInModuleCommand> {
    private final TestDirectMaskService testDirectMaskService;

    @Inject
    public MaskTestInModuleCommandHandler(TestDirectMaskService testDirectMaskService) {
        this.testDirectMaskService = testDirectMaskService;
    }

    @Override
    public DomainCommandResponse handle(MaskTestInModuleCommand command) {
        DomainCommandResponse response;
        try {
            MaskingTest maskingTest = testDirectMaskService.maskTestInModule(
                    command.getTestId(),
                    command.getInstrumentId(),
                    command.getModuleId(),
                    command.getActor(),
                    command.getMaskingType(),
                    command.getUser(),
                    command.getComment(),
                    Optional.empty()
            );
            response = DomainCommandResponse.successResponse(maskingTest);
        }
        catch (IllegalStateException ise) {
            response = DomainCommandResponse.failResponse(ise);
        }
        return response;
    }
}
