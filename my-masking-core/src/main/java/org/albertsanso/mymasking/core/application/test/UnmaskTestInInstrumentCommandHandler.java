package org.albertsanso.mymasking.core.application.test;

import org.albertsanso.commons.command.DomainCommandHandler;
import org.albertsanso.commons.command.DomainCommandResponse;
import org.albertsanso.mymasking.core.command.test.UnmaskTestInInstrumentCommand;
import org.albertsanso.mymasking.core.domain.service.test.TestDirectMaskService;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class UnmaskTestInInstrumentCommandHandler extends DomainCommandHandler<UnmaskTestInInstrumentCommand> {

    private final TestDirectMaskService testDirectMaskService;

    @Inject
    public UnmaskTestInInstrumentCommandHandler(TestDirectMaskService testDirectMaskService) {
        this.testDirectMaskService = testDirectMaskService;
    }

    @Override
    public DomainCommandResponse handle(UnmaskTestInInstrumentCommand command) {
        DomainCommandResponse response;
        try {
            testDirectMaskService.unmaskTestInInstrument(
                    command.getTestId(),
                    command.getInstrumentId(),
                    command.getActor(),
                    command.getMaskingType()
            );
            response = DomainCommandResponse.successResponse(command);
        }
        catch (IllegalStateException ise) {
            response = DomainCommandResponse.failResponse(ise);
        }
        return response;
    }
}
