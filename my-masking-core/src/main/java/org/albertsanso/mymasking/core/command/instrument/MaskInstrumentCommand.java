package org.albertsanso.mymasking.core.command.instrument;

import org.albertsanso.commons.command.DomainCommand;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;
import org.albertsanso.mymasking.core.domain.model.User;

import java.time.ZonedDateTime;

public class MaskInstrumentCommand extends DomainCommand {

    private final String instrumentId;
    private final MaskingActor actor;
    private final MaskingType maskingType;
    private final User user;
    private final String comment;

    public MaskInstrumentCommand(ZonedDateTime occurredOn, String uuid, String instrumentId, MaskingActor actor, MaskingType maskingType, User user, String comment) {
        super(occurredOn, uuid);
        this.instrumentId = instrumentId;
        this.actor = actor;
        this.maskingType = maskingType;
        this.user = user;
        this.comment = comment;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public MaskingActor getActor() {
        return actor;
    }

    public MaskingType getMaskingType() {
        return maskingType;
    }

    public User getUser() {
        return user;
    }

    public String getComment() {
        return comment;
    }
}
