package org.albertsanso.mymasking.core.command.instrument;

import org.albertsanso.commons.command.DomainCommand;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;

import java.time.ZonedDateTime;

public class UnmaskInstrumentCommand extends DomainCommand {

    private final String instrumentId;
    private final MaskingActor actor;
    private final MaskingType maskingType;

    public UnmaskInstrumentCommand(ZonedDateTime occurredOn, String uuid, String instrumentId, MaskingActor actor, MaskingType maskingType) {
        super(occurredOn, uuid);
        this.instrumentId = instrumentId;
        this.actor = actor;
        this.maskingType = maskingType;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public MaskingActor getActor() {
        return actor;
    }

    public MaskingType getMaskingType() {
        return maskingType;
    }
}
