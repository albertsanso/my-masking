package org.albertsanso.mymasking.core.command.module;

import org.albertsanso.commons.command.DomainCommand;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;

import java.time.ZonedDateTime;

public class UnmaskModuleInInstrumentCommand extends DomainCommand {

    private final String moduleId;
    private final String instrumentId;
    private final MaskingActor actor;
    private final MaskingType maskingType;

    public UnmaskModuleInInstrumentCommand(ZonedDateTime occurredOn, String uuid, String moduleId, String instrumentId, MaskingActor actor, MaskingType maskingType) {
        super(occurredOn, uuid);
        this.moduleId = moduleId;
        this.instrumentId = instrumentId;
        this.actor = actor;
        this.maskingType = maskingType;
    }

    public String getModuleId() {
        return moduleId;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public MaskingActor getActor() {
        return actor;
    }

    public MaskingType getMaskingType() {
        return maskingType;
    }
}
