package org.albertsanso.mymasking.core.command.test;

import org.albertsanso.commons.command.DomainCommand;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;
import org.albertsanso.mymasking.core.domain.model.User;

import java.time.ZonedDateTime;

public class MaskTestInModuleCommand extends DomainCommand {

    private final String testId;
    private final String instrumentId;
    private final String moduleId;
    private final MaskingActor actor;
    private final MaskingType maskingType;
    private final User user;
    private final String comment;

    public MaskTestInModuleCommand(ZonedDateTime occurredOn, String uuid, String testId, String instrumentId, String moduleId, MaskingActor actor, MaskingType maskingType, User user, String comment) {
        super(occurredOn, uuid);
        this.testId = testId;
        this.instrumentId = instrumentId;
        this.moduleId = moduleId;
        this.actor = actor;
        this.maskingType = maskingType;
        this.user = user;
        this.comment = comment;
    }

    public String getTestId() {
        return testId;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public String getModuleId() {
        return moduleId;
    }

    public MaskingActor getActor() {
        return actor;
    }

    public MaskingType getMaskingType() {
        return maskingType;
    }

    public User getUser() {
        return user;
    }

    public String getComment() {
        return comment;
    }
}
