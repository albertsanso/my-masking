package org.albertsanso.mymasking.core.command.test;

import org.albertsanso.commons.command.DomainCommand;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;

import java.time.ZonedDateTime;

public class UnmaskTestInInstrumentCommand extends DomainCommand {

    private final String testId;
    private final String instrumentId;
    private final MaskingActor actor;
    private final MaskingType maskingType;

    public UnmaskTestInInstrumentCommand(ZonedDateTime occurredOn, String uuid, String testId, String instrumentId, MaskingActor actor, MaskingType maskingType) {
        super(occurredOn, uuid);
        this.testId = testId;
        this.instrumentId = instrumentId;
        this.actor = actor;
        this.maskingType = maskingType;
    }

    public String getTestId() {
        return testId;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public MaskingActor getActor() {
        return actor;
    }

    public MaskingType getMaskingType() {
        return maskingType;
    }
}
