package org.albertsanso.mymasking.core.domain.event;

import org.albertsanso.commons.event.DomainEvent;
import org.albertsanso.mymasking.core.domain.model.MaskingAct;

import java.time.ZonedDateTime;

public abstract class MaskingEvent extends DomainEvent {

    private final MaskingAct maskingAct;

    public MaskingEvent(ZonedDateTime occurredOn, String uuid, MaskingAct maskingAct) {
        super(occurredOn, uuid);
        this.maskingAct = maskingAct;
    }

    public MaskingAct getMaskingAct() {
        return maskingAct;
    }
}
