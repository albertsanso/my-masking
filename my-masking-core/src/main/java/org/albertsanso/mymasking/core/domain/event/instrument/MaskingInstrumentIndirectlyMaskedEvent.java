package org.albertsanso.mymasking.core.domain.event.instrument;

import org.albertsanso.mymasking.core.domain.event.MaskingEvent;
import org.albertsanso.mymasking.core.domain.model.MaskingAct;
import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrumentId;

import java.time.ZonedDateTime;

public class MaskingInstrumentIndirectlyMaskedEvent extends MaskingEvent {

    private final MaskingInstrumentId maskingInstrumentId;

    private final String instrumentId;

    public MaskingInstrumentIndirectlyMaskedEvent(ZonedDateTime occurredOn, String uuid, MaskingAct maskingAct, MaskingInstrumentId maskingInstrumentId, String instrumentId) {
        super(occurredOn, uuid, maskingAct);
        this.maskingInstrumentId = maskingInstrumentId;
        this.instrumentId = instrumentId;
    }

    public MaskingInstrumentId getMaskingInstrumentId() {
        return maskingInstrumentId;
    }

    public String getInstrumentId() {
        return instrumentId;
    }
}
