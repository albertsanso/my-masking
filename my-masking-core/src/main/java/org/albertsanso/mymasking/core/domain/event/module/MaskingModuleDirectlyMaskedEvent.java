package org.albertsanso.mymasking.core.domain.event.module;

import org.albertsanso.mymasking.core.domain.event.MaskingEvent;
import org.albertsanso.mymasking.core.domain.model.MaskingAct;
import org.albertsanso.mymasking.core.domain.model.module.MaskingModuleId;

import java.time.ZonedDateTime;

public class MaskingModuleDirectlyMaskedEvent extends MaskingEvent {

    private final MaskingModuleId maskingModuleId;

    private final String moduleId;

    private final String instrumentId;

    public MaskingModuleDirectlyMaskedEvent(ZonedDateTime occurredOn, String uuid, MaskingAct maskingAct, MaskingModuleId maskingModuleId, String moduleId, String instrumentId) {
        super(occurredOn, uuid, maskingAct);
        this.maskingModuleId = maskingModuleId;
        this.moduleId = moduleId;
        this.instrumentId = instrumentId;
    }

    public MaskingModuleId getMaskingModuleId() {
        return maskingModuleId;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public String getModuleId() {
        return moduleId;
    }
}
