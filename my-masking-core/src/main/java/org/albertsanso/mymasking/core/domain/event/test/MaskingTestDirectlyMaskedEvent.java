package org.albertsanso.mymasking.core.domain.event.test;

import org.albertsanso.mymasking.core.domain.event.MaskingEvent;
import org.albertsanso.mymasking.core.domain.model.MaskingAct;
import org.albertsanso.mymasking.core.domain.model.test.MaskingTestId;

import java.time.ZonedDateTime;
import java.util.Optional;

public class MaskingTestDirectlyMaskedEvent extends MaskingEvent {

    private final MaskingTestId maskingTestId;

    private final String testId;

    private final String instrumentId;

    private final Optional<String> moduleId;

    public MaskingTestDirectlyMaskedEvent(ZonedDateTime occurredOn, String uuid, MaskingAct maskingAct, MaskingTestId maskingTestId, String testId, String instrumentId, Optional<String> moduleId) {
        super(occurredOn, uuid, maskingAct);
        this.maskingTestId = maskingTestId;
        this.testId = testId;
        this.instrumentId = instrumentId;
        this.moduleId = moduleId;
    }

    public MaskingTestId getMaskingTestId() {
        return maskingTestId;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public Optional<String> getModuleId() {
        return moduleId;
    }

    public String getTestId() {
        return testId;
    }
}
