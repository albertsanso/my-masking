package org.albertsanso.mymasking.core.domain.model;

import java.util.Objects;

public class DirectMaskingActKey {
    private final MaskingActor actor;
    private final MaskingType type;

    public DirectMaskingActKey(MaskingActor actor, MaskingType type) {
        this.actor = actor;
        this.type = type;
    }

    public MaskingActor getActor() {
        return actor;
    }

    public MaskingType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DirectMaskingActKey)) return false;
        DirectMaskingActKey that = (DirectMaskingActKey) o;
        return getActor() == that.getActor() &&
                getType() == that.getType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getActor(), getType());
    }
}
