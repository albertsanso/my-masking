package org.albertsanso.mymasking.core.domain.model;

import java.util.HashMap;
import java.util.Map;

public class IndirectContextInfo {

    private final MaskingEntityType entityType;
    private final Map<String, String> properties = new HashMap<>();
    private String entityId = "";

    public IndirectContextInfo(MaskingEntityType entityType) {
        this.entityType = entityType;
    }

    public void addProperty(String name, String value) {
        properties.put(name, value);
        entityId = rebuildInternalId();
    }

    private String rebuildInternalId() {
        String newId = "";
        for (String key : properties.keySet()) {
            String value = properties.get(key);
            if ("".equals(newId)) {
                newId = value;
            } else {
                newId = newId+","+value;
            }
        }
        return newId;
    }

    public String getEntityId() {
        return entityId;
    }

    public MaskingEntityType getEntityType() {
        return entityType;
    }
}
