package org.albertsanso.mymasking.core.domain.model;

import java.util.Objects;

public class IndirectMaskingActKey {
    private final MaskingActor actor;
    private final MaskingType type;
    private final MaskingEntityType entityType;
    private final String entityId;

    public IndirectMaskingActKey(MaskingActor actor, MaskingType type, MaskingEntityType entityType, String entityId) {
        this.actor = actor;
        this.type = type;
        this.entityType = entityType;
        this.entityId = entityId;
    }

    public MaskingActor getActor() {
        return actor;
    }

    public MaskingType getType() {
        return type;
    }

    public MaskingEntityType getEntityType() {
        return entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IndirectMaskingActKey)) return false;
        IndirectMaskingActKey that = (IndirectMaskingActKey) o;
        return getActor() == that.getActor() &&
                getType() == that.getType() &&
                getEntityType() == that.getEntityType() &&
                Objects.equals(getEntityId(), that.getEntityId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getActor(), getType(), getEntityType(), getEntityId());
    }
}
