package org.albertsanso.mymasking.core.domain.model;

import org.albertsanso.commons.model.ValueObject;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class MaskingAct extends ValueObject {
    private final MaskingActor actor;
    private final MaskingType type;
    private final String userName;
    private final ZonedDateTime zonedDateTime;
    private final String comment;
    private final MaskingStyle style;
    private final Optional<IndirectContextInfo> contextInfo;

    private MaskingAct(MaskingActor actor, MaskingType type, String userName, ZonedDateTime zonedDateTime, String comment, MaskingStyle style, Optional<IndirectContextInfo> contextInfo) {
        this.actor = actor;
        this.type = type;
        this.userName = userName;
        this.zonedDateTime = zonedDateTime;
        this.comment = comment;
        this.style = style;
        this.contextInfo = contextInfo;
    }

    private static MaskingAct createNewMasking(MaskingActBuilder builder) {
        return new MaskingAct(
                builder.getActor(),
                builder.getType(),
                builder.getUserName(),
                builder.getZonedDateTime(),
                builder.getComment(),
                builder.getStyle(),
                builder.getContextInfo());
    }

    public static MaskingActBuilder builder() {
        return new MaskingActBuilder();
    }

    public MaskingActor getActor() {
        return actor;
    }

    public MaskingType getType() {
        return type;
    }

    public String getUserName() {
        return userName;
    }

    public ZonedDateTime getZonedDateTime() {
        return zonedDateTime;
    }

    public String getComment() {
        return comment;
    }

    public MaskingStyle getStyle() {
        return style;
    }

    public Optional<IndirectContextInfo> getContextInfo() {
        return contextInfo;
    }

    public static final class MaskingActBuilder {
        private MaskingActor actor;
        private MaskingType type;
        private String userName;
        private ZonedDateTime zonedDateTime;
        private String comment;
        private MaskingStyle style;
        private Optional<IndirectContextInfo> contextInfo;

        public MaskingActBuilder() {
        }

        public MaskingActBuilder withActor(MaskingActor actor) {
            this.actor = actor;
            return this;
        }

        public MaskingActBuilder withType(MaskingType type) {
            this.type = type;
            return this;
        }

        public MaskingActBuilder withUserName(String userName) {
            this.userName = userName;
            return this;
        }

        public MaskingActBuilder withZonedDateTime(ZonedDateTime zonedDateTime) {
            this.zonedDateTime = zonedDateTime;
            return this;
        }

        public MaskingActBuilder withComment(String comment) {
            this.comment = comment;
            return this;
        }

        public MaskingActBuilder withMaskingStyle(MaskingStyle style) {
            this.style = style;
            return this;
        }

        public MaskingActBuilder withContextInfo(Optional<IndirectContextInfo> contextInfo) {
            this.contextInfo = contextInfo;
            return this;
        }

        public MaskingActor getActor() {
            return actor;
        }

        public MaskingType getType() {
            return type;
        }

        public String getUserName() {
            return userName;
        }

        public ZonedDateTime getZonedDateTime() {
            return zonedDateTime;
        }

        public String getComment() {
            return comment;
        }

        public MaskingStyle getStyle() {
            return style;
        }

        public Optional<IndirectContextInfo> getContextInfo() {
            return contextInfo;
        }

        public MaskingAct build() {
            return createNewMasking(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MaskingAct)) return false;
        MaskingAct that = (MaskingAct) o;
        return getActor() == that.getActor() &&
                getType() == that.getType() &&
                getStyle() == that.getStyle();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getActor(), getType(), getStyle());
    }
}
