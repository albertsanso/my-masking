package org.albertsanso.mymasking.core.domain.model;

import org.albertsanso.commons.model.ValueObject;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class MaskingActivity extends ValueObject {

    private Set<DirectMaskingActKey> directMaskingActSet = new HashSet<>();
    private Set<IndirectMaskingActKey> indirectMaskingActKeySet = new HashSet<>();
    private List<MaskingAct> maskingActs = new ArrayList<>();

    private MaskingActivity(List<MaskingAct> maskingActs) {
        //this.maskingActs = maskingActs;
        for (MaskingAct maskingAct : maskingActs) {
            mask(
              maskingAct.getStyle(),
                    maskingAct.getActor(),
                    maskingAct.getType(),
                    User.builder().withUserName(maskingAct.getUserName()).build(),
                    maskingAct.getComment(),
                    maskingAct.getContextInfo()
            );
        }
    }

    private static MaskingActivity createNewMaskingActivity(MaskingActivityBuilder builder) {
        return new MaskingActivity(builder.getMaskingActs());
    }

    public static MaskingActivityBuilder builder() {
        return new MaskingActivityBuilder();
    }

    public MaskingAct mask(MaskingStyle style, MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo) {

        if (MaskingStyle.DIRECT.equals(style)) {
            return directMask(actor, type, user, comment, contextInfo);
        }

        if (MaskingStyle.INDIRECT.equals(style)) {
            return indirectMask(actor, type, user, comment, contextInfo);
        }

        return null;
    }

    public MaskingAct directMask(MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        MaskingAct maskingAct = MaskingAct.builder()
                .withActor(actor)
                .withType(type)
                .withUserName(user.getUserName())
                .withComment(comment)
                .withZonedDateTime(ZonedDateTime.now())
                .withMaskingStyle(MaskingStyle.DIRECT)
                .withContextInfo(Optional.empty())
                .build();

        DirectMaskingActKey directKey = new DirectMaskingActKey(maskingAct.getActor(), maskingAct.getType());
        if (!directMaskingActSet.contains(directKey)) {
            directMaskingActSet.add(directKey);
            maskingActs.add(maskingAct);
        }
        else {
            throw new IllegalStateException("Already masked!");
        }
        return maskingAct;
    }

    public MaskingAct indirectMask(MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> optionalContextInfo) {

        MaskingEntityType entityType = null;
        String entityId = "";
        if (optionalContextInfo.isPresent()) {
            final IndirectContextInfo contextInfoMap = optionalContextInfo.get();
            entityType = contextInfoMap.getEntityType();
            entityId = contextInfoMap.getEntityId();
        }
        IndirectMaskingActKey key = new IndirectMaskingActKey(actor, type, entityType, entityId);

        if (indirectMaskingActKeySet.contains(key)) {
            return null;
        }

        MaskingAct maskingAct = MaskingAct.builder()
                .withActor(actor)
                .withType(type)
                .withUserName(user.getUserName())
                .withComment(comment)
                .withZonedDateTime(ZonedDateTime.now())
                .withMaskingStyle(MaskingStyle.INDIRECT)
                .withContextInfo(optionalContextInfo)
                .build();

        indirectMaskingActKeySet.add(key);
        maskingActs.add(maskingAct);
        return maskingAct;
    }

    public MaskingAct unmask(MaskingStyle style, MaskingActor actor, MaskingType type) {

        /*
        if (!isMaskedFor(style, actor, type)) {
            throw new IllegalStateException("Not masked");
        }

        StyleActorTypeKey key = new StyleActorTypeKey(style, actor, type);
        MaskingAct maskingAct = maskingActs.remove(key);
        return maskingAct;
         */
        return null;
    }

    public boolean isMasked() {
        return !maskingActs.isEmpty();
    }

    public List<MaskingAct> getMaskingActs() {
        return maskingActs;
    }

    public static final class MaskingActivityBuilder {
        private List<MaskingAct> builderMaskingActsList = new ArrayList<>();

        public MaskingActivityBuilder() {
        }

        public MaskingActivityBuilder withMaskingActs(List<MaskingAct> maskingActsList) {
            this.builderMaskingActsList = maskingActsList;
            return this;
        }
        public MaskingActivity build() {
            return createNewMaskingActivity(this);
        }

        public List<MaskingAct> getMaskingActs() {
            return builderMaskingActsList;
        }
    }
}
