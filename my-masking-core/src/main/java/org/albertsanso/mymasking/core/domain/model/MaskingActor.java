package org.albertsanso.mymasking.core.domain.model;

public enum MaskingActor {
    USER,
    INSTRUMENT,
    RULE_ENGINE,
    QC,
    AON,
    REFORMATTER
}
