package org.albertsanso.mymasking.core.domain.model;

import org.albertsanso.commons.model.Entity;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class MaskingEntity extends Entity {
    protected MaskingActivity maskingActivity = MaskingActivity.builder().build();

    public MaskingEntity() {
    }

    public MaskingEntity(List<MaskingAct> maskingActs) {

        this.maskingActivity = MaskingActivity.builder().withMaskingActs(maskingActs).build();
    }

    protected MaskingAct mask(MaskingStyle style, MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        return maskingActivity.mask(style, actor, type, user, comment, contextInfo);
    }

    public MaskingAct unmask(MaskingStyle style, MaskingActor actor, MaskingType type) {
        return maskingActivity.unmask(style, actor, type);
    }

    public boolean isMasked() {
        return maskingActivity.isMasked();
    }

    /*
    public boolean isMaskedFor(MaskingStyle style, MaskingActor actor, MaskingType type) {
        return maskingActivity.isMaskedFor(style, actor, type);
    }
     */

    public List<MaskingAct> getMaskingActs() {
        return maskingActivity.getMaskingActs();
    }
}
