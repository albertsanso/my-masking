package org.albertsanso.mymasking.core.domain.model;

public enum MaskingEntityType {
    TEST,
    MODULE,
    INSTRUMENT,
    TARGET,
    PROFILE
}
