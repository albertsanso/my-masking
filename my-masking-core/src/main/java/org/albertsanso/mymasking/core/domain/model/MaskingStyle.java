package org.albertsanso.mymasking.core.domain.model;

public enum MaskingStyle {
    DIRECT,
    INDIRECT
}
