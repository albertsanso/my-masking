package org.albertsanso.mymasking.core.domain.model;

public enum MaskingType {
    PROCESSING,
    DISTRIBUTION
}
