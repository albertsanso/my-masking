package org.albertsanso.mymasking.core.domain.model;

import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrument;
import org.albertsanso.mymasking.core.domain.model.module.MaskingModule;
import org.albertsanso.mymasking.core.domain.model.test.MaskingTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Profile {

    private final String profileId;

    private final List<MaskingInstrument> maskingInstruments;

    private final List<MaskingModule> maskingModules;

    private final List<MaskingTest> maskingTests;

    public Profile(String profileId) {
        this.profileId = profileId;
        this.maskingInstruments = new ArrayList<>();
        this.maskingModules = new ArrayList<>();
        this.maskingTests = new ArrayList<>();
    }

    public String getProfileId() {
        return profileId;
    }

    public List<MaskingInstrument> getMaskingInstruments() {
        return Collections.unmodifiableList(maskingInstruments);
    }

    public List<MaskingModule> getMaskingModules() {
        return Collections.unmodifiableList(maskingModules);
    }

    public List<MaskingTest> getMaskingTests() {
        return Collections.unmodifiableList(maskingTests);
    }

    public void addInstrument(MaskingInstrument maskingInstrument) {
        this.maskingInstruments.add(maskingInstrument);
    }

    public void addModule(MaskingModule maskingModule) {
        this.maskingModules.add(maskingModule);
    }

    public void addTest(MaskingTest maskingTest) {
        this.maskingTests.add(maskingTest);
    }
}
