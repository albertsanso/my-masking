package org.albertsanso.mymasking.core.domain.model;

public class User {
    private final String userName;

    private User(String userName) {
        this.userName = userName;
    }

    private static User createNewUser(UserBuilder userBuilder) {
        return new User(userBuilder.getUserName());
    }

    public static UserBuilder builder() { return new UserBuilder(); }

    public String getUserName() {
        return userName;
    }

    public static final class UserBuilder {
        private String userName;

        public UserBuilder() {
        }

        public UserBuilder withUserName(String userName) {
            this.userName = userName;
            return this;
        }

        public String getUserName() {
            return userName;
        }

        public User build() {
            return createNewUser(this);
        }
    }
}
