package org.albertsanso.mymasking.core.domain.model.instrument;

import org.albertsanso.mymasking.core.domain.event.instrument.MaskingInstrumentDirectlyMaskedEvent;
import org.albertsanso.mymasking.core.domain.event.instrument.MaskingInstrumentIndirectlyMaskedEvent;
import org.albertsanso.mymasking.core.domain.event.instrument.MaskingInstrumentUnmaskedEvent;
import org.albertsanso.mymasking.core.domain.model.*;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;

public class MaskingInstrument extends MaskingEntity {

    private final MaskingInstrumentId id;

    private final String instrumentId;

    private MaskingInstrument(MaskingInstrumentId id, String instrumentId, List<MaskingAct> maskingActs) {
        super(maskingActs);
        this.id = id;
        this.instrumentId = instrumentId;
    }

    private static MaskingInstrument createNewInstrument(InstrumentBuilder builder) {
        return new MaskingInstrument(builder.getId(), builder.getInstrumentId(), builder.getMaskingActs());
    }

    public static InstrumentBuilder builder(String instrumentId) {
        return new InstrumentBuilder(instrumentId);
    }

    public MaskingInstrumentId getId() {
        return id;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public MaskingAct directMask(MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        final MaskingAct maskingAct = maskInstrument(MaskingStyle.DIRECT, actor, type, user, comment, contextInfo);
        publishEvent(new MaskingInstrumentUnmaskedEvent(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                maskingAct,
                getId(),
                instrumentId));
        return maskingAct;
    }

    public MaskingAct indirectMask(MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        final MaskingAct maskingAct = maskInstrument(MaskingStyle.INDIRECT, actor, type, user, comment, contextInfo);
        publishEvent(new MaskingInstrumentIndirectlyMaskedEvent(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                maskingAct,
                getId(),
                instrumentId));
        return maskingAct;
    }

    private MaskingAct maskInstrument(MaskingStyle style, MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        MaskingAct maskingAct = mask(style, actor, type, user, comment, contextInfo);
        publishEvent(new MaskingInstrumentDirectlyMaskedEvent(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                maskingAct,
                getId(),
                instrumentId));
        return maskingAct;
    }

    public MaskingAct unmaskInstrument(MaskingActor actor, MaskingType type) {
        return unmask(MaskingStyle.DIRECT, actor, type);
    }

    public static final class InstrumentBuilder {

        private MaskingInstrumentId id;

        private String instrumentId;

        private List<MaskingAct> maskingActs = new ArrayList<>();

        public InstrumentBuilder(String instrumentId) {
            this.instrumentId = instrumentId;
        }

        public InstrumentBuilder withMaskingActs(List<MaskingAct> maskingActs) {
            this.maskingActs = maskingActs;
            return this;
        }

        public InstrumentBuilder WithId(MaskingInstrumentId id) {
            this.id = id;
            return this;
        }

        public MaskingInstrumentId getId() {
            return id;
        }

        public String getInstrumentId() {
            return instrumentId;
        }

        public List<MaskingAct> getMaskingActs() {
            return maskingActs;
        }

        public MaskingInstrument build() { return createNewInstrument(this); }
    }
}
