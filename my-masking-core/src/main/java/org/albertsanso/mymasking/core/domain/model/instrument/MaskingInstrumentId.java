package org.albertsanso.mymasking.core.domain.model.instrument;

import org.albertsanso.commons.model.ValueObject;

import java.util.UUID;

public class MaskingInstrumentId extends ValueObject {
    private final UUID id;

    public MaskingInstrumentId(UUID id) {
        this.id = id;
    }

    public static MaskingInstrumentId of(UUID id) { return new MaskingInstrumentId(id); }

    public static MaskingInstrumentId createNewId() {
        return new MaskingInstrumentId(UUID.randomUUID());
    }

    public UUID getId() {
        return id;
    }
}
