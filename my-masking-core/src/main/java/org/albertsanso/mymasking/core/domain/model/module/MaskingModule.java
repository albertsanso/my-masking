package org.albertsanso.mymasking.core.domain.model.module;

import org.albertsanso.mymasking.core.domain.event.module.MaskingModuleDirectlyMaskedEvent;
import org.albertsanso.mymasking.core.domain.event.module.MaskingModuleIndirectlyMaskedEvent;
import org.albertsanso.mymasking.core.domain.event.module.MaskingModuleUnmaskedEvent;
import org.albertsanso.mymasking.core.domain.model.*;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;

public class MaskingModule extends MaskingEntity {

    private final MaskingModuleId id;

    private final String moduleId;

    private final String instrumentId;

    private MaskingModule(MaskingModuleId id, String moduleId, String instrumentId, List<MaskingAct> maskingActs) {
        super(maskingActs);
        this.id = id;
        this.moduleId = moduleId;
        this.instrumentId = instrumentId;
    }

    public static ModuleBuilder builder(String moduleId, String  instrumentId) {
        return new ModuleBuilder(moduleId, instrumentId);
    }

    private static MaskingModule createNewModule(ModuleBuilder builder) {
        return new MaskingModule(builder.getId(), builder.getModuleId(), builder.getInstrumentId(), builder.getMaskingActs());
    }

    public MaskingModuleId getId() {
        return id;
    }

    public String getModuleId() {
        return moduleId;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public MaskingAct directMask(MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        final MaskingAct maskingAct = maskModule(MaskingStyle.DIRECT, actor, type, user, comment, contextInfo);
        publishEvent(new MaskingModuleDirectlyMaskedEvent(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                maskingAct,
                getId(),
                moduleId,
                instrumentId));
        return maskingAct;
    }

    public MaskingAct indirectMask(MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        final MaskingAct maskingAct = maskModule(MaskingStyle.INDIRECT, actor, type, user, comment, contextInfo);
        publishEvent(new MaskingModuleIndirectlyMaskedEvent(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                maskingAct,
                getId(),
                moduleId,
                instrumentId));
        return maskingAct;
    }

    private MaskingAct maskModule(MaskingStyle style, MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        return mask(style, actor, type, user, comment, contextInfo);
    }

    public MaskingAct unmaskModule(MaskingActor actor, MaskingType type) {
        MaskingAct maskingAct = unmask(MaskingStyle.DIRECT, actor, type);

        publishEvent(new MaskingModuleUnmaskedEvent(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                maskingAct,
                getId(),
                moduleId,
                instrumentId));
        return maskingAct;
    }

    public static final class ModuleBuilder {

        private MaskingModuleId id;

        private String moduleId;

        private String instrumentId;

        private List<MaskingAct> maskingActs = Collections.EMPTY_LIST;

        public ModuleBuilder(String moduleId, String instrumentId) {
            this.moduleId = moduleId;
            this.instrumentId = instrumentId;
        }

        public ModuleBuilder withMaskingActs(List<MaskingAct> maskingActs) {
            this.maskingActs = maskingActs;
            return this;
        }

        public ModuleBuilder withId(MaskingModuleId id) {
            this.id = id;
            return this;
        }

        public MaskingModuleId getId() {
            return id;
        }

        public String getModuleId() {
            return moduleId;
        }

        public String getInstrumentId() {
            return instrumentId;
        }

        public List<MaskingAct> getMaskingActs() {
            return maskingActs;
        }

        public MaskingModule build() { return createNewModule(this); }
    }
}
