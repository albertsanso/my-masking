package org.albertsanso.mymasking.core.domain.model.module;

import org.albertsanso.commons.model.ValueObject;

import java.util.UUID;

public class MaskingModuleId extends ValueObject {

    private final UUID id;

    public MaskingModuleId(UUID id) {
        this.id = id;
    }

    public static MaskingModuleId of (UUID id) { return new MaskingModuleId(id); }

    public static MaskingModuleId createNewId() {
        return new MaskingModuleId(UUID.randomUUID());
    }

    public UUID getId() {
        return id;
    }
}
