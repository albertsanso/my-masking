package org.albertsanso.mymasking.core.domain.model.relation;

import org.albertsanso.commons.model.ValueObject;
import org.albertsanso.mymasking.core.domain.model.MaskingAct;
import org.albertsanso.mymasking.core.domain.model.MaskingEntityType;

import java.util.List;

public class MaskingRelation extends ValueObject {

    private final String id;
    private final String maskingEntityId;
    private final MaskingEntityType type;
    private final String refererEntityId;
    private final MaskingEntityType refererType;
    private final List<MaskingAct> maskingActs;

    public MaskingRelation(String id, String maskingEntityId, MaskingEntityType type, String refererEntityId, MaskingEntityType refererType, List<MaskingAct> maskingActs) {
        this.id = id;
        this.maskingEntityId = maskingEntityId;
        this.type = type;
        this.refererEntityId = refererEntityId;
        this.refererType = refererType;
        this.maskingActs = maskingActs;
    }

    public String getId() {
        return id;
    }

    public String getMaskingEntityId() {
        return maskingEntityId;
    }

    public MaskingEntityType getType() {
        return type;
    }

    public String getRefererEntityId() {
        return refererEntityId;
    }

    public MaskingEntityType getRefererType() {
        return refererType;
    }

    public List<MaskingAct> getMaskingActs() {
        return maskingActs;
    }
}
