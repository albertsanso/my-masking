package org.albertsanso.mymasking.core.domain.model.test;

import org.albertsanso.mymasking.core.domain.event.test.MaskingTestDirectlyMaskedEvent;
import org.albertsanso.mymasking.core.domain.event.test.MaskingTestIndirectlyMaskedEvent;
import org.albertsanso.mymasking.core.domain.event.test.MaskingTestUnmaskedEvent;
import org.albertsanso.mymasking.core.domain.model.*;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;

public class MaskingTest extends MaskingEntity {

    private final MaskingTestId id;

    private final String testId;

    private final String instrumentId;

    private Optional<String> moduleId = Optional.empty();

    private MaskingTest(MaskingTestId id, String testId, String instrumentId, Optional<String> moduleId, List<MaskingAct> maskingActs) {
        super(maskingActs);
        this.id = id;
        this.testId = testId;
        this.instrumentId = instrumentId;
        this.moduleId = moduleId;
    }

    private static MaskingTest createNewTest(TestBuilder builder) {
        return new MaskingTest(
                builder.getId(),
                builder.getTestId(),
                builder.getInstrumentId(),
                builder.getModuleId(),
                builder.getMaskingActs());
    }

    public static TestBuilder builder(String testId, String instrumentId) {
        return new TestBuilder(testId, instrumentId);
    }

    public MaskingTestId getId() {
        return id;
    }

    public String getTestId() {
        return testId;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public Optional<String> getModuleId() {
        return moduleId;
    }

    public MaskingAct directMask(MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        final MaskingAct maskingAct = maskTest(MaskingStyle.DIRECT, actor, type, user, comment, contextInfo);
        publishEvent(new MaskingTestDirectlyMaskedEvent(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                maskingAct,
                getId(),
                testId,
                instrumentId,
                moduleId));
        return maskingAct;
    }

    public MaskingAct indirectMask(MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        final MaskingAct maskingAct = maskTest(MaskingStyle.INDIRECT, actor, type, user, comment, contextInfo);
        publishEvent(new MaskingTestIndirectlyMaskedEvent(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                maskingAct,
                getId(),
                testId,
                instrumentId,
                moduleId));
        return maskingAct;
    }

    private MaskingAct maskTest(MaskingStyle style, MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        return mask(style, actor, type, user, comment, contextInfo);
    }

    public MaskingAct unmaskTest(MaskingActor actor, MaskingType type) {
        MaskingAct maskingAct = unmask(MaskingStyle.DIRECT, actor, type);

        publishEvent(new MaskingTestUnmaskedEvent(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                maskingAct,
                getId(),
                testId,
                instrumentId,
                moduleId));
        return maskingAct;
    }

    public static final class TestBuilder {

        private MaskingTestId id;

        private String testId;

        private String instrumentId;

        private Optional<String> moduleId = Optional.empty();

        private List<MaskingAct> maskingActs = new ArrayList<>();

        public TestBuilder(String testId, String instrumentId) {
            this.testId = testId;
            this.instrumentId = instrumentId;
        }

        public TestBuilder withModuleId(String moduleId) {
            if (Objects.isNull(moduleId)) {
                this.moduleId = Optional.empty();
            }
            else {
                this.moduleId = Optional.of(moduleId);
            }
            return this;
        }

        public TestBuilder withMaskingActs(List<MaskingAct> maskingActs) {
            this.maskingActs = maskingActs;
            return this;
        }

        public TestBuilder withId(MaskingTestId id) {
            this.id = id;
            return this;
        }

        public MaskingTestId getId() {
            return id;
        }

        public String getTestId() {
            return testId;
        }

        public String getInstrumentId() {
            return instrumentId;
        }

        public Optional<String> getModuleId() {
            return moduleId;
        }

        public List<MaskingAct> getMaskingActs() {
            return maskingActs;
        }

        public MaskingTest build() { return createNewTest(this); }
    }
}
