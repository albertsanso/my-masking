package org.albertsanso.mymasking.core.domain.model.test;

import org.albertsanso.commons.model.ValueObject;

import java.util.UUID;

public class MaskingTestId extends ValueObject {

    private final UUID id;

    public MaskingTestId(UUID id) {
        this.id = id;
    }

    public static MaskingTestId of(UUID id) {
        return new MaskingTestId(id);
    }

    public static MaskingTestId createNewId() {
        return new MaskingTestId(UUID.randomUUID());
    }

    public UUID getId() {
        return id;
    }
}
