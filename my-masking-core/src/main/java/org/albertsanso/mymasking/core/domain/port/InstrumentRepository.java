package org.albertsanso.mymasking.core.domain.port;

import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrument;
import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrumentId;

import java.util.Optional;

public interface InstrumentRepository {
    Optional<MaskingInstrument> findById(MaskingInstrumentId id);
    Optional<MaskingInstrument> findByInstrumentId(String instrumentId);
    MaskingInstrument save(MaskingInstrument maskingInstrument);
}
