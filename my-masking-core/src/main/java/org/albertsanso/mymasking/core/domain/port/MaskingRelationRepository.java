package org.albertsanso.mymasking.core.domain.port;

import org.albertsanso.mymasking.core.domain.model.MaskingEntityType;
import org.albertsanso.mymasking.core.domain.model.relation.MaskingRelation;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MaskingRelationRepository {
    Optional<MaskingRelation> findById(UUID id);
    List<MaskingRelation> findByMaskingEntityIdAndType(String maskingEntityId, MaskingEntityType type);
    List<MaskingRelation> findByMaskingEntityIdAndTypeAndRefererType(String maskingEntityId, MaskingEntityType type, MaskingEntityType refererType);
    MaskingRelation findByMaskingEntityIdAndTypeAndRefererEntityIdAndRefererType(String maskingEntityId, MaskingEntityType type, String refererEntityId, MaskingEntityType refererType);

    MaskingRelation save(MaskingRelation maskingRelation);
}
