package org.albertsanso.mymasking.core.domain.port;

import org.albertsanso.mymasking.core.domain.model.module.MaskingModule;
import org.albertsanso.mymasking.core.domain.model.module.MaskingModuleId;

import java.util.Optional;

public interface ModuleRepository {
    Optional<MaskingModule> findById(MaskingModuleId id);
    Optional<MaskingModule> findOneForIds(String moduleId, String instrumentId);
    MaskingModule save(MaskingModule maskingModule);
}
