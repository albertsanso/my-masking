package org.albertsanso.mymasking.core.domain.port;

import org.albertsanso.mymasking.core.domain.model.test.MaskingTest;
import org.albertsanso.mymasking.core.domain.model.test.MaskingTestId;

import java.util.List;
import java.util.Optional;

public interface TestRepository {
    Optional<MaskingTest> findById(MaskingTestId id);
    Optional<MaskingTest> findOneForIds(String testId, String instrumentId, Optional<String> moduleId);
    List<MaskingTest> findByTestId(String testId);
    MaskingTest save(MaskingTest maskingTest);
}
