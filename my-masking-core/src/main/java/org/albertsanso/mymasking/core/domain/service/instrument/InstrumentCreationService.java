package org.albertsanso.mymasking.core.domain.service.instrument;

import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrument;
import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrumentId;
import org.albertsanso.mymasking.core.domain.port.InstrumentRepository;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class InstrumentCreationService {
    private final InstrumentRepository instrumentRepository;

    @Inject
    public InstrumentCreationService(InstrumentRepository instrumentRepository) {
        this.instrumentRepository = instrumentRepository;
    }

    public MaskingInstrument createInstrument(String instrumentId) {
        MaskingInstrument.InstrumentBuilder builder = MaskingInstrument.builder(instrumentId).WithId(MaskingInstrumentId.createNewId());
        MaskingInstrument maskingInstrument = builder.build();
        return maskingInstrument;
    }
}
