package org.albertsanso.mymasking.core.domain.service.instrument;

import org.albertsanso.mymasking.core.domain.model.IndirectContextInfo;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;
import org.albertsanso.mymasking.core.domain.model.User;
import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrument;
import org.albertsanso.mymasking.core.domain.port.InstrumentRepository;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import java.util.Optional;

@Named
public class InstrumentDirectMaskService extends InstrumentMaskService {

    @Inject
    public InstrumentDirectMaskService(InstrumentRepository instrumentRepository, InstrumentCreationService instrumentCreationService) {
        super(instrumentRepository, instrumentCreationService);
    }

    @Override
    public MaskingInstrument maskInstrument(String instrumentId, MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        final MaskingInstrument maskingInstrument = getOrCreate(instrumentId);
        maskingInstrument.directMask(actor, type, user, comment, contextInfo);
        return saveMaskingInstrument(maskingInstrument);
    }
}
