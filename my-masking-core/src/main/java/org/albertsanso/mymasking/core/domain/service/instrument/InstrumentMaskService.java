package org.albertsanso.mymasking.core.domain.service.instrument;

import org.albertsanso.mymasking.core.domain.model.IndirectContextInfo;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;
import org.albertsanso.mymasking.core.domain.model.User;
import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrument;
import org.albertsanso.mymasking.core.domain.port.InstrumentRepository;

import java.util.Map;
import java.util.Optional;

public abstract class InstrumentMaskService {

    private final InstrumentRepository instrumentRepository;

    private final InstrumentCreationService instrumentCreationService;

    public InstrumentMaskService(InstrumentRepository instrumentRepository, InstrumentCreationService instrumentCreationService) {
        this.instrumentRepository = instrumentRepository;
        this.instrumentCreationService = instrumentCreationService;
    }

    public abstract MaskingInstrument maskInstrument(String instrumentId, MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo);

    public void unmaskInstrument(String instrumentId, MaskingActor actor, MaskingType type) {
        Optional<MaskingInstrument> instrumentOptional = instrumentRepository.findByInstrumentId(instrumentId);
        if (instrumentOptional.isPresent()) {
            MaskingInstrument maskingInstrument = instrumentOptional.get();
            maskingInstrument.unmaskInstrument(actor, type);
            instrumentRepository.save(maskingInstrument);
        }
        else {
            throw new IllegalStateException("Not masked");
        }
    }

    protected MaskingInstrument getOrCreate(String instrumentId) {
        Optional<MaskingInstrument> instrumentOptional = instrumentRepository.findByInstrumentId(instrumentId);
        MaskingInstrument maskingInstrument;
        if (!instrumentOptional.isPresent()) {
            maskingInstrument = instrumentCreationService.createInstrument(instrumentId);
        }
        else {
            maskingInstrument = instrumentOptional.get();
        }
        return maskingInstrument;
    }

    protected MaskingInstrument saveMaskingInstrument(MaskingInstrument maskingInstrument) {
        return instrumentRepository.save(maskingInstrument);
    }
}
