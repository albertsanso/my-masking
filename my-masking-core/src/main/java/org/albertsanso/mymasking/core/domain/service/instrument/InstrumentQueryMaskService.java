package org.albertsanso.mymasking.core.domain.service.instrument;

import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrument;
import org.albertsanso.mymasking.core.domain.port.InstrumentRepository;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
public class InstrumentQueryMaskService {

    private final InstrumentRepository instrumentRepository;

    @Inject
    public InstrumentQueryMaskService(InstrumentRepository instrumentRepository) {
        this.instrumentRepository = instrumentRepository;
    }

    public boolean isInstrumentMasked(String instrumentId) {
        Optional<MaskingInstrument> instrumentOptional = instrumentRepository.findByInstrumentId(instrumentId);
        if (instrumentOptional.isPresent()) {
            MaskingInstrument maskingInstrument = instrumentOptional.get();
            return maskingInstrument.isMasked();
        }
        return false;
    }
}
