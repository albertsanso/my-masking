package org.albertsanso.mymasking.core.domain.service.module;

import org.albertsanso.mymasking.core.domain.model.module.MaskingModule;
import org.albertsanso.mymasking.core.domain.model.module.MaskingModuleId;
import org.albertsanso.mymasking.core.domain.port.ModuleRepository;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class ModuleCreationService {

    private final ModuleRepository moduleRepository;

    @Inject
    public ModuleCreationService(ModuleRepository moduleRepository) {
        this.moduleRepository = moduleRepository;
    }

    public MaskingModule createModule(String moduleId, String instrumentId) {
        MaskingModule.ModuleBuilder moduleBuilder = MaskingModule.builder(moduleId, instrumentId).withId(MaskingModuleId.createNewId());
        MaskingModule maskingModule = moduleBuilder.build();
        MaskingModule maskingModule1 = moduleRepository.save(maskingModule);
        return maskingModule1;
    }
}
