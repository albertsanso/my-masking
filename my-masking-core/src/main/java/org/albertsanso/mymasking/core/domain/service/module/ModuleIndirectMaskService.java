package org.albertsanso.mymasking.core.domain.service.module;

import org.albertsanso.mymasking.core.domain.model.IndirectContextInfo;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;
import org.albertsanso.mymasking.core.domain.model.User;
import org.albertsanso.mymasking.core.domain.model.module.MaskingModule;
import org.albertsanso.mymasking.core.domain.port.ModuleRepository;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import java.util.Optional;

@Named
public class ModuleIndirectMaskService extends ModuleMaskService {

    @Inject
    public ModuleIndirectMaskService(ModuleRepository moduleRepository, ModuleCreationService moduleCreationService) {
        super(moduleRepository, moduleCreationService);
    }

    @Override
    public MaskingModule maskModule(String moduleId, String instrumentId, MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo) {
        MaskingModule maskingModule = getOrCreateModule(moduleId, instrumentId);
        maskingModule.indirectMask(actor, type, user, comment, contextInfo);
        return saveMaskingModule(maskingModule);
    }
}
