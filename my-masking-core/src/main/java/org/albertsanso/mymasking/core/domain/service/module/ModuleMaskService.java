package org.albertsanso.mymasking.core.domain.service.module;

import org.albertsanso.mymasking.core.domain.model.IndirectContextInfo;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;
import org.albertsanso.mymasking.core.domain.model.User;
import org.albertsanso.mymasking.core.domain.model.module.MaskingModule;
import org.albertsanso.mymasking.core.domain.port.ModuleRepository;

import java.util.Optional;

public abstract class ModuleMaskService {

    protected final ModuleRepository moduleRepository;

    protected final ModuleCreationService moduleCreationService;

    public ModuleMaskService(ModuleRepository moduleRepository, ModuleCreationService moduleCreationService) {
        this.moduleRepository = moduleRepository;
        this.moduleCreationService = moduleCreationService;
    }

    public abstract MaskingModule maskModule(String moduleId, String instrumentId, MaskingActor actor, MaskingType type, User user, String comment, Optional<IndirectContextInfo> contextInfo);

    public void unmaskModule(String moduleId, String instrumentId, MaskingActor actor, MaskingType type) {
        Optional<MaskingModule> moduleOptional = moduleRepository.findOneForIds(moduleId, instrumentId);
        if (moduleOptional.isPresent()) {
            MaskingModule maskingModule = moduleOptional.get();
            maskingModule.unmaskModule(actor, type);
            moduleRepository.save(maskingModule);
        }
        else {
            throw new IllegalStateException("Not masked");
        }
    }

    protected MaskingModule getOrCreateModule(String moduleId, String instrumentId) {
        Optional<MaskingModule> moduleOptional = moduleRepository.findOneForIds(moduleId, instrumentId);
        MaskingModule maskingModule;
        if (!moduleOptional.isPresent()) {
            maskingModule = moduleCreationService.createModule(moduleId, instrumentId);
        }
        else {
            maskingModule = moduleOptional.get();
        }
        return maskingModule;
    }

    protected MaskingModule saveMaskingModule(MaskingModule maskingModule) {
        return moduleRepository.save(maskingModule);
    }
}
