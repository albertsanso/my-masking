package org.albertsanso.mymasking.core.domain.service.module;

import org.albertsanso.mymasking.core.domain.model.module.MaskingModule;
import org.albertsanso.mymasking.core.domain.port.ModuleRepository;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
public class ModuleQueryMaskService {

    private final ModuleRepository moduleRepository;

    @Inject
    public ModuleQueryMaskService(ModuleRepository moduleRepository) {
        this.moduleRepository = moduleRepository;
    }

    public boolean isModuleMaskedInInstrument(String moduleId, String instrumentId) {
        Optional<MaskingModule> moduleOptional = moduleRepository.findOneForIds(moduleId, instrumentId);
        if (moduleOptional.isPresent()) {
            MaskingModule maskingModule = moduleOptional.get();
            return maskingModule.isMasked();
        }
        return false;
    }
}
