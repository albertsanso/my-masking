package org.albertsanso.mymasking.core.domain.service.relation;

import org.albertsanso.mymasking.core.domain.port.MaskingRelationRepository;

import javax.inject.Inject;

public class MaskingTestRelationSearchService {

    private final MaskingRelationRepository maskingRelationRepository;

    @Inject
    public MaskingTestRelationSearchService(MaskingRelationRepository maskingRelationRepository) {
        this.maskingRelationRepository = maskingRelationRepository;
    }
}
