package org.albertsanso.mymasking.core.domain.service.test;

import org.albertsanso.mymasking.core.domain.model.test.MaskingTest;
import org.albertsanso.mymasking.core.domain.model.test.MaskingTestId;
import org.albertsanso.mymasking.core.domain.port.TestRepository;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
public class TestCreationService {

    private final TestRepository testRepository;

    @Inject
    public TestCreationService(TestRepository testRepository) {
        this.testRepository = testRepository;
    }

    public MaskingTest createTest(String testId, String instrumentId, Optional<String> moduleId) {
        MaskingTest.TestBuilder testBuilder = MaskingTest.builder(testId, instrumentId).withId(MaskingTestId.createNewId());
        if (moduleId.isPresent()) testBuilder.withModuleId(moduleId.get());
        MaskingTest maskingTest = testBuilder.build();
        testRepository.save(maskingTest);
        return maskingTest;
    }
}
