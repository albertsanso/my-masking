package org.albertsanso.mymasking.core.domain.service.test;

import org.albertsanso.mymasking.core.domain.model.IndirectContextInfo;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;
import org.albertsanso.mymasking.core.domain.model.User;
import org.albertsanso.mymasking.core.domain.model.test.MaskingTest;
import org.albertsanso.mymasking.core.domain.port.TestRepository;

import java.util.Map;
import java.util.Optional;

public abstract class TestMaskService {

    protected final TestRepository testRepository;

    protected final TestCreationService testCreationService;

    public TestMaskService(TestRepository testRepository, TestCreationService testCreationService) {
        this.testRepository = testRepository;
        this.testCreationService = testCreationService;
    }

    public abstract MaskingTest maskTestInInstrument(String testId, String instrumentId, MaskingActor actor, MaskingType maskingType, User user, String comment, Optional<IndirectContextInfo> contextInfo);

    public abstract MaskingTest maskTestInModule(String testId, String instrumentId, String moduleId, MaskingActor actor, MaskingType maskingType, User user, String comment, Optional<IndirectContextInfo> contextInfo);

    public void unmaskTestInInstrument(String testId, String instrumentId, MaskingActor actor, MaskingType maskingType) {
        unmaskTest(testId, instrumentId, Optional.empty(), actor, maskingType);
    }

    public void unmaskTestInModule(String testId, String instrumentId, String moduleId, MaskingActor actor, MaskingType maskingType) {
        unmaskTest(testId, instrumentId, Optional.of(moduleId), actor, maskingType);
    }

    protected void unmaskTest(String testId, String instrumentId, Optional<String> moduleId, MaskingActor actor, MaskingType maskingType) {
        Optional<MaskingTest> testOptional = testRepository.findOneForIds(testId, instrumentId, moduleId);
        if (testOptional.isPresent()) {
            MaskingTest maskingTest = testOptional.get();
            maskingTest.unmaskTest(actor, maskingType);
            testRepository.save(maskingTest);
        }
        else {
            throw new IllegalStateException("Not masked");
        }
    }

    protected MaskingTest getOrCreateTest(String testId, String instrumentId, Optional<String> moduleId) {
        Optional<MaskingTest> testOptional = testRepository.findOneForIds(testId, instrumentId, moduleId);
        MaskingTest maskingTest;
        if (!testOptional.isPresent()) {
            maskingTest = testCreationService.createTest(testId, instrumentId, moduleId);
        }
        else {
            maskingTest = testOptional.get();
        }
        return maskingTest;
    }

    protected MaskingTest saveMaskingTest(MaskingTest maskingTest) {
        return testRepository.save(maskingTest);
    }
}
