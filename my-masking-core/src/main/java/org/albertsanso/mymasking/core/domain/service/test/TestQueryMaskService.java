package org.albertsanso.mymasking.core.domain.service.test;

import org.albertsanso.mymasking.core.domain.model.test.MaskingTest;
import org.albertsanso.mymasking.core.domain.port.TestRepository;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;

@Named
public class TestQueryMaskService {

    private final TestRepository testRepository;

    @Inject
    public TestQueryMaskService(TestRepository testRepository) {
        this.testRepository = testRepository;
    }

    public boolean isTestMaskedInInstrument(String testId, String instrumentId) {
        Optional<MaskingTest> testOptional = testRepository.findOneForIds(testId, instrumentId, Optional.empty());
        if (testOptional.isPresent()) {
            MaskingTest maskingTest = testOptional.get();
            maskingTest.isMasked();
        }
        return false;
    }

    public boolean isTestMaskedInModule(String testId, String instrumentId, String moduleId) {
        Optional<MaskingTest> testOptional = testRepository.findOneForIds(testId, instrumentId, Optional.of(moduleId));
        if (testOptional.isPresent()) {
            MaskingTest maskingTest = testOptional.get();
            return maskingTest.isMasked();
        }
        return false;
    }

    public boolean isTestMasked(String testId) {
        final List<MaskingTest> maskingTestList = testRepository.findByTestId(testId);
        if (!isNull(maskingTestList)) return maskingTestList.size()>0;
        return false;
    }
}
