package org.albertsanso.mymasking.core.query.test;

import org.albertsanso.commons.query.DomainQuery;

import java.time.ZonedDateTime;

public class IsTestMaskedInInstrumentQuery extends DomainQuery {

    private final String testId;
    private final String instrumentId;

    protected IsTestMaskedInInstrumentQuery(ZonedDateTime occurredOn, String uuid, String testId, String instrumentId) {
        super(occurredOn, uuid);
        this.testId = testId;
        this.instrumentId = instrumentId;
    }

    public String getTestId() {
        return testId;
    }

    public String getInstrumentId() {
        return instrumentId;
    }
}
