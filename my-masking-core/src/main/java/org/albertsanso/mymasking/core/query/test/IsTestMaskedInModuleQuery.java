package org.albertsanso.mymasking.core.query.test;

import org.albertsanso.commons.query.DomainQuery;

import java.time.ZonedDateTime;

public class IsTestMaskedInModuleQuery extends DomainQuery {
    private final String testId;
    private final String instrumentId;
    private final String moduleId;

    protected IsTestMaskedInModuleQuery(ZonedDateTime occurredOn, String uuid, String testId, String instrumentId, String moduleId) {
        super(occurredOn, uuid);
        this.testId = testId;
        this.instrumentId = instrumentId;
        this.moduleId = moduleId;
    }

    public String getTestId() {
        return testId;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public String getModuleId() {
        return moduleId;
    }
}
