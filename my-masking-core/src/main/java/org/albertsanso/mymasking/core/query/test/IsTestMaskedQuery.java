package org.albertsanso.mymasking.core.query.test;

import org.albertsanso.commons.query.DomainQuery;

import java.time.ZonedDateTime;

public class IsTestMaskedQuery extends DomainQuery {
    private final String testId;

    public IsTestMaskedQuery(ZonedDateTime occurredOn, String uuid, String testId) {
        super(occurredOn, uuid);
        this.testId = testId;
    }

    public String getTestId() {
        return testId;
    }
}
