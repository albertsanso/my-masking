package org.albertsanso.core.domain.model;

import org.assertj.core.api.WithAssertions;
import org.junit.Test;

public class MaskingTestShould implements WithAssertions {

    @Test
    public void have_test_masked_after_masking_by_instrument_for_processing() {

        /*
        TestInInstrument test = new TestInInstrument("test-1", "instrument-1");
        User user = User.builder().withUserName("User-1").build();

        test.mask(MaskingActor.INSTRUMENT, MaskingType.PROCESSING, user, "");

        assertThat(test.isMasked());
        */
    }

    @Test
    public void have_test_not_masked_by_aon_after_masking_by_qc() {
        /*
        TestInInstrument test = new TestInInstrument("test-1", "instrument-1");
        User user = User.builder().withUserName("User-1").build();

        test.mask(MaskingActor.QC, MaskingType.PROCESSING, user, "");

        Assert.assertFalse(test.isMaskedByActor(MaskingActor.AON));
         */
    }
}
