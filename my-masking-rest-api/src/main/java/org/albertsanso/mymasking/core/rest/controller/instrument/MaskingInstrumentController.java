package org.albertsanso.mymasking.core.rest.controller.instrument;

import org.albertsanso.commons.command.CommandBus;
import org.albertsanso.commons.command.DomainCommand;
import org.albertsanso.commons.command.DomainCommandResponse;
import org.albertsanso.mymasking.core.rest.dto.instrument.MaskInstrumentRequest;
import org.albertsanso.mymasking.core.rest.mapper.instrument.MaskInstrumentRequestToCommandMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("instrument")
public class MaskingInstrumentController {

    private final CommandBus commandBus;

    private final MaskInstrumentRequestToCommandMapper maskInstrumentRequestToCommandMapper;

    @Inject
    public MaskingInstrumentController(CommandBus commandBus, MaskInstrumentRequestToCommandMapper maskInstrumentRequestToCommandMapper) {
        this.commandBus = commandBus;
        this.maskInstrumentRequestToCommandMapper = maskInstrumentRequestToCommandMapper;
    }

    @PatchMapping("mask")
    public ResponseEntity<Object> mask(@RequestBody MaskInstrumentRequest maskInstrumentRequest) {
        DomainCommand command = maskInstrumentRequestToCommandMapper.apply(maskInstrumentRequest);
        DomainCommandResponse response = commandBus.push(command);
        if (!response.isSuccess()) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok().build();
    }
}
