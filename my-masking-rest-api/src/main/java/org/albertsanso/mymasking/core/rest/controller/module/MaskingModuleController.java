package org.albertsanso.mymasking.core.rest.controller.module;

import org.albertsanso.commons.command.CommandBus;
import org.albertsanso.commons.command.DomainCommand;
import org.albertsanso.commons.command.DomainCommandResponse;
import org.albertsanso.mymasking.core.rest.dto.module.MaskModuleRequest;
import org.albertsanso.mymasking.core.rest.mapper.module.MaskModuleRequestToCommandMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("module")
public class MaskingModuleController {

    private final CommandBus commandBus;

    private final MaskModuleRequestToCommandMapper maskModuleRequestToCommandMapper;

    @Inject
    public MaskingModuleController(CommandBus commandBus, MaskModuleRequestToCommandMapper maskModuleRequestToCommandMapper) {
        this.commandBus = commandBus;
        this.maskModuleRequestToCommandMapper = maskModuleRequestToCommandMapper;
    }

    @PatchMapping("mask")
    public ResponseEntity<Object> mask(@RequestBody MaskModuleRequest maskModuleRequest) {
        DomainCommand command = maskModuleRequestToCommandMapper.apply(maskModuleRequest);
        DomainCommandResponse response = commandBus.push(command);
        if (!response.isSuccess()) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok().build();
    }
}
