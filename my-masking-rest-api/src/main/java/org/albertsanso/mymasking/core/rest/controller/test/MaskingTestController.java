package org.albertsanso.mymasking.core.rest.controller.test;

import org.albertsanso.commons.command.CommandBus;
import org.albertsanso.commons.command.DomainCommand;
import org.albertsanso.commons.command.DomainCommandResponse;
import org.albertsanso.commons.query.DomainQueryResponse;
import org.albertsanso.commons.query.QueryBus;
import org.albertsanso.mymasking.core.query.test.IsTestMaskedQuery;
import org.albertsanso.mymasking.core.rest.dto.test.MaskTestRequest;
import org.albertsanso.mymasking.core.rest.mapper.test.MaskTestRequestToCommandMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.UUID;

@RestController
@RequestMapping("test")
public class MaskingTestController {

    private final CommandBus commandBus;

    private final QueryBus queryBus;

    private final MaskTestRequestToCommandMapper maskTestRequestToCommandMapper;

    @Inject
    public MaskingTestController(CommandBus commandBus, QueryBus queryBus, MaskTestRequestToCommandMapper maskTestRequestToCommandMapper) {
        this.commandBus = commandBus;
        this.queryBus = queryBus;
        this.maskTestRequestToCommandMapper = maskTestRequestToCommandMapper;
    }

    @PatchMapping("mask")
    public ResponseEntity<Object> mask(@RequestBody MaskTestRequest maskTestRequest) {
        DomainCommand command = maskTestRequestToCommandMapper.apply(maskTestRequest);
        DomainCommandResponse response = commandBus.push(command);
        if (!response.isSuccess()) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.ok().build();
    }

    @GetMapping("ismasked")
    public ResponseEntity<Object> getMaskingDetails(String testId) {

        IsTestMaskedQuery query = new IsTestMaskedQuery(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                testId
        );

        final DomainQueryResponse queryResponse = queryBus.push(query);
        return ResponseEntity.ok().body(queryResponse.getResponse());
    }

}
