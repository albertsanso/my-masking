package org.albertsanso.mymasking.core.rest.dto.test;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public class MaskTestRequest {

    @NotNull
    private String testId;

    private String moduleId;

    @NotNull
    private String instrumentId;

    @NotNull
    private String actor;

    @NotNull
    private String type;

    @NotNull
    private String userName;

    private String comments;

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(String instrumentId) {
        this.instrumentId = instrumentId;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
