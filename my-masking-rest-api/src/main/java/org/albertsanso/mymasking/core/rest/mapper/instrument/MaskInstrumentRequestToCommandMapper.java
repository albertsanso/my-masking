package org.albertsanso.mymasking.core.rest.mapper.instrument;

import org.albertsanso.commons.command.DomainCommand;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;
import org.albertsanso.mymasking.core.domain.model.User;
import org.albertsanso.mymasking.core.indirect.command.instrument.FullMaskInstrumentCommand;
import org.albertsanso.mymasking.core.rest.dto.instrument.MaskInstrumentRequest;

import javax.inject.Named;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.UUID;
import java.util.function.Function;

@Named
public class MaskInstrumentRequestToCommandMapper implements Function<MaskInstrumentRequest, DomainCommand> {
    @Override
    public DomainCommand apply(MaskInstrumentRequest maskInstrumentRequest) {
        DomainCommand command;

        User user = User.builder().withUserName("albert").build();

        command = new FullMaskInstrumentCommand(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                maskInstrumentRequest.getInstrumentId(),
                MaskingActor.valueOf(maskInstrumentRequest.getActor()),
                MaskingType.valueOf(maskInstrumentRequest.getType()),
                user,
                maskInstrumentRequest.getComments()
        );

        return command;
    }
}
