package org.albertsanso.mymasking.core.rest.mapper.module;

import org.albertsanso.commons.command.DomainCommand;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;
import org.albertsanso.mymasking.core.domain.model.User;
import org.albertsanso.mymasking.core.indirect.command.module.FullMaskModuleInInstrumentCommand;
import org.albertsanso.mymasking.core.rest.dto.module.MaskModuleRequest;

import javax.inject.Named;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.UUID;
import java.util.function.Function;

@Named
public class MaskModuleRequestToCommandMapper implements Function<MaskModuleRequest, DomainCommand> {
    @Override
    public DomainCommand apply(MaskModuleRequest maskModuleRequest) {
        DomainCommand command;

        User user = User.builder().withUserName("albert").build();

        command = new FullMaskModuleInInstrumentCommand(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                maskModuleRequest.getModuleId(),
                maskModuleRequest.getInstrumentId(),
                MaskingActor.valueOf(maskModuleRequest.getActor()),
                MaskingType.valueOf(maskModuleRequest.getType()),
                user,
                maskModuleRequest.getComments()
        );

        return command;
    }
}
