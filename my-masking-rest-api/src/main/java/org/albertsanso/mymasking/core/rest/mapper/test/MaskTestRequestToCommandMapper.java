package org.albertsanso.mymasking.core.rest.mapper.test;

import org.albertsanso.commons.command.DomainCommand;
import org.albertsanso.mymasking.core.command.test.MaskTestInInstrumentCommand;
import org.albertsanso.mymasking.core.command.test.MaskTestInModuleCommand;
import org.albertsanso.mymasking.core.domain.model.MaskingActor;
import org.albertsanso.mymasking.core.domain.model.MaskingType;
import org.albertsanso.mymasking.core.domain.model.User;
import org.albertsanso.mymasking.core.rest.dto.test.MaskTestRequest;

import javax.inject.Named;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;

@Named
public class MaskTestRequestToCommandMapper implements Function<MaskTestRequest, DomainCommand> {

    @Override
    public DomainCommand apply(MaskTestRequest maskTestRequest) {
        DomainCommand command;

        User user = User.builder().withUserName("albert").build();

        if (Objects.isNull(maskTestRequest.getModuleId())) {
            command = new MaskTestInInstrumentCommand(
                    ZonedDateTime.now(ZoneOffset.UTC),
                    UUID.randomUUID().toString(),
                    maskTestRequest.getTestId(),
                    maskTestRequest.getInstrumentId(),
                    MaskingActor.valueOf(maskTestRequest.getActor()),
                    MaskingType.valueOf(maskTestRequest.getType()),
                    user,
                    maskTestRequest.getComments()
            );
        }
        else {
            command = new MaskTestInModuleCommand(
                    ZonedDateTime.now(ZoneOffset.UTC),
                    UUID.randomUUID().toString(),
                    maskTestRequest.getTestId(),
                    maskTestRequest.getInstrumentId(),
                    maskTestRequest.getModuleId(),
                    MaskingActor.valueOf(maskTestRequest.getActor()),
                    MaskingType.valueOf(maskTestRequest.getType()),
                    user,
                    maskTestRequest.getComments()
            );
        }

        return command;
    }
}
