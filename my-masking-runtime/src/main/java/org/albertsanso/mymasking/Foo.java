package org.albertsanso.mymasking;

import org.albertsanso.mymasking.configuration.command.MyCommandBus;
import org.albertsanso.mymasking.core.command.module.MaskModuleInInstrumentCommand;
import org.albertsanso.mymasking.core.command.test.MaskTestInInstrumentCommand;
import org.albertsanso.mymasking.core.command.test.MaskTestInModuleCommand;
import org.albertsanso.mymasking.core.domain.model.*;
import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrument;
import org.albertsanso.mymasking.core.domain.model.instrument.MaskingInstrumentId;
import org.albertsanso.mymasking.core.domain.model.module.MaskingModule;
import org.albertsanso.mymasking.core.domain.model.module.MaskingModuleId;
import org.albertsanso.mymasking.core.domain.model.relation.MaskingRelation;
import org.albertsanso.mymasking.core.domain.model.test.MaskingTest;
import org.albertsanso.mymasking.core.domain.model.test.MaskingTestId;
import org.albertsanso.mymasking.core.domain.port.TestRepository;
import org.albertsanso.mymasking.core.domain.service.instrument.InstrumentDirectMaskService;
import org.albertsanso.mymasking.core.domain.service.module.ModuleDirectMaskService;
import org.albertsanso.mymasking.core.domain.service.test.TestCreationService;
import org.albertsanso.mymasking.core.domain.service.test.TestDirectMaskService;
import org.albertsanso.mymasking.core.domain.service.test.TestIndirectMaskService;
import org.albertsanso.mymasking.core.indirect.command.module.FullMaskModuleInInstrumentCommand;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument.MasterInstrumentJpa;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument.MasterInstrumentJpaRepositoryHelper;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module.MasterModuleJpa;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module.MasterModuleJpaRepository;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module.MasterModuleJpaRepositoryHelper;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test.MasterTestJpa;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test.MasterTestJpaRepository;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test.MasterTestJpaRepositoryHelper;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping.MasterTestMappingJpa;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping.MasterTestMappingJpaRepositoryHelper;
import org.albertsanso.mymasking.core.masterdata.domain.model.test.MasterTest;
import org.albertsanso.mymasking.core.repository.jpa.adapter.InstrumentJpaRepository;
import org.albertsanso.mymasking.core.repository.jpa.adapter.MaskingRelationJpaRepository;
import org.albertsanso.mymasking.core.repository.jpa.adapter.ModuleJpaRepository;
import org.albertsanso.mymasking.core.repository.jpa.adapter.TestJpaRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.inject.Inject;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

@SpringBootApplication(scanBasePackages = { "org.albertsanso.mymasking" })
public class Foo implements CommandLineRunner {

    @Inject
    MaskingRelationJpaRepository maskingRelationJpaRepository;

    @Inject
    TestDirectMaskService testDirectMaskService;

    @Inject
    TestIndirectMaskService testIndirectMaskService;

    @Inject
    TestJpaRepository testJpaRepository;

    @Inject
    TestRepository testRepository;

    @Inject
    ModuleJpaRepository moduleJpaRepository;

    @Inject
    InstrumentJpaRepository instrumentJpaRepository;

    @Inject
    ModuleDirectMaskService moduleDirectMaskService;

    @Inject
    InstrumentDirectMaskService instrumentDirectMaskService;

    @Inject
    MasterTestJpaRepository masterTestJpaRepository;

    @Inject
    MasterModuleJpaRepository masterModuleJpaRepository;

    @Inject
    MasterTestMappingJpaRepositoryHelper masterTestMappingJpaRepositoryHelper;

    @Inject
    MasterModuleJpaRepositoryHelper masterModuleJpaRepositoryHelper;

    @Inject
    MasterInstrumentJpaRepositoryHelper masterInstrumentJpaRepositoryHelper;

    @Inject
    MasterTestJpaRepositoryHelper masterTestJpaRepositoryHelper;

    @Inject
    TestCreationService testCreationService;

    @Inject
    MyCommandBus myCommandBus;

    public static void main(String[] args) {
        SpringApplication.run(Foo.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        scenario();

        //fooTest();
        //fooTestT();
        //fooTestService();

        //fooModule();
        //fooInstrument();



        //fooModuleService();
        //fooInstrumentService();

        //maskTestCommand();
        //maskModuleCommand();

        //fullMaskModule();
        //masterTest();
    }

    /* ---------------------------------------------------------- */

    private void masterTest() {


        /*
        //final List<MasterTest> masterTestList = masterTestJpaRepository.findByInstrumentId(instrument1.getId());
        final List<MasterTest> masterTestList = masterTestJpaRepository.findByModuleIdAndInstrumentId(
                module3.getId(),
                instrument1.getId()
        );

        final List<MasterModule> masterModules = masterModuleJpaRepository.findByInstrumentId(instrument1.getId());
        */

        User user = User.builder().withUserName("User-1").build();


        FullMaskModuleInInstrumentCommand command = new FullMaskModuleInInstrumentCommand(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                "572||1||1", //module1.getModuleId(),
                "572||1", //instrument1.getInstrumentId(),
                MaskingActor.QC,
                MaskingType.PROCESSING,
                user,
                "--"
        );
        myCommandBus.push(command);

        /*
        FullMaskInstrumentCommand command = new FullMaskInstrumentCommand(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                instrument2.getInstrumentId(),
                MaskingActor.QC,
                MaskingType.PROCESSING,
                user,
                "--"
        );
        Instant start = Instant.now();
        for (int i=0; i<1; i++) {
            commandBus.push(command);
        }
        Instant finish = Instant.now();
        long elapsedTime = Duration.between(start, finish).toMillis();
        System.out.println("---------> TOOK: "+elapsedTime+" ms");
        */
        System.out.println();
    }

    private void fullMaskModule() {

        User user = User.builder().withUserName("User-1").build();
        FullMaskModuleInInstrumentCommand command = new FullMaskModuleInInstrumentCommand(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                "572||1||1",
                "572||1",
                MaskingActor.QC,
                MaskingType.PROCESSING,
                user,
                "--"
        );

        Instant start = Instant.now();
        for (int i=0; i<1; i++) {
            myCommandBus.push(command);
            final Optional<MaskingTest> t8 = testRepository.findOneForIds("T8", "572||1", Optional.of("572||1||1"));
        }
        Instant finish = Instant.now();
        long elapsedTime = Duration.between(start, finish).toMillis();
        System.out.println("---------> TOOK: "+elapsedTime+" ms");
    }

    private void maskModuleCommand() {

        User user = User.builder().withUserName("User-1").build();

        MaskModuleInInstrumentCommand command;

        command = new MaskModuleInInstrumentCommand(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                "572||1||1",
                "572||1",
                MaskingActor.QC,
                MaskingType.PROCESSING,
                user,
                "--"
        );


        Instant start = Instant.now();
        for (int i=0; i<1; i++) {
            myCommandBus.push(command);
        }
        Instant finish = Instant.now();
        long elapsedTime = Duration.between(start, finish).toMillis();
        System.out.println("---------> TOOK: "+elapsedTime+" ms");
    }

    private void maskTestCommand() {



        User user = User.builder().withUserName("User-1").build();

        MaskTestInInstrumentCommand maskCommand = new MaskTestInInstrumentCommand(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                "T1",
                "572||1",
                MaskingActor.QC,
                MaskingType.PROCESSING,
                user,
                "no comments"
        );

        MaskTestInModuleCommand maskCommand2 = new MaskTestInModuleCommand(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                "T1",
                "572||1",
                "572||1||1",
                MaskingActor.QC,
                MaskingType.PROCESSING,
                user,
                "no comments"
        );

        /*
        UnmaskTestInInstrumentCommand unmaskCommand = new UnmaskTestInInstrumentCommand(
                ZonedDateTime.now(ZoneOffset.UTC),
                UUID.randomUUID().toString(),
                "T1",
                "572||1",
                MaskingActor.QC,
                MaskingType.PROCESSING
        );
        */

        Instant start = Instant.now();
        for (int i=0; i<1; i++) {
            myCommandBus.push(maskCommand2);
            //commandBus.push(unmaskCommand);
        }
        Instant finish = Instant.now();
        long elapsedTime = Duration.between(start, finish).toMillis();
        System.out.println("---------> TOOK: "+elapsedTime+" ms");
    }

    private void fooInstrumentService() {
        User user = User.builder().withUserName("User-1").build();
        String comment = "My funny comment";
        instrumentDirectMaskService.maskInstrument("572||1", MaskingActor.AON, MaskingType.DISTRIBUTION, user, comment, Optional.empty());
    }

    private void fooTestService() {
        User user = User.builder().withUserName("User-1").build();
        String comment = "My funny comment";


        /*MaskingTest test = testCreationService.createTest("T1", "572||1", Optional.empty());
        test.directMask(MaskingActor.QC, MaskingType.PROCESSING, user, comment, Optional.empty());
        System.out.println(test);*/

        /*
        testDirectMaskService.maskTestInInstrument("T1", "572||1", MaskingActor.QC, MaskingType.PROCESSING, user, comment, Optional.empty());
        testDirectMaskService.maskTestInModule("T1", "572||1", "572||1||1", MaskingActor.QC, MaskingType.PROCESSING, user, comment, Optional.empty());
        testDirectMaskService.maskTestInInstrument("T1", "572||1", MaskingActor.AON, MaskingType.PROCESSING, user, comment, Optional.empty());
        testDirectMaskService.maskTestInInstrument("T1", "572||1", MaskingActor.QC, MaskingType.DISTRIBUTION, user, comment, Optional.empty());
        testDirectMaskService.maskTestInInstrument("T1", "572||1", MaskingActor.QC, MaskingType.DISTRIBUTION, user, comment, Optional.empty());
         */



        testIndirectMaskService.maskTestInInstrument("T1", "572||1", MaskingActor.QC, MaskingType.PROCESSING, user, comment, Optional.empty());
        testIndirectMaskService.maskTestInModule("T1", "572||1", "572||1||1", MaskingActor.QC, MaskingType.PROCESSING, user, comment, Optional.empty());
        testIndirectMaskService.maskTestInInstrument("T1", "572||1", MaskingActor.AON, MaskingType.PROCESSING, user, comment, Optional.empty());
        testIndirectMaskService.maskTestInInstrument("T1", "572||1", MaskingActor.QC, MaskingType.DISTRIBUTION, user, comment, Optional.empty());
        testIndirectMaskService.maskTestInInstrument("T1", "572||1", MaskingActor.QC, MaskingType.DISTRIBUTION, user, comment, Optional.empty());


    }

    private void fooModuleService() {
        User user = User.builder().withUserName("User-1").build();
        String comment = "My funny comment";

        moduleDirectMaskService.maskModule("572||1||1", "572||1", MaskingActor.AON, MaskingType.DISTRIBUTION, user, comment, Optional.empty());
        moduleDirectMaskService.maskModule("572||1||1", "572||1", MaskingActor.QC, MaskingType.DISTRIBUTION, user, comment, Optional.empty());
        moduleDirectMaskService.maskModule("572||1||1", "572||1", MaskingActor.REFORMATTER, MaskingType.PROCESSING, user, comment, Optional.empty());
    }

    private void fooInstrument() {

        User user = User.builder().withUserName("User-1").build();

        MaskingInstrument maskingInstrument = MaskingInstrument.builder("572||1").WithId(MaskingInstrumentId.createNewId()).build();

        maskingInstrument.directMask(MaskingActor.AON, MaskingType.PROCESSING, user, "comment-1 fd af dsa f a", Optional.empty());
        /*
        instrument.mask(MaskingActor.AON, MaskingType.DISTRIBUTION, user, "comment-1 fd af dsa f das f dsa fs f sda f dsa fd saf dsa f ds af dsa fd saf dsa f dsa f dsa f dsa f dsa f sda fds a");
        instrument.mask(MaskingActor.QC, MaskingType.PROCESSING, user, "comment-1 fd af dsa f das f dsa fs f sda f dsa fd saf dsa f ds af dsa fd saf dsa f dsa f dsa f dsa f dsa f sda fds a");
        instrument.mask(MaskingActor.QC, MaskingType.DISTRIBUTION, user, "comment-1 fd af dsa f das f dsa fs f sda f dsa fd saf dsa f ds af dsa fd saf dsa f dsa f dsa f dsa f dsa f sda fds a");
        instrument.mask(MaskingActor.USER, MaskingType.PROCESSING, user, "comment-1 fd af dsa f das f dsa fs f sda f dsa fd saf dsa f ds af dsa fd saf dsa f dsa f dsa f dsa f dsa f sda fds a");
        instrument.mask(MaskingActor.USER, MaskingType.DISTRIBUTION, user, "comment-1 fd af dsa f das f dsa fs f sda f dsa fd saf dsa f ds af dsa fd saf dsa f dsa f dsa f dsa f dsa f sda fds a");
        instrument.mask(MaskingActor.INSTRUMENT, MaskingType.PROCESSING, user, "comment-1 fd af dsa f das f dsa fs f sda f dsa fd saf dsa f ds af dsa fd saf dsa f dsa f dsa f dsa f dsa f sda fds a");
        instrument.mask(MaskingActor.INSTRUMENT, MaskingType.DISTRIBUTION, user, "comment-1 fd af dsa f das f dsa fs f sda f dsa fd saf dsa f ds af dsa fd saf dsa f dsa f dsa f dsa f dsa f sda fds a");
        instrument.mask(MaskingActor.RULE_ENGINE, MaskingType.PROCESSING, user, "comment-1 fd af dsa f das f dsa fs f sda f dsa fd saf dsa f ds af dsa fd saf dsa f dsa f dsa f dsa f dsa f sda fds a");
        instrument.mask(MaskingActor.RULE_ENGINE, MaskingType.DISTRIBUTION, user, "comment-1 fd af dsa f das f dsa fs f sda f dsa fd saf dsa f ds af dsa fd saf dsa f dsa f dsa f dsa f dsa f sda fds a");
        instrument.mask(MaskingActor.REFORMATTER, MaskingType.PROCESSING, user, "comment-1 fd af dsa f das f dsa fs f sda f dsa fd saf dsa f ds af dsa fd saf dsa f dsa f dsa f dsa f dsa f sda fds a");
        instrument.mask(MaskingActor.REFORMATTER, MaskingType.DISTRIBUTION, user, "comment-1 fd af dsa f das f dsa fs f sda f dsa fd saf dsa f ds af dsa fd saf dsa f dsa f dsa f dsa f dsa f sda fds a");
        */

        MaskingInstrument maskingInstrument1 = instrumentJpaRepository.save(maskingInstrument);
        System.out.println(maskingInstrument1);

        Optional<MaskingInstrument> instrument2 = instrumentJpaRepository.findById(maskingInstrument1.getId());
        if (instrument2.isPresent()) {
            System.out.println(instrument2);
        }
    }

    private void fooModule() {
        MaskingModule maskingModule = MaskingModule.builder("572||1||1", "572||1").withId(MaskingModuleId.createNewId()).build();
        MaskingModule maskingModule1 = moduleJpaRepository.save(maskingModule);
        System.out.println(maskingModule1);

        Optional<MaskingModule> module3 = moduleJpaRepository.findById(maskingModule1.getId());
        if (module3.isPresent()) {
            System.out.println(module3);
        }
    }

    private void fooTest() {

        User user = User.builder().withUserName("User-1").build();

        MaskingTest maskingTest = MaskingTest.builder("T1", "572||1").withModuleId("572||1||1").withId(MaskingTestId.createNewId()).build();
        maskingTest.directMask(MaskingActor.QC, MaskingType.DISTRIBUTION, user, "comment-1", Optional.empty());
        MaskingTest maskingTest2 = testJpaRepository.save(maskingTest);
        System.out.println(maskingTest2);


        //Optional<MaskingTest> test3 = testJpaRepository.findById(maskingTest2.getId());
        Optional<MaskingTest> test3 = testJpaRepository.findById(maskingTest2.getId());
        if (test3.isPresent()) {
            System.out.println(test3);
        }
    }

    private void fooTestT() {
        User user = User.builder().withUserName("User-1").build();

        MaskingTest maskingTest;

        maskingTest = MaskingTest.builder("T1", "572||1").withModuleId("572||1||1").withId(MaskingTestId.createNewId()).build();
        maskingTest.directMask(MaskingActor.QC, MaskingType.DISTRIBUTION, user, "comment-1", Optional.empty());
        testJpaRepository.save(maskingTest);

        maskingTest = MaskingTest.builder("T1", "572||1").withModuleId("572||1||1").withId(MaskingTestId.createNewId()).build();
        maskingTest.directMask(MaskingActor.QC, MaskingType.DISTRIBUTION, user, "comment-1", Optional.empty());
        testJpaRepository.save(maskingTest);

        maskingTest = MaskingTest.builder("T1", "572||1").withModuleId("572||1||1").withId(MaskingTestId.createNewId()).build();
        maskingTest.directMask(MaskingActor.QC, MaskingType.DISTRIBUTION, user, "comment-1", Optional.empty());
        testJpaRepository.save(maskingTest);

    }
    
    private void scenario() {
        String id1 = UUID.randomUUID().toString();
        String id2 = UUID.randomUUID().toString();
        String id3 = UUID.randomUUID().toString();
        String id4 = UUID.randomUUID().toString();
        String id5 = UUID.randomUUID().toString();

        MasterTest masterTest1 = MasterTest.builder()
                .withId(id1)
                .withTestId("T1")
                .withName("Test T1")
                .withAbbreviation("T1")
                .build();

        MasterTest masterTest2 = MasterTest.builder()
                .withId(id2)
                .withTestId("T2")
                .withName("Test T2")
                .withAbbreviation("T2")
                .build();

        MasterTest masterTest3 = MasterTest.builder()
                .withId(id3)
                .withTestId("T3")
                .withName("Test T3")
                .withAbbreviation("T3")
                .build();

        MasterTest masterTest4 = MasterTest.builder()
                .withId(id4)
                .withTestId("T4")
                .withName("Test T4")
                .withAbbreviation("T4")
                .build();

        MasterTest masterTest5 = MasterTest.builder()
                .withId(id5)
                .withTestId("T5")
                .withName("Test T5")
                .withAbbreviation("T5")
                .build();

        masterTestJpaRepository.save(masterTest1);
        masterTestJpaRepository.save(masterTest2);
        masterTestJpaRepository.save(masterTest3);
        masterTestJpaRepository.save(masterTest4);
        masterTestJpaRepository.save(masterTest5);

        MasterTestJpa test1 = masterTestJpaRepositoryHelper.findById(id1).get();
        MasterTestJpa test2 = masterTestJpaRepositoryHelper.findById(id2).get();
        MasterTestJpa test3 = masterTestJpaRepositoryHelper.findById(id3).get();
        MasterTestJpa test4 = masterTestJpaRepositoryHelper.findById(id4).get();
        MasterTestJpa test5 = masterTestJpaRepositoryHelper.findById(id5).get();

        /* ---------------------------------------------------------- */

        MasterModuleJpa module1 = new MasterModuleJpa(
                UUID.randomUUID().toString(),
                "572||1||1",
                "module-1"
        );
        masterModuleJpaRepositoryHelper.save(module1);

        MasterModuleJpa module2 = new MasterModuleJpa(
                UUID.randomUUID().toString(),
                "572||1||2",
                "module-2"
        );
        masterModuleJpaRepositoryHelper.save(module2);

        MasterModuleJpa module3 = new MasterModuleJpa(
                UUID.randomUUID().toString(),
                "572||2||3",
                "module-3"
        );
        masterModuleJpaRepositoryHelper.save(module3);

        /* ---------------------------------------------------------- */

        MasterInstrumentJpa instrument1 = new MasterInstrumentJpa(
                UUID.randomUUID().toString(),
                "572||1",
                "instrument-1"
        );
        masterInstrumentJpaRepositoryHelper.save(instrument1);

        MasterInstrumentJpa instrument2 = new MasterInstrumentJpa(
                UUID.randomUUID().toString(),
                "572||2",
                "instrument-2"
        );
        masterInstrumentJpaRepositoryHelper.save(instrument2);

        /* ---------------------------------------------------------- */

        MasterTestMappingJpa masterTestMappingJpa1 = new MasterTestMappingJpa(
                UUID.randomUUID().toString(),
                test1,
                module1,
                instrument1
        );
        masterTestMappingJpaRepositoryHelper.save(masterTestMappingJpa1);

        MasterTestMappingJpa masterTestMappingJpa2 = new MasterTestMappingJpa(
                UUID.randomUUID().toString(),
                test2,
                module1,
                instrument1
        );
        masterTestMappingJpaRepositoryHelper.save(masterTestMappingJpa2);

        MasterTestMappingJpa masterTestMappingJpa3 = new MasterTestMappingJpa(
                UUID.randomUUID().toString(),
                test3,
                module1,
                instrument1
        );
        masterTestMappingJpaRepositoryHelper.save(masterTestMappingJpa3);

        MasterTestMappingJpa masterTestMappingJpa4 = new MasterTestMappingJpa(
                UUID.randomUUID().toString(),
                test4,
                module2,
                instrument1
        );
        masterTestMappingJpaRepositoryHelper.save(masterTestMappingJpa4);

        MasterTestMappingJpa masterTestMappingJpa5 = new MasterTestMappingJpa(
                UUID.randomUUID().toString(),
                test5,
                module3,
                instrument2
        );
        masterTestMappingJpaRepositoryHelper.save(masterTestMappingJpa5);
    }
}
