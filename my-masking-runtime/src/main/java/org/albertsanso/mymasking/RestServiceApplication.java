package org.albertsanso.mymasking;

import org.albertsanso.mymasking.scenario.MasterEntitiesScenario1;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.inject.Inject;

@SpringBootApplication(scanBasePackages = { "org.albertsanso.mymasking" })
public class RestServiceApplication implements CommandLineRunner {

    @Inject
    MasterEntitiesScenario1 masterEntitiesScenario1;

    public static void main(String[] args) {
        SpringApplication.run(RestServiceApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        //masterEntitiesScenario1.apply();
    }
}