package org.albertsanso.mymasking.configuration.aspect;

import org.albertsanso.commons.event.DomainEvent;
import org.albertsanso.commons.event.Event;
import org.albertsanso.commons.model.Entity;
import org.albertsanso.mymasking.configuration.event.EventPublisher;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Aspect
@Component
public class AspectConfiguration {

    private final EventPublisher eventPublisher;

    @Inject
    public AspectConfiguration(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @Pointcut(value="execution(public * org.albertsanso.mymasking.core.repository.jpa.adapter.*Repository.save(..))")
    void triggerRepositorySave() {}

    @After(value="triggerRepositorySave()")
    public void afterRepositorySaveAdvice(JoinPoint joinPoint) {

        if (joinPoint.getArgs().length > 0) {
            Entity entity = (Entity) joinPoint.getArgs()[0];
            if (entity.hasEvents()) {
                for (Event event : entity.getEvents()) {
                    eventPublisher.publish((DomainEvent) event);
                }
            }
        }
    }
}
