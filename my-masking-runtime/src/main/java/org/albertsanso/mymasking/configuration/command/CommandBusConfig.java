package org.albertsanso.mymasking.configuration.command;

import org.albertsanso.commons.command.CommandBus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommandBusConfig {

    MyCommandBus cb;

    public CommandBusConfig(MyCommandBus cb) {
        this.cb = cb;
    }

    @Bean
    public CommandBus getCommandBus() {
        return this.cb.getCommandBus();
    }
}
