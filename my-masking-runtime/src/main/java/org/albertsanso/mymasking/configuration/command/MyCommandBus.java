package org.albertsanso.mymasking.configuration.command;

import org.albertsanso.commons.command.DomainCommand;
import org.albertsanso.commons.command.DomainCommandHandler;
import org.albertsanso.commons.command.DomainCommandResponse;
import org.albertsanso.commons.command.SynchronousCommandBus;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
public class MyCommandBus {

    private final SynchronousCommandBus commandBus;

    @Inject
    public MyCommandBus(List<DomainCommandHandler<? extends DomainCommand>> handlerList) {
        this.commandBus = new SynchronousCommandBus(handlerList);
    }

    public DomainCommandResponse push(DomainCommand command) {
        return this.commandBus.push(command);
    }

    public SynchronousCommandBus getCommandBus() {
        return commandBus;
    }
}
