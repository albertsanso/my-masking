package org.albertsanso.mymasking.configuration.event;

import org.albertsanso.commons.event.DomainEvent;
import org.albertsanso.commons.event.DomainEventSubscriber;
import org.albertsanso.commons.event.SynchronousEventBus;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
public class EventPublisher {
    private final SynchronousEventBus synchronousEventBus;

    @Inject
    public EventPublisher(List<DomainEventSubscriber<? extends DomainEvent>> subscriberList) {
        this.synchronousEventBus = new SynchronousEventBus(subscriberList);
    }

    public void publish(DomainEvent domainEvent) {
        synchronousEventBus.publish(domainEvent);
    }
}
