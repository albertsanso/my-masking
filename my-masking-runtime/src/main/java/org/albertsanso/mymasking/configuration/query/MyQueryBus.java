package org.albertsanso.mymasking.configuration.query;

import org.albertsanso.commons.command.SynchronousCommandBus;
import org.albertsanso.commons.query.DomainQuery;
import org.albertsanso.commons.query.DomainQueryHandler;
import org.albertsanso.commons.query.DomainQueryResponse;
import org.albertsanso.commons.query.SynchronousQueryBus;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
public class MyQueryBus {

    private final SynchronousQueryBus queryBus;

    @Inject
    public MyQueryBus(List<DomainQueryHandler<? extends DomainQuery>> handlerList) {
        this.queryBus = new SynchronousQueryBus(handlerList);
    }

    public DomainQueryResponse push(DomainQuery query) {
        return queryBus.push(query);
    }

    public SynchronousQueryBus getQueryBus() {
        return queryBus;
    }
}
