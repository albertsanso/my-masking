package org.albertsanso.mymasking.configuration.query;

import org.albertsanso.commons.query.QueryBus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueryBusConfig {

    private final MyQueryBus queryBus;

    public QueryBusConfig(MyQueryBus queryBus) {
        this.queryBus = queryBus;
    }

    @Bean
    public QueryBus getQueryBus() {
        return queryBus.getQueryBus();
    }
}
