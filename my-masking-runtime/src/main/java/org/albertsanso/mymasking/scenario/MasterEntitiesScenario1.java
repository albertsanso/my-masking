package org.albertsanso.mymasking.scenario;

import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument.MasterInstrumentJpa;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.instrument.MasterInstrumentJpaRepositoryHelper;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module.MasterModuleJpa;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.module.MasterModuleJpaRepositoryHelper;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test.MasterTestJpa;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test.MasterTestJpaRepository;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.test.MasterTestJpaRepositoryHelper;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping.MasterTestMappingJpa;
import org.albertsanso.mymasking.core.masterdata.adapter.repository.jpa.testmapping.MasterTestMappingJpaRepositoryHelper;
import org.albertsanso.mymasking.core.masterdata.domain.model.test.MasterTest;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.UUID;

@Named
public class MasterEntitiesScenario1 {

    private final MasterTestJpaRepository masterTestJpaRepository;

    private final MasterTestJpaRepositoryHelper masterTestJpaRepositoryHelper;

    private final MasterModuleJpaRepositoryHelper masterModuleJpaRepositoryHelper;

    private final MasterInstrumentJpaRepositoryHelper masterInstrumentJpaRepositoryHelper;

    private final MasterTestMappingJpaRepositoryHelper masterTestMappingJpaRepositoryHelper;

    @Inject
    public MasterEntitiesScenario1(MasterTestJpaRepository masterTestJpaRepository, MasterTestJpaRepositoryHelper masterTestJpaRepositoryHelper, MasterModuleJpaRepositoryHelper masterModuleJpaRepositoryHelper, MasterInstrumentJpaRepositoryHelper masterInstrumentJpaRepositoryHelper, MasterTestMappingJpaRepositoryHelper masterTestMappingJpaRepositoryHelper) {
        this.masterTestJpaRepository = masterTestJpaRepository;
        this.masterTestJpaRepositoryHelper = masterTestJpaRepositoryHelper;
        this.masterModuleJpaRepositoryHelper = masterModuleJpaRepositoryHelper;
        this.masterInstrumentJpaRepositoryHelper = masterInstrumentJpaRepositoryHelper;
        this.masterTestMappingJpaRepositoryHelper = masterTestMappingJpaRepositoryHelper;
    }

    public void apply() {
        String id1 = UUID.randomUUID().toString();
        String id2 = UUID.randomUUID().toString();
        String id3 = UUID.randomUUID().toString();
        String id4 = UUID.randomUUID().toString();
        String id5 = UUID.randomUUID().toString();

        MasterTest masterTest1 = MasterTest.builder()
                .withId(id1)
                .withTestId("T1")
                .withName("Test T1")
                .withAbbreviation("T1")
                .build();

        MasterTest masterTest2 = MasterTest.builder()
                .withId(id2)
                .withTestId("T2")
                .withName("Test T2")
                .withAbbreviation("T2")
                .build();

        MasterTest masterTest3 = MasterTest.builder()
                .withId(id3)
                .withTestId("T3")
                .withName("Test T3")
                .withAbbreviation("T3")
                .build();

        MasterTest masterTest4 = MasterTest.builder()
                .withId(id4)
                .withTestId("T4")
                .withName("Test T4")
                .withAbbreviation("T4")
                .build();

        MasterTest masterTest5 = MasterTest.builder()
                .withId(id5)
                .withTestId("T5")
                .withName("Test T5")
                .withAbbreviation("T5")
                .build();

        masterTestJpaRepository.save(masterTest1);
        masterTestJpaRepository.save(masterTest2);
        masterTestJpaRepository.save(masterTest3);
        masterTestJpaRepository.save(masterTest4);
        masterTestJpaRepository.save(masterTest5);

        MasterTestJpa test1 = masterTestJpaRepositoryHelper.findById(id1).get();
        MasterTestJpa test2 = masterTestJpaRepositoryHelper.findById(id2).get();
        MasterTestJpa test3 = masterTestJpaRepositoryHelper.findById(id3).get();
        MasterTestJpa test4 = masterTestJpaRepositoryHelper.findById(id4).get();
        MasterTestJpa test5 = masterTestJpaRepositoryHelper.findById(id5).get();

        /* ---------------------------------------------------------- */

        MasterModuleJpa module1 = new MasterModuleJpa(
                UUID.randomUUID().toString(),
                "572||1||1",
                "module-1"
        );
        masterModuleJpaRepositoryHelper.save(module1);

        MasterModuleJpa module2 = new MasterModuleJpa(
                UUID.randomUUID().toString(),
                "572||1||2",
                "module-2"
        );
        masterModuleJpaRepositoryHelper.save(module2);

        MasterModuleJpa module3 = new MasterModuleJpa(
                UUID.randomUUID().toString(),
                "572||2||3",
                "module-3"
        );
        masterModuleJpaRepositoryHelper.save(module3);

        /* ---------------------------------------------------------- */

        MasterInstrumentJpa instrument1 = new MasterInstrumentJpa(
                UUID.randomUUID().toString(),
                "572||1",
                "instrument-1"
        );
        masterInstrumentJpaRepositoryHelper.save(instrument1);

        MasterInstrumentJpa instrument2 = new MasterInstrumentJpa(
                UUID.randomUUID().toString(),
                "572||2",
                "instrument-2"
        );
        masterInstrumentJpaRepositoryHelper.save(instrument2);

        /* ---------------------------------------------------------- */

        MasterTestMappingJpa masterTestMappingJpa1 = new MasterTestMappingJpa(
                UUID.randomUUID().toString(),
                test1,
                module1,
                instrument1
        );
        masterTestMappingJpaRepositoryHelper.save(masterTestMappingJpa1);

        MasterTestMappingJpa masterTestMappingJpa2 = new MasterTestMappingJpa(
                UUID.randomUUID().toString(),
                test2,
                module1,
                instrument1
        );
        masterTestMappingJpaRepositoryHelper.save(masterTestMappingJpa2);

        MasterTestMappingJpa masterTestMappingJpa3 = new MasterTestMappingJpa(
                UUID.randomUUID().toString(),
                test3,
                module1,
                instrument1
        );
        masterTestMappingJpaRepositoryHelper.save(masterTestMappingJpa3);

        MasterTestMappingJpa masterTestMappingJpa4 = new MasterTestMappingJpa(
                UUID.randomUUID().toString(),
                test4,
                module2,
                instrument1
        );
        masterTestMappingJpaRepositoryHelper.save(masterTestMappingJpa4);

        MasterTestMappingJpa masterTestMappingJpa5 = new MasterTestMappingJpa(
                UUID.randomUUID().toString(),
                test5,
                module3,
                instrument2
        );
        masterTestMappingJpaRepositoryHelper.save(masterTestMappingJpa5);
    }
}
